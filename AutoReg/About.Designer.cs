﻿namespace AutoReg
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.label1 = new System.Windows.Forms.Label();
            this.lb_curVersion = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tp_about = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tp_sendBug = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tp_about.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tournament Registration tool for PokerStars";
            // 
            // lb_curVersion
            // 
            this.lb_curVersion.AutoSize = true;
            this.lb_curVersion.Location = new System.Drawing.Point(6, 16);
            this.lb_curVersion.Name = "lb_curVersion";
            this.lb_curVersion.Size = new System.Drawing.Size(78, 13);
            this.lb_curVersion.TabIndex = 2;
            this.lb_curVersion.Text = "Current version";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tp_about);
            this.tabControl1.Controls.Add(this.tp_sendBug);
            this.tabControl1.Location = new System.Drawing.Point(150, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(320, 127);
            this.tabControl1.TabIndex = 6;
            // 
            // tp_about
            // 
            this.tp_about.Controls.Add(this.label3);
            this.tp_about.Controls.Add(this.label2);
            this.tp_about.Controls.Add(this.label1);
            this.tp_about.Controls.Add(this.lb_curVersion);
            this.tp_about.Location = new System.Drawing.Point(4, 22);
            this.tp_about.Name = "tp_about";
            this.tp_about.Padding = new System.Windows.Forms.Padding(3);
            this.tp_about.Size = new System.Drawing.Size(312, 101);
            this.tp_about.TabIndex = 0;
            this.tp_about.Text = "About";
            this.tp_about.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(291, 47);
            this.label3.TabIndex = 3;
            this.label3.Text = "This program is currently in beta version. ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Created by Pavel Komarov © 2014";
            // 
            // tp_sendBug
            // 
            this.tp_sendBug.Location = new System.Drawing.Point(4, 22);
            this.tp_sendBug.Name = "tp_sendBug";
            this.tp_sendBug.Padding = new System.Windows.Forms.Padding(3);
            this.tp_sendBug.Size = new System.Drawing.Size(312, 101);
            this.tp_sendBug.TabIndex = 1;
            this.tp_sendBug.Text = "Send Bug";
            this.tp_sendBug.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AutoReg.Properties.Resources.logoreg128;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 149);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "About";
            this.Text = "About Tournament Registrator";
            this.Load += new System.EventHandler(this.About_Load);
            this.tabControl1.ResumeLayout(false);
            this.tp_about.ResumeLayout(false);
            this.tp_about.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_curVersion;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tp_about;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tp_sendBug;
        private System.Windows.Forms.Label label3;
    }
}