﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoReg
{
    enum TaskPriority
    {
        HIGHEST = 0,
        HIGH = 1,
        NORMAL = 2,
        LOW = 3,
        LOWEST = 4
    }

    class TaskQueue<T>
    {
        private List<mTask> mTaskList = new List<mTask>();
        private struct mTask
        {
            public T Task;
            public TaskPriority Priority;
        }
        private System.Threading.Mutex mQueueMutex = new System.Threading.Mutex();

        public int Count
        {
            get
            {
                return mTaskList.Count;
            }
        }

        public void Enqueue(T task, TaskPriority priority)
        {
            
            mTask mtask = new mTask();
            mtask.Task = task;
            mtask.Priority = priority;
            Insert(mtask);
            
        }

        private void Insert(mTask mtask)
        {
            if (mQueueMutex.WaitOne())
            {
                //Logger.Write("MUTEX!", Logger.L.DEBUG);
            }
            bool ifInserted = false;
            if (mtask.Task == null)
            {
                Logger.Write("Task is null!", Logger.L.DEBUG);
                return;
            }
            for (var i = 0; i < mTaskList.Count; i++)
            {
                if (mTaskList[i].Priority > mtask.Priority)
                {
                    mTaskList.Insert(i, mtask);
                    ifInserted = true;
                    break;
                }
            }
            if (!ifInserted)
            {
                mTaskList.Add(mtask);
            }
            mQueueMutex.ReleaseMutex();
        }

        public T Dequeue()
        {
            mQueueMutex.WaitOne();
            if (mTaskList.Count != 0)
            {
                mTask task = mTaskList[0];
                
                if (task.Task == null)
                {
                    Logger.Write("Task is null!", Logger.L.DEBUG);
                }
                
                mTaskList.RemoveAt(0);
                mQueueMutex.ReleaseMutex();
                return task.Task;
            }
            else
            {
                mQueueMutex.ReleaseMutex();
                return default(T);
            }
        }
    }
}
