using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Linq;
using System.Diagnostics;
using System.Xml;

namespace AutoReg
{
    class Registrator
    {
        // CLASS VARIABLES
        private Thread RegThread;
        
        public static SessionData SesData = new SessionData();
        private static LobbyParameters lobbyParameters;
        public bool UnregisterCurrent = false;
        public static DateTime SessionStartTime;
        public static int RunMinutes = 0;
        public static bool SessionActive = false;
        public static bool SessionPaused = false;
        

        public Data RegData = new Data();
        public static List<Registrator> Pool = new List<Registrator>();
        public Stopwatch DoingCycleTime;
        public int LastCycleTime = 5000;
        public bool IsActive = false;
        public static bool ReleasePendingCycles = false;
        private static bool IsLobbyNotFound = true;

        public struct Data
        {
            public HashSet<int> CurrentlyRegistered;
            public List<Tournament> Tournaments;
            public int RegID;
            public int MaxCount;
            public int OpenNow;
            public int MaxPerPass;
            public int MaxTotal;
            public int TotalNow;
            public string ShortName;
            public string Filter;
            public int Period;
        }

        public struct SessionData
        {
            public int MaxCount;
            public int OpenNow;
            public int RegisteredNow;
            public int MaxTotal;
            public int TotalNow;
        }

        public struct LobbyParameters
        {
            public IntPtr mainLobbyHndl;
            public IntPtr listboxHndl;
            public IntPtr filterHndl;
        }
        
        private static bool AllowRegSemaphore = true; // Семафор, позволяющий поток провести регистрацию
        public static bool WaitForConfirm = false;

        public Registrator()
        {

        }

        private bool FindLobby()
        {
            IntPtr t = lobbyParameters.mainLobbyHndl = WinWrapper.FindWindow(null, "PokerStars Lobby"); // Поиск главного окна старзов
            if ((t == IntPtr.Zero)&&(!WinWrapper.IsWindowVisible(t))) // Если ошибка
            {
                if(IsLobbyNotFound) Logger.Write("PokerStars lobby not found!", Logger.L.FATAL);
                IsLobbyNotFound = false;
                return false;
            }
            IsLobbyNotFound = false;
            t = IntPtr.Zero;
            do {

                t = WinWrapper.FindWindowEx(lobbyParameters.mainLobbyHndl, t, "PokerStarsListClass", null);
                if (WinWrapper.IsWindowVisible(t))
                {
                    lobbyParameters.listboxHndl = t;
                    break;
                }
            } while (t != IntPtr.Zero);

            t = IntPtr.Zero;

            do
            {

                t = WinWrapper.FindWindowEx(lobbyParameters.mainLobbyHndl, t, "PokerStarsFilterClass", null);
                if (WinWrapper.IsWindowVisible(t))
                {
                    lobbyParameters.filterHndl = t;
                    break;
                }
            } while (t != IntPtr.Zero);
            return true;
        }

        public bool Start()
        {
            //Logger.Write("RegistratorOld \"" + RegData.ShortName + "\" is starting...!", Logger.L.DEBUG);
            //RegThread = new Thread(Register);
            //RegThread.Start(); // Запускаем поток регистрации
            Logger.Write("Registrator " + RegData.ShortName + " is started!", Logger.L.STATUS);
            IsActive = true;
            return true;
        }






        // =============================================================================================
        // =============================================================================================


        public void Register()
        {
            Tournament trnmnt = new Tournament
        }





        // =============================================================================================
        // =============================================================================================

        public void Init(
            int _id,
            string _shortName,
            int _maxCount,
            int _maxPerPass, 
            int _maxTotal, 
            string _filter, 
            int _period)
        {
            RegData.RegID = _id;
            RegData.ShortName = _shortName;
            RegData.CurrentlyRegistered = new HashSet<int>();
            RegData.Tournaments = new List<Tournament>();
            //RegData.OpenNow = getCurrentlyOpenedTournaments();
            RegData.MaxCount = _maxCount;
            RegData.MaxPerPass = _maxPerPass;
            RegData.TotalNow = (RegData.TotalNow == 0 ? 0 : RegData.TotalNow);
            RegData.MaxTotal = _maxTotal;
            RegData.Filter = _filter.ToLowerInvariant();
            RegData.Period = _period;
            //FindLobby();
            if (Settings.User.IfLoadTournamentsInfo) Load();
        }

        public void Save()
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("head"));

            XmlNode node = doc.CreateElement("MainData");
            doc.DocumentElement.AppendChild(node);
            XmlAttribute attr = doc.CreateAttribute("RegID");
            attr.Value = this.RegData.RegID.ToString();
            node.Attributes.Append(attr);
            attr = doc.CreateAttribute("TotalRegistered");
            attr.Value = this.RegData.RegID.ToString();
            node.Attributes.Append(attr);

            node = doc.CreateElement("Tournaments");
            doc.DocumentElement.AppendChild(node);

            foreach (Tournament trnmnt in RegData.Tournaments)
            {
                node.AppendChild(trnmnt.ToXML(doc));
            }

            try
            {
                doc.Save("./tmp/filter" + RegData.ShortName.ToString() + ".bak");
            }
            catch (System.IO.IOException e)
            {
                Logger.Write("IO error while saving registrator data (" + e.Message + ")", Logger.L.WARN);
            }
        }

        public void Load()
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load("./tmp/filter" + RegData.ShortName.ToString() + ".bak");
            }
            catch
            {
                Logger.Write("Cannot load registrator data", Logger.L.INFO);
                return;
            }

            XmlNode node = doc.ChildNodes[0].ChildNodes[0]; // Main Data
            string str = node.InnerXml;
            RegData.RegID = int.Parse(node.Attributes[0].Value);
            //RegData.

            node = doc.ChildNodes[0].ChildNodes[1]; // tournaments

            foreach (XmlNode t in node)
            {
                Tournament trnmnt = new Tournament();
                trnmnt.FromXML(t);
                RegData.Tournaments.Add(trnmnt);
            }

        }

        //public bool Stop()
        //{
        //    if(RegThread != null)
        //    {
        //        if (RegThread.IsAlive)
        //        {
        //            AllowRegSemaphore = true;
        //            IsActive = false;
        //            Logger.Write("Registration stopped!", Logger.L.INFO);
        //            RegThread.Abort();
        //        }
        //    }
        //    return true;
        //}

        //private void Register() // Весь цикл регистрации
        //{
        //    while (RegThread.IsAlive)
        //    {
        //        LastCycleTime = 5000;
        //        ReleasePendingCycles = false;
                
        //        while (!AllowRegSemaphore)
        //        {
        //            Logger.Write("Registrator " + RegData.ShortName + " waiting...", Logger.L.DEBUG);
        //            Thread.Sleep(500);
        //        }
        //        Logger.Write("Registrator " + RegData.ShortName + " starting cycle", Logger.L.DEBUG);
        //        AllowRegSemaphore = false;
        //        IsLobbyNotFound = true;
        //        if (!FindLobby()) Stop();

                

        //        for (int i = 0; i < RegData.MaxPerPass; i++) // Количество турниров, регистрируемых за раз
        //        {
        //            DoingCycleTime = new Stopwatch(); // Засекаем время выполнения цикла регистрации
        //            DoingCycleTime.Start(); 

        //            if ((!UnregisterCurrent) && IsRegistrability()) //Стартуем регистрацию
        //            {
        //                //WinWrapper.ShowWindow(lobbyParameters.mainLobbyHndl, 9);
        //                //Thread.Sleep(200);
        //                Logger.Write("Registering tournament, offset=" + i +
        //                    " CO=" + RegData.OpenNow + " SO=" + SesData.OpenNow +
        //                    " CT=" + RegData.TotalNow + " ST=" + SesData.TotalNow,
        //                    Logger.L.DEBUG);

        //                Tournament trnmnt = new Tournament(this, i, lobbyParameters);
        //                trnmnt.Register();

                        
        //            }
        //            else if (UnregisterCurrent)
        //            {
        //                foreach(Tournament trnmnt in RegData.Tournaments)
        //                {
        //                    if (trnmnt.Status == Tournament.Statuses.REGISTERED)
        //                    {
        //                        //if (UnregisterCurrent) trnmnt.Unregister();
        //                        trnmnt.Status = Tournament.Statuses.UNKNOWN;
        //                        Logger.Write("Tournament " + trnmnt.ID + " is unregistered!", Logger.L.DEBUG);
        //                    }
        //                }
        //            }
        //            Logger.Write("Ending cycle... CO=" + RegData.OpenNow + " SO=" + SesData.OpenNow +
        //                    " CT=" + RegData.TotalNow + " ST=" + SesData.TotalNow,
        //                    Logger.L.DEBUG);
        //        }
        //        //WinWrapper.ShowWindow(lobbyParameters.mainLobbyHndl, 6);
                
        //        AllowRegSemaphore = true;
        //        Update();
        //        Registrator.UpdateSession();
        //        DoingCycleTime.Stop();
        //        Logger.Write("Registrator " + RegData.ShortName + " doing cycle " + DoingCycleTime.ElapsedMilliseconds.ToString() + "ms", Logger.L.INFO);
        //        DoingCycleTime.Reset();

        //        for (int i = 0; i < RegData.Period; i++) // "Засыпаем" поток на период, с проверкой сообщения unregister каждую секунду
        //        {
        //            Thread.Sleep(1000);
        //            if (UnregisterCurrent)
        //            {
        //                Logger.Write("Breaking sleep " + "(UnregisterCurrent)", Logger.L.DEBUG);
        //                break;
        //            }
        //        }
        //    }
            
        //}

        //public void RegisterCallback(Tournament trnmnt)
        //{
        //    if (trnmnt.Status == Tournament.Statuses.UNKNOWN)
        //    {
        //        Logger.Write("Unknown registration error!", Logger.L.WARN, false, true);
        //        RegData.Tournaments.Add(trnmnt);
        //    }
        //    else if (trnmnt.Status == Tournament.Statuses.REGISTERED) // Если всё ОК
        //    {
        //        //RegData.CurrentlyRegistered.Add(trnmnt.ID);
        //        RegData.Tournaments.Add(trnmnt);

        //        //RegData.TotalNow++;
        //        Logger.Write("Tournament " + trnmnt.ID.ToString() + " by filter \"" + RegData.ShortName + "\" is registered!", Logger.L.STATUS);
        //    }
        //    else if (trnmnt.Status == Tournament.Statuses.CURRENTLYREGISTERED)
        //    {
        //        Logger.Write("Tournament " + trnmnt.ID + " currently registered!", Logger.L.DEBUG);
        //        trnmnt.Status = Tournament.Statuses.REGISTERED;
        //    }
        //    else if (trnmnt.Status == Tournament.Statuses.ERROR)
        //    {
        //        Logger.Write("Registering tournament " + trnmnt.ID + " failed! See log for details.", Logger.L.WARN, false, true);
        //    }
        //}

        public static bool UnregisterAll() // Отменяет регистрацию во всех турнирах через Ктрл+Р
        {
            Logger.Write("Unregistering from all possible tournaments...", Logger.L.INFO);
            WinWrapper.ShowWindow(lobbyParameters.mainLobbyHndl, 9);
            WinWrapper.SetForegroundWindow(lobbyParameters.mainLobbyHndl);
            WinWrapper.keybd_event(0x11, 0, 0, 0);
            WinWrapper.keybd_event(0x52, 0, 0, 0);
            WinWrapper.keybd_event(0x52, 0, 2, 0);
            WinWrapper.keybd_event(0x11, 0, 2, 0);

            IntPtr regInHndl = IntPtr.Zero;
            for (int i = 0; i < 10; i++)
            {
                regInHndl = WinWrapper.FindWindow("#32770", "Registered In Tournaments");
                Thread.Sleep(25);
            }
            bool success = false;
            IntPtr selAllChk = IntPtr.Zero;
            do{
                Thread.Sleep(200);
                selAllChk = WinWrapper.FindWindowEx(regInHndl, selAllChk, "PokerStarsButtonClass", null);
                WinWrapper.RECT rct;
                WinWrapper.GetWindowRect(selAllChk, out rct);
                Logger.Write("Window size is " + (rct.Right - rct.Left) + "x" + (rct.Bottom - rct.Top), Logger.L.DEBUG);
                if ((rct.Right - rct.Left == 15) && (rct.Bottom - rct.Top == 15))
                {
                    Logger.Write("\"Registering in ...\" window and checkbox finded!", Logger.L.DEBUG);
                    WinWrapper.SetForegroundWindow(selAllChk);
                    WinWrapper.SendMessage(selAllChk, 0x100, 0x20, 0);
                    WinWrapper.SendMessage(selAllChk, 0x101, 0x20, 0);
                    IntPtr unRegBtn = IntPtr.Zero;
                    do{
                        unRegBtn = WinWrapper.FindWindowEx(regInHndl, unRegBtn, null, null);
                        string capt = WinWrapper.GetText(unRegBtn);
                        if (capt == "Unregister")
                        {
                            //WinWrapper.SetForegroundWindow(selAllChk);
                            WinWrapper.SendMessage(unRegBtn, 0x100, 0x20, 0);
                            WinWrapper.SendMessage(unRegBtn, 0x101, 0x20, 0);
                            IntPtr okWnd = IntPtr.Zero;
                            for (int i = 0; i < 10; i++)
                            {
                                okWnd = WinWrapper.FindWindow("#32770", "Pokerstars");
                                Thread.Sleep(25);
                            }
                            if (okWnd != IntPtr.Zero)
                            {
                                WinWrapper.SendMessage(okWnd, 0x0010, 0, 0);
                                success = true;
                            }
                            break;
                        }
                    } while (unRegBtn != IntPtr.Zero);
                }
            } while(selAllChk != IntPtr.Zero);
            if (!success) Logger.Write("You have no registered tournaments now!", Logger.L.INFO);
            WinWrapper.SetForegroundWindow(regInHndl);
            WinWrapper.SendMessage(regInHndl, 0x0010, 0, 0);
            foreach (Registrator reg in Pool)
            {
                foreach (Tournament trnmnt in reg.RegData.Tournaments)
                {
                    if (trnmnt.Status == Tournament.Statuses.REGISTERED)
                    {
                        trnmnt.Status = Tournament.Statuses.UNKNOWN;
                    }
                }
            }
            Logger.Write("All possible tournaments is unregistered!", Logger.L.STATUS);

            return true;
        }

        private bool IsRegistrability()
        {
            //Update();
            if ((SesData.MaxTotal <= SesData.TotalNow) && Settings.Session.IfTotalMaxTables)
            {
                Logger.Write("Registration is impossible, because session total tournaments is reaches limit! (ST = " +
                    SesData.TotalNow + ", SMT = " + SesData.MaxTotal + ")", Logger.L.INFO);
                return false;
            }
            else if ((SesData.MaxCount <= SesData.OpenNow)&&Settings.Session.IfKeepMaxTables)
            {
                Logger.Write("Registration is impossible, because session opened tournaments is reaches limit! (SO = " +
                    SesData.OpenNow + ", SM = " + SesData.MaxCount + ")", Logger.L.INFO);
                return false;
            }
            else if (RegData.MaxTotal <= RegData.TotalNow)
            {
                Logger.Write("Registration is impossible, because current registrator total tournaments is reaches limit! (CO = " +
                    RegData.TotalNow + ", CM = " + RegData.MaxTotal + ")", Logger.L.INFO);
                return false;
            }

            //getCurrentlyOpenedTournaments();
            if (RegData.MaxCount <= RegData.OpenNow)
            {
                Logger.Write("Registration is impossible, because current registrator opened tournaments is reaches limit! (CO = " +
                    RegData.OpenNow + ", CM = " + RegData.MaxCount + ")", Logger.L.INFO);
                return false;
            }
            else if (!TestFilter(10))
            {
                Logger.Write("Filter Error!", Logger.L.WARN);
                return false;
            }
            else
            {
                Logger.Write("Registration is allowed! (CO=" + RegData.OpenNow + ", CT=" + RegData.TotalNow +
                    ", SO=" + SesData.OpenNow + ", ST=" + SesData.TotalNow, Logger.L.DEBUG);
                return true;
            }
        }

        public bool TestFilter(int maxAttempts)
        {
            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            IsLobbyNotFound = true;
            if (!FindLobby() && (!IsActive)) return false;
            //sw.Stop();
            //Logger.Write(sw.ElapsedMilliseconds.ToString(), Logger.L.STATUS);
            for (int att = 0; att < maxAttempts; att++)
            {
                IntPtr t = IntPtr.Zero;
                if (lobbyParameters.filterHndl != IntPtr.Zero)
                {
                    WinWrapper.SetForegroundWindow(lobbyParameters.mainLobbyHndl);   //
                    WinWrapper.SendMessage(lobbyParameters.filterHndl, 0x201, 1, 0); // Focusing filter
                    WinWrapper.SendMessage(lobbyParameters.filterHndl, 0x202, 1, 0); // 

                    WinWrapper.keybd_event(0x11, 0, 0, 0); // Clearing filter
                    WinWrapper.keybd_event(0x41, 0, 0, 0);
                    WinWrapper.keybd_event(0x41, 0, 2, 0);
                    WinWrapper.keybd_event(0x11, 0, 2, 0);
                    WinWrapper.keybd_event(0x08, 0, 0, 0);
                    WinWrapper.keybd_event(0x08, 0, 2, 0);
                }
                else { return false; }
                Thread.Sleep(50);
                //WinWrapper.SetForegroundWindow(lobbyParameters.mainLobbyHndl);
                WinWrapper.SetForegroundWindow(lobbyParameters.mainLobbyHndl);   //
                WinWrapper.SendMessage(lobbyParameters.filterHndl, 0x201, 1, 0); // Focusing filter
                WinWrapper.SendMessage(lobbyParameters.filterHndl, 0x202, 1, 0); // 

                /*
                 * Когда-нибудь тут будет переключение раскладки клавы
                uint threadId;
                WinWrapper.GetWindowThreadProcessId(lobbyParameters.mainLobbyHndl, out threadId);
           
                WinWrapper.PostMessage(
                    lobbyParameters.mainLobbyHndl,
                    0x0050, 
                    0,
                    (int)WinWrapper.LoadKeyboardLayout("00000409", 1)); */


                WinWrapper.SendKeys(RegData.Filter);

                Thread.Sleep(200);
                t = IntPtr.Zero;

                t = WinWrapper.FindWindowEx(lobbyParameters.mainLobbyHndl, t, "PokerStarsFilterClass", null);
                string c = WinWrapper.GetText(t);
                if (c == RegData.Filter)
                {
                    Logger.Write("Testing filter: " + c + " = " + RegData.Filter + "... OK!", Logger.L.DEBUG);
                    return true;
                }
                else
                {
                    Logger.Write("Testing filter: " + c + " != " + RegData.Filter + "... Fail!", Logger.L.DEBUG);
                }
            }
            return false;
        }

        //public HashSet<int> currentlyOpenedTournaments()
        //{
        //    HashSet<int> tournArr = new HashSet<int>();
        //    /* ДО ЛУЧШИХ ВРЕМЁН...
        //     * Regex regex = new Regex(    @"[$]"+  // $
        //                                "([0-9.]*)[ ]"+  // Байин
        //                                "([A-Z]{2})[ ]" +  // Лимит
        //                                "([A-Za-z']*)[ ]" + // Игра
        //                                "[[]([0-9]{2}) Players"  // Количество игроков
        //                                );
        //     * */
        //    Regex regex = new Regex(@"[A-Za-z0-9.'\[\]$\/ \-]*Tournament ([0-9]*).*");

            
        //    IntPtr t = IntPtr.Zero;
        //    int trnmtID = 0;
        //    IntPtr desktopWnd = WinWrapper.GetDesktopWindow();
        //    do
        //    {
        //        t = WinWrapper.FindWindowEx(desktopWnd, t, "PokerStarsTableFrameClass", null);
        //        string capt = WinWrapper.GetText(t);
        //        if (capt != "")
        //        {
        //            if (regex.IsMatch(capt))
        //            {
        //                Match match = regex.Match(capt);
        //                trnmtID = Convert.ToInt32(match.Groups[1].Value);
        //                tournArr.Add(trnmtID);
        //                //int i = 1;
        //            }
        //        }

        //    } while ((t != IntPtr.Zero)||ReleasePendingCycles);
            
        //    //string str = "$1.50 NL Hold'em [90 Players, Knockout] - Blinds $600/$1200 Ante $125 - Tournament 1021021020 Table 9";
        //    //bool r = regex.IsMatch(str);
        //    //Match match = regex.Match(str);
        //    /*str = match.Groups[1].Value;
        //    double t_buyIn = Convert.ToDouble(((str.IndexOf('.') != -1) ? str=str.Replace('.', ',') : str));
        //    string t_limit = match.Groups[2].Value;
        //    string t_game = match.Groups[3].Value;
        //    int t_players = Convert.ToInt32(match.Groups[4].Value);*/
        //    //int tID = Convert.ToInt32(match.Groups[1].Value);

        //    return tournArr;
        //}

        //public static void TestClick()
        //{
        //    IntPtr desktopWnd = WinWrapper.GetDesktopWindow();
        //    IntPtr t = IntPtr.Zero;
        //    do
        //    {
        //        t = WinWrapper.FindWindowEx(desktopWnd, t, "PokerStarsTableFrameClass", null);
        //        string capt = WinWrapper.GetText(t);
        //        if (capt != "")
        //        {
        //            WinWrapper.SendMessage(t, 0x0201, 0x0001, ((475 << 16) | 215));
        //            WinWrapper.SendMessage(t, 0x0202, 0, ((475 << 16) | 215));
        //        }

        //    } while ((t != IntPtr.Zero) || ReleasePendingCycles);
        //}

        //public int getCurrentlyOpenedTournaments()
        //{
        //    int curTourn = 0;
        //    HashSet<int> curOpen = currentlyOpenedTournaments();

        //    int uncountedTournaments = 0;  // Потерянные открытые турниры
        //    int registeredNotOpenedYet = 0; // Зарегистрированные, но ещё не открытые
        //    foreach (Tournament trnmnt in RegData.Tournaments)
        //    {
        //        bool isClosed = true;
        //        foreach (int op in curOpen)
        //        {
        //            if (trnmnt.ID == op)
        //            {
        //                if (trnmnt.Status != Tournament.Statuses.OPENED) Logger.Write("Tournament " + trnmnt.ID + " is OPENED!", Logger.L.STATUS);
        //                trnmnt.Status = Tournament.Statuses.OPENED;
        //                isClosed = false;
        //                break;
        //            }
        //        }
        //        if (!isClosed) uncountedTournaments++; // Если ни один из открытых не существует в массиве
        //        if (isClosed && (trnmnt.Status == Tournament.Statuses.OPENED))
        //        {
        //            trnmnt.Status = Tournament.Statuses.CLOSED;
        //            Logger.Write("Tournament " + trnmnt.ID + " is CLOSED!", Logger.L.STATUS);
        //        }
        //        if (trnmnt.Status == Tournament.Statuses.REGISTERED) registeredNotOpenedYet++;
        //    }
            
        //    return curTourn;
        //}

        //public void Update()
        //{
        //    RegData.OpenNow = 0;
        //    RegData.TotalNow = 0;
        //    getCurrentlyOpenedTournaments();
        //    //bool needDiskCache;
        //    foreach (Tournament trnmnt in RegData.Tournaments)
        //    {
        //        if ((trnmnt.Status == Tournament.Statuses.OPENED)||
        //            (trnmnt.Status == Tournament.Statuses.REGISTERED)) RegData.OpenNow++;

        //        if ((trnmnt.Status == Tournament.Statuses.REGISTERED) ||
        //            (trnmnt.Status == Tournament.Statuses.OPENED) ||
        //            (trnmnt.Status == Tournament.Statuses.CLOSED) ||
        //            (trnmnt.Status == Tournament.Statuses.CURRENTLYREGISTERED)) RegData.TotalNow++;
        //        //if((trnmnt.Status != Tournament.Statuses.UNKNOWN)||
        //            //(trnmnt.Status != Tournament.Statuses.UNREGISTERED)) RegData.TotalNow++;
        //    }
        //    this.Save(); // Сброс информации на диск
        //}

        //public static void UpdateSession()
        //{
        //    SesData.OpenNow = 0;
        //    SesData.TotalNow = 0;

        //    foreach (Registrator reg in Pool)
        //    {
        //        SesData.OpenNow += reg.RegData.OpenNow;
        //        SesData.TotalNow += reg.RegData.TotalNow;
        //    }

        //}
    }
}
