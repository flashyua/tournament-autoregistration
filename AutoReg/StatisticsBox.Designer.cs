﻿namespace AutoReg
{
    partial class StatisticsBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatisticsBox));
            this.lb_keepNowStat = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lb_totalNowStat = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lb_keepNowStat
            // 
            this.lb_keepNowStat.AutoSize = true;
            this.lb_keepNowStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_keepNowStat.Location = new System.Drawing.Point(16, 15);
            this.lb_keepNowStat.Name = "lb_keepNowStat";
            this.lb_keepNowStat.Size = new System.Drawing.Size(25, 25);
            this.lb_keepNowStat.TabIndex = 1;
            this.lb_keepNowStat.Text = "0";
            this.lb_keepNowStat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(4, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "Keep now";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(55, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "Total Now";
            // 
            // lb_totalNowStat
            // 
            this.lb_totalNowStat.AutoSize = true;
            this.lb_totalNowStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_totalNowStat.Location = new System.Drawing.Point(66, 15);
            this.lb_totalNowStat.Name = "lb_totalNowStat";
            this.lb_totalNowStat.Size = new System.Drawing.Size(25, 25);
            this.lb_totalNowStat.TabIndex = 1;
            this.lb_totalNowStat.Text = "0";
            this.lb_totalNowStat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StatisticsBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(110, 40);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lb_totalNowStat);
            this.Controls.Add(this.lb_keepNowStat);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StatisticsBox";
            this.Text = "Statistics";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StatisticsBox_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lb_keepNowStat;
        public System.Windows.Forms.Label lb_totalNowStat;
    }
}