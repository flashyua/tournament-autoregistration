﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace AutoReg
{
    public static class WinWrapper
    {
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string lclassName, string windowTitle);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool IsIconic(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr PostMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        public static extern IntPtr SendMessageGetText(IntPtr hWnd, uint msg, UIntPtr wParam, StringBuilder lParam);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetWindowText(IntPtr hWnd, StringBuilder lpStr, int nCount);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetDesktopWindow();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr SetActiveWindow(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetFocus();

        [DllImport("user32.dll")]
        public static extern IntPtr SetFocus(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern bool keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);

        private const uint WM_GETTEXT = 13;
        private const uint WM_QUIT = (uint)0x0012;
        private const uint WM_CLOSE = (uint)0x0010;
        private const uint WM_KEYDOWN = (uint)0x0100;
        private const uint WM_KEYUP = (uint)0x0101;

        private const int bufferSize = 1000; // adjust as necessary


        public static string GetText(IntPtr hWnd)
        {
            StringBuilder sb = new StringBuilder(bufferSize);
            SendMessageGetText(hWnd, WM_GETTEXT, (UIntPtr)sb.Capacity, sb);
            //InternalGetWindowText(hWnd, sb, sb.Capacity);
            return sb.ToString();
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(long dwFlags, long dx, long dy, long cButtons, long dwExtraInfo);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);

        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        private const byte VK_RETURN = 0x0D;
        const uint KEYEVENTF_KEYUP = 0x0002;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        public static void PressEnter()
        {
            keybd_event(VK_RETURN, 0, 0, 0);
            //Thread.Sleep(100);
            keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);
        }

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool GetWindowRect(IntPtr hwnd, out RECT lpRect);

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern IntPtr GetKeyboardLayout(uint idThread);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.dll")]
        public static extern IntPtr LoadKeyboardLayout(string pwszKLID, uint Flags);

        public struct RECT
        {
            public int Left;        // x position of upper-left corner
            public int Top;         // y position of upper-left corner
            public int Right;       // x position of lower-right corner
            public int Bottom;      // y position of lower-right corner
        }

        public static void ScrollListBox(IntPtr hWnd, int pos, int count)
        {
            if (hWnd != IntPtr.Zero)
            {
                //WinWrapper.SetForegroundWindow(t);
                WinWrapper.SetForegroundWindow(hWnd);
                WinWrapper.SendMessage(hWnd, 0x201, 1, 0); // Focusing listbox
                WinWrapper.SendMessage(hWnd, 0x202, 0, 0); // 
                for (int i = 0; i < count; i++) WinWrapper.SendKeys("↑");
                for (int i = 0; i < pos; i++) WinWrapper.SendKeys("↓");
            }
            Thread.Sleep(500);
        }

        public static void SendKeys(string str)
        {
            str = str.ToLowerInvariant();
            char[] chr = str.ToCharArray(); // Разбивка строки

            //Stopwatch sWatch = new Stopwatch();
            //sWatch.Start();

            foreach (char ch in chr)
            {
                switch (ch)
                {
                    case '0':   WinWrapper.keybd_event(0x30, 0, 0, 0);
                                WinWrapper.keybd_event(0x30, 0, 2, 0); break;
                    case '1':   WinWrapper.keybd_event(0x31, 0, 0, 0);
                                WinWrapper.keybd_event(0x31, 0, 2, 0); break;
                    case '2':   WinWrapper.keybd_event(0x32, 0, 0, 0);
                                WinWrapper.keybd_event(0x32, 0, 2, 0); break;
                    case '3':   WinWrapper.keybd_event(0x33, 0, 0, 0);
                                WinWrapper.keybd_event(0x33, 0, 2, 0); break;
                    case '4':   WinWrapper.keybd_event(0x34, 0, 0, 0);
                                WinWrapper.keybd_event(0x34, 0, 2, 0); break;
                    case '5':   WinWrapper.keybd_event(0x35, 0, 0, 0);
                                WinWrapper.keybd_event(0x35, 0, 2, 0); break;
                    case '6':   WinWrapper.keybd_event(0x36, 0, 0, 0);
                                WinWrapper.keybd_event(0x36, 0, 2, 0); break;
                    case '7':   WinWrapper.keybd_event(0x37, 0, 0, 0);
                                WinWrapper.keybd_event(0x37, 0, 2, 0); break;
                    case '8':   WinWrapper.keybd_event(0x38, 0, 0, 0);
                                WinWrapper.keybd_event(0x38, 0, 2, 0); break;
                    case '9':   WinWrapper.keybd_event(0x39, 0, 0, 0);
                                WinWrapper.keybd_event(0x39, 0, 2, 0); break;
                    case 'a':   WinWrapper.keybd_event(0x41, 0, 0, 0);
                                WinWrapper.keybd_event(0x41, 0, 2, 0); break;
                    case 'b':   WinWrapper.keybd_event(0x42, 0, 0, 0);
                                WinWrapper.keybd_event(0x42, 0, 2, 0); break;
                    case 'c':   WinWrapper.keybd_event(0x43, 0, 0, 0);
                                WinWrapper.keybd_event(0x43, 0, 2, 0); break;
                    case 'd':   WinWrapper.keybd_event(0x44, 0, 0, 0);
                                WinWrapper.keybd_event(0x44, 0, 2, 0); break;
                    case 'e':   WinWrapper.keybd_event(0x45, 0, 0, 0);
                                WinWrapper.keybd_event(0x45, 0, 2, 0); break;
                    case 'f':   WinWrapper.keybd_event(0x46, 0, 0, 0);
                                WinWrapper.keybd_event(0x46, 0, 2, 0); break;
                    case 'g':   WinWrapper.keybd_event(0x47, 0, 0, 0);
                                WinWrapper.keybd_event(0x47, 0, 2, 0); break;
                    case 'h':   WinWrapper.keybd_event(0x48, 0, 0, 0);
                                WinWrapper.keybd_event(0x48, 0, 2, 0); break;
                    case 'i':   WinWrapper.keybd_event(0x49, 0, 0, 0);
                                WinWrapper.keybd_event(0x49, 0, 2, 0); break;
                    case 'j':   WinWrapper.keybd_event(0x4A, 0, 0, 0);
                                WinWrapper.keybd_event(0x4A, 0, 2, 0); break;
                    case 'k':   WinWrapper.keybd_event(0x4B, 0, 0, 0);
                                WinWrapper.keybd_event(0x4B, 0, 2, 0); break;
                    case 'l':   WinWrapper.keybd_event(0x4C, 0, 0, 0);
                                WinWrapper.keybd_event(0x4C, 0, 2, 0); break;
                    case 'm':   WinWrapper.keybd_event(0x4D, 0, 0, 0);
                                WinWrapper.keybd_event(0x4D, 0, 2, 0); break;
                    case 'n':   WinWrapper.keybd_event(0x4E, 0, 0, 0);
                                WinWrapper.keybd_event(0x4E, 0, 2, 0); break;
                    case 'o':   WinWrapper.keybd_event(0x4F, 0, 0, 0);
                                WinWrapper.keybd_event(0x4F, 0, 2, 0); break;
                    case 'p':   WinWrapper.keybd_event(0x50, 0, 0, 0);
                                WinWrapper.keybd_event(0x50, 0, 2, 0); break;
                    case 'q':   WinWrapper.keybd_event(0x51, 0, 0, 0);
                                WinWrapper.keybd_event(0x51, 0, 2, 0); break;
                    case 'r':   WinWrapper.keybd_event(0x52, 0, 0, 0);
                                WinWrapper.keybd_event(0x52, 0, 2, 0); break;
                    case 's':   WinWrapper.keybd_event(0x53, 0, 0, 0);
                                WinWrapper.keybd_event(0x53, 0, 2, 0); break;
                    case 't':   WinWrapper.keybd_event(0x54, 0, 0, 0);
                                WinWrapper.keybd_event(0x54, 0, 2, 0); break;
                    case 'u':   WinWrapper.keybd_event(0x55, 0, 0, 0);
                                WinWrapper.keybd_event(0x55, 0, 2, 0); break;
                    case 'v':   WinWrapper.keybd_event(0x56, 0, 0, 0);
                                WinWrapper.keybd_event(0x56, 0, 2, 0); break;
                    case 'w':   WinWrapper.keybd_event(0x57, 0, 0, 0);
                                WinWrapper.keybd_event(0x57, 0, 2, 0); break;
                    case 'x':   WinWrapper.keybd_event(0x58, 0, 0, 0);
                                WinWrapper.keybd_event(0x58, 0, 2, 0); break;
                    case 'y':   WinWrapper.keybd_event(0x59, 0, 0, 0);
                                WinWrapper.keybd_event(0x59, 0, 2, 0); break;
                    case 'z':   WinWrapper.keybd_event(0x5A, 0, 0, 0);
                                WinWrapper.keybd_event(0x5A, 0, 2, 0); break;
                    case '$':   WinWrapper.keybd_event(0x10, 0, 0, 0);
                                WinWrapper.keybd_event(0x34, 0, 0, 0);
                                WinWrapper.keybd_event(0x34, 0, 2, 0);
                                WinWrapper.keybd_event(0x10, 0, 2, 0); break;
                    case '-':   WinWrapper.keybd_event(0x6D, 0, 0, 0);
                                WinWrapper.keybd_event(0x6D, 0, 2, 0); break;
                    case ' ':   WinWrapper.keybd_event(0x20, 0, 0, 0);
                                WinWrapper.keybd_event(0x20, 0, 2, 0); break;
                    case '.':   WinWrapper.keybd_event(0xBE, 0, 0, 0);
                                WinWrapper.keybd_event(0xBE, 0, 2, 0); break;
                    case '\'':  WinWrapper.keybd_event(0xDE, 0, 0, 0);
                                WinWrapper.keybd_event(0xDE, 0, 2, 0); break;
                    case '↑':   WinWrapper.keybd_event(0x26, 0, 0, 0);
                                WinWrapper.keybd_event(0x26, 0, 2, 0); break;
                    case '↓':   WinWrapper.keybd_event(0x28, 0, 0, 0);
                                WinWrapper.keybd_event(0x28, 0, 2, 0); break;
                    default:  break;
                }
            }
            //sWatch.Stop();
            //MessageBox.Show(sWatch.ElapsedMilliseconds.ToString());
        }
    }
}
