﻿//#define EMULATE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace AutoReg
{
    static class PokerstarsParser
    {
        private static FileStream StarsLogFileStream;
        private static StreamReader StarsLogFileReader;
        private static Regex regex;
        public delegate void TournamentStatusChanged(int iID, TournamentState state);
        public static event TournamentStatusChanged OnTournamentStatusChanged;
        private static FileSystemWatcher mFSWatcher = new FileSystemWatcher();
        private static System.Timers.Timer mTimer = new System.Timers.Timer();
        private static System.Threading.Thread mParserThread;
        private static string mLine;
        private static bool mIfFirstLaunch = true;

        private static long counter = 0;

        public static string CurrentLine
        {
            get
            {
                return mLine;
            }
        }

        public static bool IsAlive
        {
            get;
            set;
        }

        public static bool Init()
        {
            try
            {
#if EMULATE
                StarsLogFileStream = new FileStream(@"C:\Users\Admin\Documents\Visual Studio 2013\Projects\AutoReg\AutoReg\bin\Release\psemullog.txt", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
#else
                StarsLogFileStream = new FileStream(Settings.User.PokerstarsSettingsFolder + @"\PokerStars.log.0", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
#endif
                StarsLogFileReader = new StreamReader(StarsLogFileStream);

                if (mParserThread != null)
                {
                    if (mParserThread.IsAlive)
                    {
                        mParserThread.Abort();
                    }
                }

                //if (mIfFirstLaunch)
                //{
                //    using (StarsLogFileReader = new StreamReader(new FileStream(Settings.User.PokerstarsSettingsFolder + @"\PokerStars.log.0", FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                //    {
                //        StarsLogFileReader.BaseStream.Position = counter;
                //        while (!StarsLogFileReader.EndOfStream)
                //        {
                //            StarsLogFileReader.ReadLine();
                //        }
                //        counter = StarsLogFileReader.BaseStream.Position;
                //    }
                //    mIfFirstLaunch = false;
                //}

                mParserThread = new System.Threading.Thread(delegate()
                {
                    while (true)
                    {
                        using (StarsLogFileReader = new StreamReader(new FileStream(Settings.User.PokerstarsSettingsFolder + @"\PokerStars.log.0", FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                        {
                            StarsLogFileReader.BaseStream.Position = counter;
                            ParseLog();
                            counter = StarsLogFileReader.BaseStream.Position;
                            Program.MainWindow.BeginInvoke(new System.Windows.Forms.MethodInvoker(delegate()
                            {
                                Program.MainWindow.ss_lb_parser.Text = "Parser line: " + counter;
                            }));
                        }
                        IsAlive = true;
                        System.Threading.Thread.Sleep(5000);
                    }
                });
                mParserThread.Name = "Parser Thread";
                mParserThread.IsBackground = true;
                mParserThread.Start();
                counter = 0;
                Logger.Write("Pokerstars parser is started!", Logger.L.DEBUG);
                return true;
            }
            catch (System.Exception e)
            {
                Logger.Write("Unable to create Parser! " + e.Message, Logger.L.FATAL);
                return false;
            }
        }

        public static void ResetCounter()
        {
            Logger.Write("Parser is resetted!", Logger.L.STATUS);
            counter = 0;
        }

        public static void ParseLog()
        {
            while (!StarsLogFileReader.EndOfStream)
            {
                string mLine = StarsLogFileReader.ReadLine();
                //Logger.Write("Parser succesfully readed line: " + mLine + ", count: " + counter, Logger.L.DEBUG);
                regex = new Regex(@"RT add ([0-9]{10}).*"); // If register
                Match m = regex.Match(mLine);
                if (m.Success)
                {
                    Logger.Write(mLine, Logger.L.DEBUG);
                    if (OnTournamentStatusChanged != null)
                    {
                        //OnTournamentStatusChanged(int.Parse(m.Groups[1].Value), TournamentState.REGISTERED);
                        
                    }
                    TaskManager.AddTask(new TaskTournamentRegister(int.Parse(m.Groups[1].Value)), TaskPriority.HIGH);
                }
                regex = new Regex(@"RT shown ([0-9]{10}).*"); // If open
                m = regex.Match(mLine);
                if (m.Success)
                {
                    Logger.Write(mLine, Logger.L.DEBUG);
                    if (OnTournamentStatusChanged != null)
                    {
                        //OnTournamentStatusChanged(int.Parse(m.Groups[1].Value), TournamentState.OPENED);
                       
                    }
                    TaskManager.AddTask(new TaskTournamentStateChange(int.Parse(m.Groups[1].Value), TournamentState.OPENED), TaskPriority.HIGH);
                }
                regex = new Regex(@"RT remove ([0-9]{10}).*"); // If close
                m = regex.Match(mLine);
                if (m.Success)
                {
                    Logger.Write(mLine, Logger.L.DEBUG);
                    if (OnTournamentStatusChanged != null)
                    {
                        //OnTournamentStatusChanged(int.Parse(m.Groups[1].Value), TournamentState.CLOSED);
                        
                    }
                    TaskManager.AddTask(new TaskTournamentStateChange(int.Parse(m.Groups[1].Value), TournamentState.CLOSED), TaskPriority.HIGH);
                }
                regex = new Regex(@"MSG_1003-M ([0-9]{10}) 00000000 error 13.*"); // If registration is closed
                m = regex.Match(mLine);
                if (m.Success)
                {
                    Logger.Write(mLine, Logger.L.DEBUG);
                    if (OnTournamentStatusChanged != null)
                    {
                        //OnTournamentStatusChanged(int.Parse(m.Groups[1].Value), TournamentState.ERROR);
                        
                    }
                    TaskManager.AddTask(new TaskTournamentRegister(int.Parse(m.Groups[1].Value)), TaskPriority.HIGH);
                    TaskManager.AddTask(new TaskTournamentStateChange(int.Parse(m.Groups[1].Value), TournamentState.ERROR), TaskPriority.HIGH);
                }
            }
        }
    }
}
