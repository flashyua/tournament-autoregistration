﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace AutoReg
{
    static class Statistics
    {
        private static string mBaseURL = "http://lugansktrams.org.ua/autoreg/addstat.php";
        public static void SendLaunchInfo()
        {
            string url = mBaseURL + "?mode=addlaunch&build=" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build +
                "&extinfo=NULL";
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse resp;
            try
            {
                resp = (HttpWebResponse)req.GetResponse();

                if (resp != null)
                {
                    //string XMLResponse;

                    //using (System.IO.StreamReader stream = new System.IO.StreamReader(
                    //    resp.GetResponseStream()))
                    //{
                    //    XMLResponse = stream.ReadToEnd();
                    //}
                    Logger.Write("Launch statistics succesfully sended!", Logger.L.DEBUG);
                }
                else
                {
                    Logger.Write("Launch statistics: NULL Response from server", Logger.L.DEBUG);
                }
            }
            catch
            {
                Logger.Write("System exception during sending statistics!", Logger.L.DEBUG);
            }
        }
    }
}
