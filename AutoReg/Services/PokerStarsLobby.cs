﻿//#define EMULATE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoReg
{
    static class PokerStarsLobby
    {
        public static IntPtr MainWindow;
        public static IntPtr TournamentListBox;
        public static IntPtr TournamentFilter;

        public static bool FindLobby()
        {
            // Поиск главного окна
            MainWindow = WinWrapper.FindWindow(null, "PokerStars Lobby");
            if (MainWindow == IntPtr.Zero)
            {
                Logger.Write("PokerStars lobby not found!", Logger.L.WARN, true);
                return false;
            }

            if (WinWrapper.IsIconic(MainWindow))
            {
                Logger.Write("PokerStars lobby is minimized!", Logger.L.WARN, true);
                return false;
            }

            // Проверка на ситнгоу фильтр - поиск кнопки фильтра
            if (Settings.User.IfCheckSNGTab)
            {
                IntPtr SNGFilter = IntPtr.Zero;
                do
                {
                    SNGFilter = WinWrapper.FindWindowEx(PokerStarsLobby.MainWindow, SNGFilter, null, null);
                    string capt = WinWrapper.GetText(SNGFilter);
                    if ((capt == "Sit & Go Tournament Filter") && WinWrapper.IsWindowVisible(SNGFilter))
                    {
                        break;
                    }
                } while (SNGFilter != IntPtr.Zero);

                if (SNGFilter == IntPtr.Zero)
                {
                    Logger.Write("Sit&Go tab is not selected!", Logger.L.WARN, true);
                    return false;
                }
            }

            // Проверка на список турниров
            TournamentListBox = IntPtr.Zero;
            do
            {

#if (EMULATE)
                TournamentListBox = WinWrapper.FindWindowEx(MainWindow, TournamentListBox, null, "PSListbox");
#else
                TournamentListBox = WinWrapper.FindWindowEx(MainWindow, TournamentListBox, "PokerStarsListClass", null);
#endif
                if (WinWrapper.IsWindowVisible(TournamentListBox))
                {
                    break;
                }
            } while (TournamentListBox != IntPtr.Zero);
            if (TournamentListBox == IntPtr.Zero)
            {
                Logger.Write("PokerStars tournament listbox not found!", Logger.L.WARN, true);
                return false;
            }

            
            // Проверка на окошко фильтра турнира
            TournamentFilter = IntPtr.Zero;
            do
            {
#if (EMULATE)
                TournamentFilter = WinWrapper.FindWindowEx(MainWindow, TournamentFilter, null, "Filter");
#else
                TournamentFilter = WinWrapper.FindWindowEx(MainWindow, TournamentFilter, "PokerStarsFilterClass", null);
#endif
                if (WinWrapper.IsWindowVisible(TournamentFilter))
                {
                    break;
                }
            } while (TournamentFilter != IntPtr.Zero);

            if (TournamentFilter == IntPtr.Zero)
            {
                Logger.Write("PokerStars tournament filter not found!", Logger.L.WARN, true);
                return false;
            }

            

            return true;
        }
    }
}
