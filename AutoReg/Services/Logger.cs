﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace AutoReg
{
    static class Logger
    {
        private const string filename = "log.txt";
        private static FileInfo file;
        private static StreamWriter writer;
        public static bool Enabled = false;
        private static Thread LogThread;
        private static List<string> MessageBuffer = new List<string>();
        private static bool dumpingBufferSemaphore = false;
        //public static LogBox logbox = new LogBox();
        private static int EventCounter = 0;

        public struct L
        {
            public const int STATUS = -1;
            public const int FATAL = 0;
            public const int MAIN = 1;
            public const int ERROR = 2;
            public const int WARN = 4;
            public const int INFO = 6;
            public const int DEBUG = 8;
        }
        private static int LogLevel = 8;

        public static void Start()
        {
            file = new FileInfo(filename);
            //if (file.Exists == true) file.Delete();
            if (file.Exists == true)
            {
                FileInfo logcopy = new FileInfo("old0." + filename);
                if (logcopy.Exists == true)
                {
                    FileInfo logcopy2 = new FileInfo("old1." + filename);
                    logcopy2.Delete();
                    File.Copy("old0." + filename, "old1." + filename);
                    logcopy.Delete();
                }
                File.Copy(filename, "old0." + filename);
                file.Delete();
            }
            Enabled = true;

            LogThread = new Thread(WriteBuffer);
            LogThread.IsBackground = true;
            LogThread.Priority = ThreadPriority.Lowest;
            LogThread.Start();

            string str = "###############################################################################\n" +
                "####################### Start logging #####################################################";
            //logbox.Show();
            Write(str, Logger.L.MAIN);
        }

        public static DialogResult Write(string str, int level, bool showMessage = false, bool showTrayMessage = false)
        {
            DialogResult dr = new DialogResult();
            dr = DialogResult.OK;
            if ((!Enabled)||(level > LogLevel)) return dr;
            while (dumpingBufferSemaphore)
            {
                Thread.Sleep(25);
            }

            /*logbox.BeginInvoke(new System.Windows.Forms.MethodInvoker(delegate()
            {
                logbox.appendText(str + Environment.NewLine);
            }));*/
            string strW = " \t" + str;
            strW = System.DateTime.Now.ToString() + " " +
                (System.DateTime.Now.Millisecond < 100 ? "0" + System.DateTime.Now.Millisecond.ToString() : System.DateTime.Now.Millisecond.ToString()) +
                "\t" + str + " \tThread: " + Thread.CurrentThread.ManagedThreadId.ToString() + "(" + Thread.CurrentThread.Name + ")";
            EventCounter++;
            MessageBuffer.Add(strW);
            
            switch (level)
            {
                case L.WARN: if(showMessage) dr = MessageBox.Show(str, "Warning!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    if (showTrayMessage)
                    {
                        Program.MainWindow.trayIcon.BalloonTipText = str;
                        Program.MainWindow.trayIcon.ShowBalloonTip(200);
                    }
                    break;
                case L.ERROR: if (showMessage) dr = MessageBox.Show(str, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case L.FATAL: dr = MessageBox.Show(str, "Fatal Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case L.STATUS: 
                    Program.MainWindow.tb_status.BeginInvoke(new MethodInvoker(delegate()
                        {
                            Program.MainWindow.tb_status.Text = Program.MainWindow.tb_status.Text + str + Environment.NewLine;
                            Program.MainWindow.tb_status.SelectionStart = Program.MainWindow.tb_status.Text.Length - 1;
                            Program.MainWindow.tb_status.ScrollToCaret();
                        })); break;
            };
            return dr;
        }

        private static void WriteBuffer()
        {
            while (true)
            {
                dumpingBufferSemaphore = true;
                if (MessageBuffer.Count != 0)
                {
                    
                    writer = file.AppendText();
                    while(MessageBuffer.Count != 0)
                    {
                        string str = MessageBuffer[0];
                        writer.WriteLine(str);
                        MessageBuffer.RemoveAt(0);
                    }
                    writer.Close();
                }
                dumpingBufferSemaphore = false;
                Thread.Sleep(500);
            }
        }

        public static void End()
        {
            //logbox.Show();
            //logbox.Dispose();
            Write("Ending log... " + EventCounter.ToString() +" records writed.", L.INFO);
            Enabled = false;
            while (MessageBuffer.Count != 0) { Thread.Sleep(100); };
            LogThread.Abort();
        }

        public static bool IsEnabled()
        {
            return Enabled;
        }

    }
}
