﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Xml;

namespace AutoReg
{
    public partial class Update : Form
    {
        private struct upFile
        {
            public string name;
            public int size;
            public string md5;
            public bool IfCompleteDownload;
        }

        private List<upFile> FileList = new List<upFile>();
        string url = "http://lugansktrams.org.ua/autoreg/";
        int totalSize = 0;
        int totalDownload = 0;

        public Update()
        {
            Logger.Write("Updating process...", Logger.L.DEBUG);
            InitializeComponent();
        }

        private void CheckingFile()
        {

        }

        private void Update_Load(object sender, EventArgs e)
        {

        }

        private void Update_Shown(object sender, EventArgs ea)
        {
            totalSize = 0;
            totalDownload = 0;
            lb_update.Text = "Receiving update list..";
            Logger.Write("Receiving update list..", Logger.L.DEBUG);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url + "update.php?mode=getfiles");
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                XmlDocument doc = new XmlDocument();
                string XMLResponse;

                using (System.IO.StreamReader stream = new System.IO.StreamReader(
                    response.GetResponseStream()))
                {
                    XMLResponse = stream.ReadToEnd();
                }
                doc.LoadXml(XMLResponse);

                
                foreach (XmlNode file in doc.ChildNodes[1]) // Getting response, files need to update
                {
                    lb_update.Text = "Receiving file info...";
                    upFile f = new upFile();
                    foreach (XmlAttribute attr in file.Attributes)
                    {
                        switch (attr.Name)
                        {
                            case "name": f.name = attr.Value; break;
                            case "size": f.size = int.Parse(attr.Value); totalSize += f.size; break;
                            case "md5": f.md5 = attr.Value; break;
                            default: break;
                        }
                    }
                    Logger.Write("\"" + f.name + "\" needs to update! (" + f.size + "KB, " + f.md5 + ")", Logger.L.DEBUG);
                    FileList.Add(f); // Adding to file list
                    lb_update.Text = "Receiving file info... Total size to download: " + totalSize/1024 + "KB";
                }
                pb_update.Value = 0;

                DownloadFile(FileList[0], 0);
            }
            catch (System.Net.WebException e)
            {
                Logger.Write("Cannot receive server answer! " + e.Message, Logger.L.ERROR);
            }
        }

        private void DownloadFile(upFile file, int fileIndex)
        {
            lb_update.Text = "Downloading file..." + file.name;
            Logger.Write("Downloading file..." + file.name, Logger.L.DEBUG);
            WebClient webCl = new WebClient();

            webCl.DownloadProgressChanged += (s, e) =>
            {
                pb_update.Value = (int)(((double)e.ProgressPercentage/100 * (double)file.size / (double)totalSize + (double)totalDownload/(double)totalSize)*100);
            };
            webCl.DownloadFileCompleted += (s, e) =>
            {
                Logger.Write("Downloading file " + file.name + " completed!", Logger.L.DEBUG);
                if (FileList.Count > fileIndex + 1)
                {
                    DownloadFile(FileList[fileIndex + 1], fileIndex + 1);
                }
                else
                {
                    Logger.Write("All files is downloaded!", Logger.L.DEBUG);
                    RunningUpdateBat();
                }
            };
            System.IO.Directory.CreateDirectory(Application.StartupPath + @"\update\");
            webCl.DownloadFileAsync(new Uri(url + file.name), Application.StartupPath + @"\update\" + file.name);
            while (webCl.IsBusy)
            {
                Application.DoEvents();
            }
        }

        private void RunningUpdateBat()
        {
            // running update.bat
            Logger.Write("Running update script", Logger.L.DEBUG);
            System.Diagnostics.ProcessStartInfo prInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c \"" + Application.StartupPath + @"\update\update.bat" + "\"");
            System.Diagnostics.Process updateBat = System.Diagnostics.Process.Start(prInfo);
            //Application.Exit();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
