﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Linq;

namespace AutoReg
{
    static class Configurator
    {
        
        private static XmlDocument doc;
        //public static Dictionary<string, string> Config = new Dictionary<string, string>();
        public static bool IsUpdating = false;
        public static string LoadedFiltersFile = "";
        
        public static void SaveFilters(ListView lw, string file)  // Сохранение фильтров в ХМЛ
        {
            doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("head"));
            for (int i = 0; i < lw.Items.Count; i++)
            {
                XmlNode node = doc.CreateElement("filter");
                doc.DocumentElement.AppendChild(node);

                XmlAttribute attr = doc.CreateAttribute("Name");
                attr.Value = lw.Items[i].Text;
                
                node.Attributes.Append(attr);

                attr = doc.CreateAttribute("IsActive");
                attr.Value = Session.mRegistratorPool[i].IsActive.ToString();
                node.Attributes.Append(attr);

                attr = doc.CreateAttribute("KeepMax");
                attr.Value = Session.mRegistratorPool[i].KeepMax.ToString();
                node.Attributes.Append(attr);

                attr = doc.CreateAttribute("MaxPerPass");
                attr.Value = Session.mRegistratorPool[i].MaxPerPass.ToString();
                node.Attributes.Append(attr);

                attr = doc.CreateAttribute("TotalMax");
                attr.Value = Session.mRegistratorPool[i].TotalMax.ToString();
                node.Attributes.Append(attr);

                attr = doc.CreateAttribute("SessionLimit");
                attr.Value = Session.mRegistratorPool[i].SessionLimit.ToString();
                node.Attributes.Append(attr);

                attr = doc.CreateAttribute("FilterDetails");
                attr.Value = Session.mRegistratorPool[i].Filter;
                node.Attributes.Append(attr);


            }

            doc.Save(file);
            doc = null;

        }

        public static bool LoadFilters(ListView lw, string file)  // Загрузка фильтров из ХМЛ
        {
            doc = new XmlDocument();
            try
            {
                doc.Load(file);
            }

            catch
            {
                Logger.Write("Load XML failed!", Logger.L.FATAL);
                return false;
            }

            IsUpdating = true;

            XmlNode rootNode = doc.ChildNodes[0];

            for (var i = lw.Items.Count - 1; i >= 0; i--)
            {
                lw.Items.RemoveAt(i);
                Session.RemoveAt(i);
            }

            bool loggerEnabled = Logger.Enabled; // Выключаем логгер на период загрузки деталей фильтра
            Logger.Write("Loading session, details is disabled", Logger.L.DEBUG);
            Logger.Enabled = false;
            for (int i = 0; i < rootNode.ChildNodes.Count; i++)
            {
                Registrator reg = Session.AddRegistrator();
                for (var j = 0; j < rootNode.ChildNodes[i].Attributes.Count; j++)
                {
                    string attrVal = rootNode.ChildNodes[i].Attributes[j].Value;
                    switch (rootNode.ChildNodes[i].Attributes[j].Name)
                    {
                        case "Name": reg.Name = attrVal; break;
                        case "IsActive": reg.IsActive = bool.Parse(attrVal); break;
                        case "KeepMax": reg.KeepMax = int.Parse(attrVal); break;
                        case "MaxPerPass": reg.MaxPerPass = int.Parse(attrVal); break;
                        case "TotalMax": reg.TotalMax = int.Parse(attrVal); break;
                        case "SessionLimit": reg.SessionLimit = int.Parse(attrVal); break;
                        case "FilterDetails": reg.Filter = attrVal; break;
                    }
                }
            }
            Session.KeepMax = (int)Program.MainWindow.nb_sesKeepMax.Value;
            Session.TotalMax = (int)Program.MainWindow.nb_sesMaxTotal.Value;
            Session.Load();
            Logger.Enabled = loggerEnabled;
            LoadedFiltersFile = file;
            IsUpdating = false;

            return true;
        }
    }
}
