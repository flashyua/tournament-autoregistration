﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO;

namespace AutoReg
{
    public partial class MainForm : Form
    {
        Thread CloseNotificationsThr;
        public StatisticsBox StatisticsBox = new StatisticsBox();

        public MainForm()
        {
            PokerStarsLobby.FindLobby();
            InitializeComponent();
            this.Text = "Tournament autoregistration " + Program.GetAssemblyInfo();
            AddOwnedForm(Program.OptionsWindow);
        }


        private void MainForm_Shown(object sender, EventArgs e)
        {
            
        }

        private void onExit()
        {
            Session.StopThread();
            trayIcon.Dispose();
            Logger.End();
            Settings.Save();
            Configurator.SaveFilters(lw_mainFilter, Configurator.LoadedFiltersFile);
            Application.Exit();
        }

        private void closeNotifications()
        {
            while (true)
            {
                if (Settings.User.IfCloseDialogs)
                {
                    IntPtr confWnd = IntPtr.Zero;
                    do
                    {
                        confWnd = WinWrapper.FindWindow(null, "Tournament Registration");
                        //Logger.Write("Confirmation... confWndHandle = " + confWnd.ToString(), Logger.L.DEBUG);
                        if (confWnd != IntPtr.Zero)
                        {
                            Logger.Write("Close \"Tournament Registration\" dialog!", Logger.L.DEBUG);
                            WinWrapper.SetForegroundWindow(confWnd);
                            WinWrapper.PressEnter();
                            Thread.Sleep(200);
                        }
                        else
                        {
                            Thread.Sleep(50);
                        }
                    } while (confWnd == IntPtr.Zero);
                    Thread.Sleep(100);
                }
                else
                {
                    Thread.Sleep(1000);
                }
                
            }
        }

        private void start_btn_Click(object sender, EventArgs e)
        {
            Session.Start();
        }

        private void stop_btn_Click(object sender, EventArgs e)
        {
            Session.Stop();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void enableLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Logger.IsEnabled() == false)
            {
                Logger.Start();
                this.enableLogToolStripMenuItem.Checked = true;
            }
            else
            {
                Logger.End();
                this.enableLogToolStripMenuItem.Checked = false;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            onExit();
        }

        private void aboutTournamentAutoregistrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About ab = new About();
            AddOwnedForm(ab);
            ab.ShowDialog();
        }

        private void domainUpDown1_SelectedItemChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void domainUpDown1_MouseDown(object sender, MouseEventArgs e)
        {
            MessageBox.Show("2");
        }

        private void domainUpDown1_KeyUp(object sender, KeyEventArgs e)
        {
            MessageBox.Show("2");
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            //Configurator.Read();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            //Configurator.Save();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = 0;
            if(lw_mainFilter.SelectedIndices.Count != 0)
            {
                index = lw_mainFilter.SelectedIndices[0];
                tb_shortName.Text = Session.mRegistratorPool[index].Name;
                tb_filterDetails.Text = Session.mRegistratorPool[index].Filter;
                nb_keepMax.Value = Session.mRegistratorPool[index].KeepMax;
                nb_maxCycle.Value = Session.mRegistratorPool[index].MaxPerPass;
                nb_totalMax.Value = Session.mRegistratorPool[index].TotalMax;
                nb_sessionLimit.Value = Session.mRegistratorPool[index].SessionLimit;
            }
            
        }

        private void bt_addRow_Click(object sender, EventArgs e)
        {
            Registrator reg = Session.AddRegistrator();
        }

        private void nb_sesMaxCount_ValueChanged_1(object sender, EventArgs e)
        {
            Session.KeepMax = (int)nb_sesKeepMax.Value;
        }

        private void bt_test_Click(object sender, EventArgs e)
        {

        }

        private void loadFilters(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = "xml";
            ofd.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            if(ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK) Configurator.LoadFilters(lw_mainFilter, ofd.FileName);
        }

        private void bt_testFilter_Click(object sender, EventArgs e)
        {
            try
            {

                Session.mRegistratorPool[lw_mainFilter.SelectedIndices[0]].ApplyFilter(2);
                WinWrapper.SetForegroundWindow(PokerStarsLobby.MainWindow);
            }
            catch
            {
                Logger.Write("Filter not selected or loaded!", Logger.L.WARN);
            }

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            onExit();
        }

        private void saveFilters(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = "xml";
            sfd.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK) Configurator.SaveFilters(lw_mainFilter, sfd.FileName);
        }

        private void filtersToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void bt_EditRow_Click(object sender, EventArgs e)
        {
            if (lw_mainFilter.SelectedIndices.Count != 0)
            {
                int index = lw_mainFilter.SelectedIndices[0];
                Session.mRegistratorPool[index].Name = tb_shortName.Text;
                Session.mRegistratorPool[index].Filter = tb_filterDetails.Text;
                Session.mRegistratorPool[index].MaxPerPass = (int)nb_maxCycle.Value;
                Session.mRegistratorPool[index].SessionLimit = (int)nb_sessionLimit.Value;
                Session.mRegistratorPool[index].KeepMax = (int)nb_keepMax.Value;
                Session.mRegistratorPool[index].TotalMax = (int)nb_totalMax.Value;
            }
            else
            {
                Logger.Write("Filter not selected!", Logger.L.WARN, true, false);
            }
        }

        private void bt_deleteRow_Click(object sender, EventArgs e)
        {
            if (lw_mainFilter.SelectedIndices.Count != 0)
            {
                Session.RemoveAt(lw_mainFilter.SelectedIndices[0]);
            }
            else
            {
                Logger.Write("Filter not selected!", Logger.L.WARN);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Registrator.TestClick();
        }

        private void nb_sesMaxTotal_ValueChanged(object sender, EventArgs e)
        {
            Session.TotalMax = (int)nb_sesMaxTotal.Value;
        }

        
        private void resetSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = new DialogResult();
            dr = MessageBox.Show("Are You really want to reset session parameters?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            {
                Session.Clear();
            }
        }

        private void unregisterAllTournamentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Session.UnregisterAll();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.OptionsWindow.ShowDialog();
        }

        private void cb_keepMax_CheckedChanged(object sender, EventArgs e)
        {
            nb_sesKeepMax.Enabled = Settings.Sess.IfKeepMaxTables = cb_keepMax.Checked;
        }

        private void cb_totalReg_CheckedChanged(object sender, EventArgs e)
        {
            nb_sesMaxTotal.Enabled = Settings.Sess.IfTotalMaxTables = cb_totalReg.Checked;
        }

        private void cb_minutes_CheckedChanged(object sender, EventArgs e)
        {
            nb_sesMinutes.Enabled = Settings.Sess.IfMinutesToSession = cb_minutes.Checked;
        }

        private void cb_upTime_CheckedChanged(object sender, EventArgs e)
        {
            dp_sesTime.Enabled = Settings.Sess.IfTimeToSession = cb_upTime.Checked;
        }

        private void nb_sesMinutes_ValueChanged(object sender, EventArgs e)
        {
            Settings.Sess.Minutes = (int)nb_sesMinutes.Value;
        }

        public void bt_pause_Click(object sender, EventArgs e)
        {
            Session.Pause();
        }

        private void lw_mainFilter_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if(Session.mRegistratorPool.Count > e.Item.Index) Session.mRegistratorPool[e.Item.Index].IsActive = e.Item.Checked;
        }

        public void SetSessionKeepNow(int _value)
        {
            lb_sesKeepNow.Text = _value.ToString();
        }

        public void SetSessionTotalNow(int _value)
        {
            lb_sesTotalNow.Text = _value.ToString();
        }

        private void lb_sesKeepNow_Click(object sender, EventArgs e)
        {

        }

        private void insertWarningInLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Logger.Write("======== USER WARNING!! ========", Logger.L.DEBUG);
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PokerstarsParser.Init();
        }

        private void writeLastReadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Logger.Write("Last parsed record: " + PokerstarsParser.CurrentLine, Logger.L.DEBUG);
        }

        private void checkAliveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PokerstarsParser.IsAlive = false;
            Thread.Sleep(500);
            Logger.Write("Pokerstars parser is alive: " + PokerstarsParser.IsAlive, Logger.L.STATUS);
        }

        private void trayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void trayIcon_MouseClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.BringToFront();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if ((WindowState == FormWindowState.Minimized)&&Settings.User.IfCloseToTray)
            {
                this.Hide();
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                Configurator.LoadFilters(lw_mainFilter, "filters.xml");
            }
            catch
            {
                Logger.Write("Unable to load default filter!", Logger.L.FATAL);
            };
            Session.Init();
            Session.Clear();
            PokerstarsParser.Init();
            //TaskManager.AddTask(new TaskRestartParser(), TaskPriority.HIGH);
            TaskManager.AddTimeTask(new TaskRestartParser(), DateTime.Today.AddDays(1).AddMilliseconds(50000));
            CloseNotificationsThr = new Thread(closeNotifications);
            Logger.Write("Starting close notifications...", Logger.L.DEBUG);

            CloseNotificationsThr.IsBackground = true;
            CloseNotificationsThr.Start();
            //MessageBox.Show();
            Statistics.SendLaunchInfo();

            Logger.Write("Loading settings...", Logger.L.DEBUG);
            //Configurator.LoadSettingsIntoForms();
            cb_keepMax.Checked = Settings.Sess.IfKeepMaxTables;
            cb_totalReg.Checked = Settings.Sess.IfTotalMaxTables;
            cb_minutes.Checked = Settings.Sess.IfMinutesToSession;
            cb_upTime.Checked = Settings.Sess.IfTimeToSession;
            nb_sesKeepMax.Value = Settings.Sess.KeepMax;
            nb_sesMaxTotal.Value = Settings.Sess.TotalMax;
            nb_sesMinutes.Value = Settings.Sess.Minutes;
            lb_sesKeepNow.Text = Session.KeepNow.ToString();
            lb_sesTotalNow.Text = Session.TotalNow.ToString();
        }

        private void showStatisticsWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (showStatisticsWindowToolStripMenuItem.Checked)
            {
                showStatisticsWindowToolStripMenuItem.Checked = false;
                StatisticsBox.Hide();
            }
            else
            {
                showStatisticsWindowToolStripMenuItem.Checked = true;
                StatisticsBox = new StatisticsBox();
                StatisticsBox.Owner = this;
                StatisticsBox.Show();
            }
        }

        private void resetCounterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PokerstarsParser.ResetCounter();
        }

    }
}