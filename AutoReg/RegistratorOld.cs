using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;

namespace AutoReg
{
    class RegistratorOld
    {
        // CLASS VARIABLES
        Thread thr;
        IntPtr mainLobbyWnd;
        WinWrapper.RECT mainLobbyRct;
        public string Filter;
        public List<int> currentlyRegistered = new List<int>();

        private int maxCount = -1;
        private int RegPerPass = -1;
        private int TotalLimit = -1;
        private int TotalRegistered = -1;

        public int listItem = -1;
        
        public struct Data
        {
            public int listItem;
            public List<int> registeredTournaments;
        }
        
        private static bool allowRegSemaphore = true; // �������, ����������� ����� �������� �����������
        
        // WINDOW COORDINATES CONST
        private const int WINDOWS_AERO_THICKNESS = 4;
        private const int LOBBY_X_SIZE = 808 + WINDOWS_AERO_THICKNESS*2;
        private const int LOBBY_Y_SIZE = 600 + WINDOWS_AERO_THICKNESS*2;
        private const int LOBBY_TEXTINPUT_X = 334 + WINDOWS_AERO_THICKNESS;
        private const int LOBBY_TEXTINPUT_Y = 228 + WINDOWS_AERO_THICKNESS;
        private const int LOBBY_TEXTINPUT_CLEAR_X = 440 + WINDOWS_AERO_THICKNESS;
        //private const int FIRST_TOURNAMENT_IN_LIST_X = 86;
        //private const int FIRST_TOURNAMENT_IN_LIST_Y = 384;
        private const int FIRST_TOURNAMENT_IN_LIST_X = 58 + WINDOWS_AERO_THICKNESS;
        private const int FIRST_TOURNAMENT_IN_LIST_Y = 276 + WINDOWS_AERO_THICKNESS;
        private const int REGISTER_BUTTON_X = 610 + WINDOWS_AERO_THICKNESS;
        private const int REGISTER_BUTTON_Y = 504 + WINDOWS_AERO_THICKNESS;

        // CONFIGURATION VARIABLES
        

        public RegistratorOld()
        {
            
        }

        private IntPtr FindLobby()
        {
            IntPtr t = WinWrapper.FindWindow(null, "PokerStars Lobby"); // ����� �������� ���� �������
            if (t == IntPtr.Zero) // ���� ������
            {
                ErrorMessager.LobbyNotFound();

                return IntPtr.Zero;
            }

            // �������� ����������� �������
            if (!WinWrapper.GetWindowRect(t, out mainLobbyRct)) // ����������� ��������� ����
            {
                ErrorMessager.CustomMessage("Cannot detect Main Lobby rectangle!");
                Stop();
                return IntPtr.Zero;
            }
            return t;
        }

        public bool Start(
            string shortName,
            string filter,
            int max_Count,
            int maxCountPerCycle,
            int lItem)
        {
            RegPerPass = maxCountPerCycle;
            listItem = lItem;
            loadSavedData(lItem);
            maxCount = max_Count;

            mainLobbyWnd = FindLobby();
            if (mainLobbyWnd == IntPtr.Zero) // ���� ������
            {
                return false;
            }
            Filter = filter;            
            Logger.Write("Registration started!");

            thr = new Thread(Register);
            thr.Start(); // ��������� ����� �����������

            return true;
        }

        public void loadSavedData(int lItem)
        {
            RegistratorOld.Data data = Configurator.LoadRegistratorData(lItem);
            currentlyRegistered = data.registeredTournaments;
        }

        public bool Stop()
        {
            try { thr.Abort(); }
            catch { };
            Logger.Write("Registration stopped at reg" + listItem + "!");
            
            return true;
        }

        private void Register() // ���� ���� �����������
        {
            while (thr.IsAlive)
            {
                while (!allowRegSemaphore)
                {
                    Thread.Sleep(100);
                }
                int c = getCurrentlyOpenedTournaments();
                if (getCurrentlyOpenedTournaments() >= maxCount + 1) break; // +1 ������ ��� ������� �� 1 ������ ��� ��������

                allowRegSemaphore = false;
                //WinWrapper.MaximizeWindow(mainLobbyWnd);
                Thread.Sleep(100);
                // �������� ����������� �������
                if (!WinWrapper.GetWindowRect(mainLobbyWnd, out mainLobbyRct)) // ����������� ��������� ����
                {
                    ErrorMessager.CustomMessage("Cannot detect Main Lobby rectangle!");
                    Stop();
                }

                if (((mainLobbyRct.Right - mainLobbyRct.Left) != LOBBY_X_SIZE) || ((mainLobbyRct.Bottom - mainLobbyRct.Top) != LOBBY_Y_SIZE)) // ���� ����� �� ��������������
                {
                    ErrorMessager.CustomMessage("Lobby is not minimized!");
                    Stop();
                }
                //MessageBox.Show(size.X + "x" + size.Y); // 816x608
                bool f = false;
                do
                {
                    f = TestFilter(Filter);
                } while (!f);

                for (int i = 0; i < RegPerPass; i++) // ���������� ��������, �������������� �� ���
                {
                    int tID = RegisterOneTournament(i);
                    currentlyRegistered.Add(tID);
                    TotalRegistered++;
                    Logger.Write("Tournament ID " + tID.ToString());
                    //Thread.Sleep(500);
                }
                //WinWrapper.MinimizeWindow(mainLobbyWnd);
                allowRegSemaphore = true;

                
                Configurator.SaveRegistratorData(listItem, currentlyRegistered); // ����� ���������� �� ����

                Thread.Sleep(10000);
            }
            
        }

        private int RegisterOneTournament(int offset) // ����� ������ �������
        {
            WinWrapper.SetForegroundWindow(mainLobbyWnd);
            WinWrapper.LeftMouseClick(new Point(
                mainLobbyRct.Left + FIRST_TOURNAMENT_IN_LIST_X,
                mainLobbyRct.Top + FIRST_TOURNAMENT_IN_LIST_Y + offset*15)); // ����� ������� � ������

            // �������� �����������
            IntPtr t = IntPtr.Zero;
            int tID = -1;
            do
            {
                t = WinWrapper.FindWindowEx(mainLobbyWnd, t, null, null);
                if ((WinWrapper.GetText(t) == "Register") && (WinWrapper.IsWindowVisible(t)))
                {
                    ConfirmRegistration();
                    tID = GetTournamentID(offset);
                }
                else if ((WinWrapper.GetText(t) == "Unregister") && (WinWrapper.IsWindowVisible(t)))
                {
                    Logger.Write("Tournament already registered by filter: " + Filter + "!");
                }
            } while (t != IntPtr.Zero);

            return tID;
        }

        private void ConfirmRegistration() // ������������� ����������� � ����� �������
        {
            WinWrapper.LeftMouseClick(new Point(
                mainLobbyRct.Left + REGISTER_BUTTON_X,
                mainLobbyRct.Top + REGISTER_BUTTON_Y)); // ���� �� ������ �����������
           // WinWrapper.LeftMouseClick(new Point(REGISTER_BUTTON_X,REGISTER_BUTTON_Y), mainLobbyWnd); // ���� �� ������ �����������


            //CloseDialog(); // ������������� �������� ����
            //Thread.Sleep(200);
            //CloseDialog(); // ������������� ���������� ����
            
            Logger.Write("Tournament registered by filter: " + Filter + "!");
        }

        private void CloseDialog()  // �������� ����������� ���� �������������
        {
            IntPtr tournRegConfirmWnd = IntPtr.Zero;
            do
            {                                    // ����� ���� � �������������� �����������
                tournRegConfirmWnd = WinWrapper.FindWindow(null, "Tournament Registration");
                Thread.Sleep(100);

            } while (tournRegConfirmWnd == IntPtr.Zero);

            WinWrapper.SetForegroundWindow(tournRegConfirmWnd);

            WinWrapper.RECT rct;
            if (!WinWrapper.GetWindowRect(tournRegConfirmWnd, out rct))
            {
                MessageBox.Show("ERROR");
                return;
            }

            WinWrapper.PressEnter();
            
        }

        public bool TestFilter(string filter)
        {
            mainLobbyWnd = FindLobby();
            if (!WinWrapper.GetWindowRect(mainLobbyWnd, out mainLobbyRct)) // ����������� ��������� ����
            {
                ErrorMessager.CustomMessage("Cannot detect Main Lobby rectangle!");
                Stop();
            }
            WinWrapper.LeftMouseClick(new Point(
                mainLobbyRct.Left + LOBBY_TEXTINPUT_CLEAR_X,
                mainLobbyRct.Top + LOBBY_TEXTINPUT_Y));
           WinWrapper.LeftMouseClick(new Point(
                mainLobbyRct.Left + LOBBY_TEXTINPUT_X,
                mainLobbyRct.Top + LOBBY_TEXTINPUT_Y));

            WinWrapper.SetForegroundWindow(mainLobbyWnd);
            Thread.Sleep(50);
            SendKeys.SendWait(filter);

            IntPtr t = IntPtr.Zero;

            t = WinWrapper.FindWindowEx(mainLobbyWnd, t, "PokerStarsFilterClass", null);
            string c = WinWrapper.GetText(t);
            if (c == filter) return true;
            else return false;

        }

        public int GetTournamentID(int offset) // ��������� ����� � ������������ �� �������
        {
            mainLobbyWnd = FindLobby();
            WinWrapper.SetForegroundWindow(mainLobbyWnd);
            WinWrapper.LeftMouseDblClick(new Point(
                mainLobbyRct.Left + FIRST_TOURNAMENT_IN_LIST_X,
                mainLobbyRct.Top + FIRST_TOURNAMENT_IN_LIST_Y + offset * 15)); // ����� ������� ������� � ������

            Thread.Sleep(200);
            IntPtr t = IntPtr.Zero;
            int trnmtID = 0;
            IntPtr desktopWnd = WinWrapper.GetDesktopWindow();
            do
            {
                t = WinWrapper.FindWindowEx(desktopWnd, t, "#32770", null);
                string capt = WinWrapper.GetText(t);
                if ((capt.Length>9)&&(capt != ""))
                {
                    string[] c = capt.Split(new Char[] { ' ' });
                    
                    if (c[0] == "Tournament")
                    {
                        trnmtID = Convert.ToInt32(c[1]);
                        break;
                    }
                }
                
            } while (t != IntPtr.Zero);

            WinWrapper.SetForegroundWindow(t);
            WinWrapper.CloseWindow(t);
            return trnmtID;
        }

        public List<int> currentlyOpenedTournaments()
        {
            List<int> tournArr = new List<int>();
            /* �� ������ ���̨�...
             * Regex regex = new Regex(    @"[$]"+  // $
                                        "([0-9.]*)[ ]"+  // �����
                                        "([A-Z]{2})[ ]" +  // �����
                                        "([A-Za-z']*)[ ]" + // ����
                                        "[[]([0-9]{2}) Players"  // ���������� �������
                                        );
             * */
            Regex regex = new Regex(@"[A-Za-z0-9.'\[\]$\/ \-]*Tournament ([0-9]*).*");

            
            IntPtr t = IntPtr.Zero;
            int trnmtID = 0;
            IntPtr desktopWnd = WinWrapper.GetDesktopWindow();
            do
            {
                t = WinWrapper.FindWindowEx(desktopWnd, t, "PokerStarsTableFrameClass", null);
                string capt = WinWrapper.GetText(t);
                if (capt != "")
                {
                    if (capt.IndexOf('P') == -1) // �������� ��� �������, �� �����. ����������� �� ���� ���� ������� 
                    {
                        break;
                    }
                    else if (regex.IsMatch(capt))
                    {
                        Match match = regex.Match(capt);
                        trnmtID = Convert.ToInt32(match.Groups[1].Value);
                        tournArr.Add(trnmtID);
                        //int i = 1;
                    }
                }

            } while (t != IntPtr.Zero);
            
            //string str = "$1.50 NL Hold'em [90 Players, Knockout] - Blinds $600/$1200 Ante $125 - Tournament 1021021020 Table 9";
            //bool r = regex.IsMatch(str);
            //Match match = regex.Match(str);
            /*str = match.Groups[1].Value;
            double t_buyIn = Convert.ToDouble(((str.IndexOf('.') != -1) ? str=str.Replace('.', ',') : str));
            string t_limit = match.Groups[2].Value;
            string t_game = match.Groups[3].Value;
            int t_players = Convert.ToInt32(match.Groups[4].Value);*/
            //int tID = Convert.ToInt32(match.Groups[1].Value);

            return tournArr;
        }

        public int getCurrentlyOpenedTournaments()
        {
            int curTourn = 0;
            List<int> curOpen = currentlyOpenedTournaments();
            if (currentlyRegistered.Count == 0) return 0;
            foreach (int reg in currentlyRegistered)
            {
                //bool isAlive = false;
                foreach (int op in curOpen)
                {
                    
                    if (op == reg)
                    {
                        curTourn++;
                        //isAlive = true;
                    }
                }
                //if(!isAlive) currentlyRegistered.  // ����� �����-������ ����� ��� ��� �������� ������������� �������
            }


            return curTourn;
        }
    }
}
