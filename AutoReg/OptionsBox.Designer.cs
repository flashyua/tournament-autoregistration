﻿namespace AutoReg
{
    partial class OptionsBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionsBox));
            this.tc_options = new System.Windows.Forms.TabControl();
            this.General = new System.Windows.Forms.TabPage();
            this.cb_checkSNGTab = new System.Windows.Forms.CheckBox();
            this.tb_regLatency = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cb_minimizeToTray = new System.Windows.Forms.CheckBox();
            this.cb_fixCtrlA = new System.Windows.Forms.CheckBox();
            this.cb_filterViaClipboard = new System.Windows.Forms.CheckBox();
            this.cb_loadTournamentInfo = new System.Windows.Forms.CheckBox();
            this.cb_loadFiltersStartup = new System.Windows.Forms.CheckBox();
            this.cb_closeNotifications = new System.Windows.Forms.CheckBox();
            this.Session = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_continueReg = new System.Windows.Forms.TextBox();
            this.tb_unregAt = new System.Windows.Forms.TextBox();
            this.tb_stopRegAt = new System.Windows.Forms.TextBox();
            this.cb_unregisterAll = new System.Windows.Forms.CheckBox();
            this.cb_stopRegisteringAt = new System.Windows.Forms.CheckBox();
            this.Account = new System.Windows.Forms.TabPage();
            this.tb_psSettingFolder = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bt_cancel = new System.Windows.Forms.Button();
            this.bt_save = new System.Windows.Forms.Button();
            this.tc_options.SuspendLayout();
            this.General.SuspendLayout();
            this.Session.SuspendLayout();
            this.Account.SuspendLayout();
            this.SuspendLayout();
            // 
            // tc_options
            // 
            this.tc_options.Controls.Add(this.General);
            this.tc_options.Controls.Add(this.Session);
            this.tc_options.Controls.Add(this.Account);
            this.tc_options.Location = new System.Drawing.Point(12, 12);
            this.tc_options.Name = "tc_options";
            this.tc_options.SelectedIndex = 0;
            this.tc_options.Size = new System.Drawing.Size(558, 270);
            this.tc_options.TabIndex = 0;
            // 
            // General
            // 
            this.General.BackColor = System.Drawing.SystemColors.Control;
            this.General.Controls.Add(this.cb_checkSNGTab);
            this.General.Controls.Add(this.tb_regLatency);
            this.General.Controls.Add(this.label4);
            this.General.Controls.Add(this.cb_minimizeToTray);
            this.General.Controls.Add(this.cb_fixCtrlA);
            this.General.Controls.Add(this.cb_filterViaClipboard);
            this.General.Controls.Add(this.cb_loadTournamentInfo);
            this.General.Controls.Add(this.cb_loadFiltersStartup);
            this.General.Controls.Add(this.cb_closeNotifications);
            this.General.Location = new System.Drawing.Point(4, 22);
            this.General.Name = "General";
            this.General.Padding = new System.Windows.Forms.Padding(3);
            this.General.Size = new System.Drawing.Size(550, 244);
            this.General.TabIndex = 0;
            this.General.Text = "General";
            // 
            // cb_checkSNGTab
            // 
            this.cb_checkSNGTab.AutoSize = true;
            this.cb_checkSNGTab.Location = new System.Drawing.Point(151, 110);
            this.cb_checkSNGTab.Name = "cb_checkSNGTab";
            this.cb_checkSNGTab.Size = new System.Drawing.Size(101, 17);
            this.cb_checkSNGTab.TabIndex = 17;
            this.cb_checkSNGTab.Text = "Check SNG tab";
            this.cb_checkSNGTab.UseVisualStyleBackColor = true;
            this.cb_checkSNGTab.CheckedChanged += new System.EventHandler(this.cb_checkSNGTab_CheckedChanged);
            // 
            // tb_regLatency
            // 
            this.tb_regLatency.Location = new System.Drawing.Point(133, 146);
            this.tb_regLatency.Name = "tb_regLatency";
            this.tb_regLatency.Size = new System.Drawing.Size(69, 20);
            this.tb_regLatency.TabIndex = 16;
            this.tb_regLatency.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Registration latency:";
            // 
            // cb_minimizeToTray
            // 
            this.cb_minimizeToTray.AutoSize = true;
            this.cb_minimizeToTray.Location = new System.Drawing.Point(18, 110);
            this.cb_minimizeToTray.Name = "cb_minimizeToTray";
            this.cb_minimizeToTray.Size = new System.Drawing.Size(98, 17);
            this.cb_minimizeToTray.TabIndex = 14;
            this.cb_minimizeToTray.Text = "Minimize to tray";
            this.cb_minimizeToTray.UseVisualStyleBackColor = true;
            this.cb_minimizeToTray.CheckedChanged += new System.EventHandler(this.cb_minimizeToTray_CheckedChanged);
            // 
            // cb_fixCtrlA
            // 
            this.cb_fixCtrlA.AutoSize = true;
            this.cb_fixCtrlA.Location = new System.Drawing.Point(151, 87);
            this.cb_fixCtrlA.Name = "cb_fixCtrlA";
            this.cb_fixCtrlA.Size = new System.Drawing.Size(110, 17);
            this.cb_fixCtrlA.TabIndex = 14;
            this.cb_fixCtrlA.Text = "Fix Ctrl+A problem";
            this.cb_fixCtrlA.UseVisualStyleBackColor = true;
            this.cb_fixCtrlA.CheckedChanged += new System.EventHandler(this.cb_fixCtrlA_CheckedChanged);
            // 
            // cb_filterViaClipboard
            // 
            this.cb_filterViaClipboard.AutoSize = true;
            this.cb_filterViaClipboard.Checked = true;
            this.cb_filterViaClipboard.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_filterViaClipboard.Location = new System.Drawing.Point(18, 87);
            this.cb_filterViaClipboard.Name = "cb_filterViaClipboard";
            this.cb_filterViaClipboard.Size = new System.Drawing.Size(127, 17);
            this.cb_filterViaClipboard.TabIndex = 14;
            this.cb_filterViaClipboard.Text = "Set filter via clipboard";
            this.cb_filterViaClipboard.UseVisualStyleBackColor = true;
            this.cb_filterViaClipboard.CheckedChanged += new System.EventHandler(this.cb_filterViaClipboard_CheckedChanged);
            // 
            // cb_loadTournamentInfo
            // 
            this.cb_loadTournamentInfo.AutoSize = true;
            this.cb_loadTournamentInfo.Checked = true;
            this.cb_loadTournamentInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_loadTournamentInfo.Location = new System.Drawing.Point(18, 64);
            this.cb_loadTournamentInfo.Name = "cb_loadTournamentInfo";
            this.cb_loadTournamentInfo.Size = new System.Drawing.Size(163, 17);
            this.cb_loadTournamentInfo.TabIndex = 13;
            this.cb_loadTournamentInfo.Text = "Load tournament info if exists";
            this.cb_loadTournamentInfo.UseVisualStyleBackColor = true;
            this.cb_loadTournamentInfo.CheckedChanged += new System.EventHandler(this.cb_loadTournamentInfo_CheckedChanged);
            // 
            // cb_loadFiltersStartup
            // 
            this.cb_loadFiltersStartup.AutoSize = true;
            this.cb_loadFiltersStartup.Checked = true;
            this.cb_loadFiltersStartup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_loadFiltersStartup.Location = new System.Drawing.Point(18, 41);
            this.cb_loadFiltersStartup.Name = "cb_loadFiltersStartup";
            this.cb_loadFiltersStartup.Size = new System.Drawing.Size(162, 17);
            this.cb_loadFiltersStartup.TabIndex = 13;
            this.cb_loadFiltersStartup.Text = "Load default filters on startup";
            this.cb_loadFiltersStartup.UseVisualStyleBackColor = true;
            this.cb_loadFiltersStartup.CheckedChanged += new System.EventHandler(this.cb_loadFiltersStartup_CheckedChanged);
            // 
            // cb_closeNotifications
            // 
            this.cb_closeNotifications.AutoSize = true;
            this.cb_closeNotifications.Location = new System.Drawing.Point(18, 18);
            this.cb_closeNotifications.Name = "cb_closeNotifications";
            this.cb_closeNotifications.Size = new System.Drawing.Size(230, 17);
            this.cb_closeNotifications.TabIndex = 13;
            this.cb_closeNotifications.Text = "Close all \"Tournament Registration\" dialogs";
            this.cb_closeNotifications.UseVisualStyleBackColor = true;
            this.cb_closeNotifications.CheckedChanged += new System.EventHandler(this.cb_closeNotifications_CheckedChanged);
            // 
            // Session
            // 
            this.Session.Controls.Add(this.label2);
            this.Session.Controls.Add(this.label1);
            this.Session.Controls.Add(this.tb_continueReg);
            this.Session.Controls.Add(this.tb_unregAt);
            this.Session.Controls.Add(this.tb_stopRegAt);
            this.Session.Controls.Add(this.cb_unregisterAll);
            this.Session.Controls.Add(this.cb_stopRegisteringAt);
            this.Session.Location = new System.Drawing.Point(4, 22);
            this.Session.Name = "Session";
            this.Session.Size = new System.Drawing.Size(550, 244);
            this.Session.TabIndex = 2;
            this.Session.Text = "Session";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(181, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "minutes, continue at";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(181, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "minutes";
            // 
            // tb_continueReg
            // 
            this.tb_continueReg.Location = new System.Drawing.Point(289, 39);
            this.tb_continueReg.Name = "tb_continueReg";
            this.tb_continueReg.Size = new System.Drawing.Size(40, 20);
            this.tb_continueReg.TabIndex = 1;
            this.tb_continueReg.TextChanged += new System.EventHandler(this.tb_unregAt_TextChanged);
            // 
            // tb_unregAt
            // 
            this.tb_unregAt.Location = new System.Drawing.Point(135, 39);
            this.tb_unregAt.Name = "tb_unregAt";
            this.tb_unregAt.Size = new System.Drawing.Size(40, 20);
            this.tb_unregAt.TabIndex = 1;
            this.tb_unregAt.TextChanged += new System.EventHandler(this.tb_unregAt_TextChanged);
            // 
            // tb_stopRegAt
            // 
            this.tb_stopRegAt.Enabled = false;
            this.tb_stopRegAt.Location = new System.Drawing.Point(135, 16);
            this.tb_stopRegAt.Name = "tb_stopRegAt";
            this.tb_stopRegAt.Size = new System.Drawing.Size(40, 20);
            this.tb_stopRegAt.TabIndex = 1;
            // 
            // cb_unregisterAll
            // 
            this.cb_unregisterAll.AutoSize = true;
            this.cb_unregisterAll.Location = new System.Drawing.Point(18, 41);
            this.cb_unregisterAll.Name = "cb_unregisterAll";
            this.cb_unregisterAll.Size = new System.Drawing.Size(99, 17);
            this.cb_unregisterAll.TabIndex = 0;
            this.cb_unregisterAll.Text = "Unregister all at";
            this.cb_unregisterAll.UseVisualStyleBackColor = true;
            this.cb_unregisterAll.CheckedChanged += new System.EventHandler(this.cb_unregisterAll_CheckedChanged);
            // 
            // cb_stopRegisteringAt
            // 
            this.cb_stopRegisteringAt.AutoSize = true;
            this.cb_stopRegisteringAt.Enabled = false;
            this.cb_stopRegisteringAt.Location = new System.Drawing.Point(18, 18);
            this.cb_stopRegisteringAt.Name = "cb_stopRegisteringAt";
            this.cb_stopRegisteringAt.Size = new System.Drawing.Size(111, 17);
            this.cb_stopRegisteringAt.TabIndex = 0;
            this.cb_stopRegisteringAt.Text = "Stop registering at";
            this.cb_stopRegisteringAt.UseVisualStyleBackColor = true;
            // 
            // Account
            // 
            this.Account.BackColor = System.Drawing.SystemColors.Control;
            this.Account.Controls.Add(this.tb_psSettingFolder);
            this.Account.Controls.Add(this.label3);
            this.Account.Location = new System.Drawing.Point(4, 22);
            this.Account.Name = "Account";
            this.Account.Size = new System.Drawing.Size(550, 244);
            this.Account.TabIndex = 1;
            this.Account.Text = "Account";
            // 
            // tb_psSettingFolder
            // 
            this.tb_psSettingFolder.Location = new System.Drawing.Point(141, 6);
            this.tb_psSettingFolder.Name = "tb_psSettingFolder";
            this.tb_psSettingFolder.Size = new System.Drawing.Size(395, 20);
            this.tb_psSettingFolder.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Pokerstars setting folder:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // bt_cancel
            // 
            this.bt_cancel.Location = new System.Drawing.Point(193, 292);
            this.bt_cancel.Name = "bt_cancel";
            this.bt_cancel.Size = new System.Drawing.Size(100, 30);
            this.bt_cancel.TabIndex = 1;
            this.bt_cancel.Text = "Cancel";
            this.bt_cancel.UseVisualStyleBackColor = true;
            this.bt_cancel.Click += new System.EventHandler(this.bt_cancel_Click);
            // 
            // bt_save
            // 
            this.bt_save.Location = new System.Drawing.Point(299, 292);
            this.bt_save.Name = "bt_save";
            this.bt_save.Size = new System.Drawing.Size(100, 30);
            this.bt_save.TabIndex = 1;
            this.bt_save.Text = "Save";
            this.bt_save.UseVisualStyleBackColor = true;
            this.bt_save.Click += new System.EventHandler(this.bt_save_Click);
            // 
            // OptionsBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 334);
            this.Controls.Add(this.bt_save);
            this.Controls.Add(this.bt_cancel);
            this.Controls.Add(this.tc_options);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsBox";
            this.Text = "Options";
            this.tc_options.ResumeLayout(false);
            this.General.ResumeLayout(false);
            this.General.PerformLayout();
            this.Session.ResumeLayout(false);
            this.Session.PerformLayout();
            this.Account.ResumeLayout(false);
            this.Account.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tc_options;
        private System.Windows.Forms.TabPage General;
        private System.Windows.Forms.TabPage Account;
        private System.Windows.Forms.TabPage Session;
        private System.Windows.Forms.Button bt_cancel;
        private System.Windows.Forms.Button bt_save;
        public System.Windows.Forms.CheckBox cb_loadFiltersStartup;
        public System.Windows.Forms.CheckBox cb_closeNotifications;
        public System.Windows.Forms.CheckBox cb_loadTournamentInfo;
        public System.Windows.Forms.CheckBox cb_unregisterAll;
        public System.Windows.Forms.CheckBox cb_stopRegisteringAt;
        public System.Windows.Forms.TextBox tb_unregAt;
        public System.Windows.Forms.TextBox tb_stopRegAt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox tb_continueReg;
        public System.Windows.Forms.TextBox tb_psSettingFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cb_filterViaClipboard;
        private System.Windows.Forms.TextBox tb_regLatency;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cb_minimizeToTray;
        private System.Windows.Forms.CheckBox cb_fixCtrlA;
        private System.Windows.Forms.CheckBox cb_checkSNGTab;
    }
}