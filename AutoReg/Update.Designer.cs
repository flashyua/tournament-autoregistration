﻿namespace AutoReg
{
    partial class Update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pb_update = new System.Windows.Forms.ProgressBar();
            this.lb_update = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pb_update
            // 
            this.pb_update.Location = new System.Drawing.Point(12, 35);
            this.pb_update.Name = "pb_update";
            this.pb_update.Size = new System.Drawing.Size(403, 20);
            this.pb_update.TabIndex = 0;
            // 
            // lb_update
            // 
            this.lb_update.Location = new System.Drawing.Point(12, 14);
            this.lb_update.Name = "lb_update";
            this.lb_update.Size = new System.Drawing.Size(403, 18);
            this.lb_update.TabIndex = 1;
            this.lb_update.Text = "label1";
            this.lb_update.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 68);
            this.Controls.Add(this.lb_update);
            this.Controls.Add(this.pb_update);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Update";
            this.Text = "Update";
            this.Load += new System.EventHandler(this.Update_Load);
            this.Shown += new System.EventHandler(this.Update_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar pb_update;
        private System.Windows.Forms.Label lb_update;
    }
}