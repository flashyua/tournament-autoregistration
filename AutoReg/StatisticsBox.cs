﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutoReg
{
    public partial class StatisticsBox : Form
    {
        public StatisticsBox()
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            lb_keepNowStat.Text = Session.KeepNow.ToString();
            lb_totalNowStat.Text = Session.TotalNow.ToString();
        }

        private void StatisticsBox_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.MainWindow.showStatisticsWindowToolStripMenuItem.Checked = false;
        }
    }
}
