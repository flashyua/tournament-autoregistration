﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;
using System.Net;
using System.Xml;



namespace AutoReg
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        public static MainForm MainWindow;
        public static OptionsBox OptionsWindow;
        [STAThread]
        static void Main()
        {
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Logger.Start();
            Logger.Write("Current version is " + GetAssemblyInfo(), Logger.L.DEBUG);
            
            //if (Settings.Load())
            if(!Settings.Load())
            {
                Logger.Write("Unable to load config!", Logger.L.FATAL);
                Application.Exit();
                return;
            }
            MainWindow = new MainForm();
            OptionsWindow = new OptionsBox();
            
            //MessageBox.Show(Environment.);
            if (CheckUpdates())
            //if (false)
            {
                Update UpdateW = new Update();
                Application.Run(UpdateW);
            }
            else
            {
                Application.Run(MainWindow);
            }
            //
            Application.ApplicationExit += new EventHandler(onApplicationExit);
        }

        public static string GetAssemblyInfo()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            return Assembly.GetExecutingAssembly().GetName().Version.Major.ToString() + "." +
            Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString() + " (build " +
            Assembly.GetExecutingAssembly().GetName().Version.Build.ToString() + ")";
        }

        private static void onApplicationExit(object sender, EventArgs e)
        {
            //f1.onExit();
        }

        private static bool CheckUpdates()
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://lugansktrams.org.ua/autoreg/update.php?mode=getversion");
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                XmlDocument doc = new XmlDocument();
                string XMLResponse;

                using (System.IO.StreamReader stream = new System.IO.StreamReader(
                    response.GetResponseStream()))
                {
                    XMLResponse = stream.ReadToEnd();
                }
                doc.LoadXml(XMLResponse);

                int serverBuild = int.Parse(doc.ChildNodes[1].ChildNodes[0].Attributes[1].Value);
                int serverRevision = int.Parse(doc.ChildNodes[1].ChildNodes[0].Attributes[2].Value);
                int build = Assembly.GetExecutingAssembly().GetName().Version.Build;
                int revision = Assembly.GetExecutingAssembly().GetName().Version.Revision;

                if ((serverBuild > build) || ( (serverBuild == build) && (serverRevision > revision)))
                {
                    Logger.Write("Update " + doc.ChildNodes[1].ChildNodes[0].Attributes[0].Value + " is on server!", Logger.L.DEBUG);
                    DialogResult dr = MessageBox.Show("A new version of tournament registrator is " +
                    doc.ChildNodes[1].ChildNodes[0].Attributes[0].Value +
                    ". Are You want to update?", "Checking update", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                    if (dr == DialogResult.OK)
                    {
                        return true;
                    }
                    else if (dr == DialogResult.Cancel)
                    {
                        if (bool.Parse(doc.ChildNodes[1].ChildNodes[0].Attributes[2].Value))
                        {
                            Logger.Write("It's a critical update!", Logger.L.DEBUG);
                            DialogResult drc = MessageBox.Show("This is critical update! Ignoring is impossible!", "Error!", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);

                            if (drc == DialogResult.Retry)
                            {
                                Logger.Write("Retrying...", Logger.L.DEBUG);
                                CheckUpdates();
                                return true;
                            }
                            else
                            {
                                Logger.Write("Exiting...", Logger.L.DEBUG);
                                Environment.Exit(0);
                                return false;
                            }
                        }
                        else return false;
                    }
                }
                else
                {
                    Logger.Write("There no updates on server.", Logger.L.DEBUG);
                }
            }
            catch
            {
                Logger.Write("Cannot connect to server! Check your internet connection or firewall settings.", Logger.L.ERROR, true);
            }
            
            return false;
        }

        private static void DownloadUpdate()
        {

        }
    }
}