﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace AutoReg
{
    static class Settings
    {
        public struct UserSettings
        {
            public int ContinueRegAtTime; // Продолжать регистрацию в ХХ минут
            public int UnregisterAllAtTime; // Отменять регистрацию в ХХ минут
            public bool IfUnregisterAtTime; // Отменять или нет регистрацию
            public bool IfCloseDialogs; // Закрывать диалоги торнамент регистер?
            public bool IfLoadTournamentsInfo; // Загружать со старта информацию о турнирах?
            public bool IfLoadFiltersStartUp; // Загружать ли со старта фильтры?
            public bool IfSetFilterViaClipboard; // Ставить фильтр через буфер обмена?
            public bool FixCtrlAProblem; // Очищать фильтр 20 бекспейсов, вместо ктрл+А
            public int RegistrationLatency; // Задержка перед регистрацией в турнире
            public string PokerstarsSettingsFolder; // Папка с настройками покерстарз
            public bool IfCloseToTray; // Сворачивать в трей или закрывать?
            public bool IfCheckSNGTab; // Проверять ли на вкладку СНГ
        }

        public struct SessionSettings
        {
            public bool IfTimeToSession;
            public bool IfMinutesToSession;
            public bool IfKeepMaxTables;
            public bool IfTotalMaxTables;

            public int KeepMax;
            public int TotalMax;
            public int Minutes;
        }

        public struct AppSettings
        {

        }

        private static XmlDocument doc = new XmlDocument();
        public static UserSettings User;
        public static AppSettings App;
        public static SessionSettings Sess;

        private static void SetDefaultSettings()
        {
            User.ContinueRegAtTime = 0;
            User.UnregisterAllAtTime = 54;
            User.IfUnregisterAtTime = true;
            User.IfCloseDialogs = true;
            User.IfLoadTournamentsInfo = true;
            User.IfLoadFiltersStartUp = true;
            User.IfSetFilterViaClipboard = true;
            User.FixCtrlAProblem = false;
            User.RegistrationLatency = 2000;
            User.PokerstarsSettingsFolder = "C:/Users/Admin/AppData/Local/PokerStars/";
            User.IfCloseToTray = false;
            User.IfCheckSNGTab = false;

            Sess.IfKeepMaxTables = true;
            Sess.IfMinutesToSession = true;
            Sess.IfTimeToSession = true;
            Sess.IfTotalMaxTables = true;

            Sess.KeepMax = 9;
            Sess.TotalMax = 99;
            Sess.Minutes = 99;
        }

        private static XmlNode SerializeSetting(string name, string value)
        {
            XmlNode set = doc.CreateElement("Setting");
            XmlAttribute attr = set.Attributes.Append(doc.CreateAttribute("name"));
            attr.Value = name;
            attr = set.Attributes.Append(doc.CreateAttribute("value"));
            attr.Value = value;
            return set;
        }

        public static void Save()
        {
            doc = new XmlDocument();
            XmlNode head = doc.AppendChild(doc.CreateElement("UserSettings"));

            head.AppendChild(SerializeSetting("ContinueRegAtTime", User.ContinueRegAtTime.ToString()));
            head.AppendChild(SerializeSetting("UnregisterAllAtTime", User.UnregisterAllAtTime.ToString()));
            head.AppendChild(SerializeSetting("IfUnregisterAtTime", User.IfUnregisterAtTime.ToString()));
            head.AppendChild(SerializeSetting("IfCloseDialogs", User.IfCloseDialogs.ToString()));
            head.AppendChild(SerializeSetting("IfLoadTournamentsInfo", User.IfLoadTournamentsInfo.ToString()));
            head.AppendChild(SerializeSetting("IfLoadFiltersStartUp", User.IfLoadFiltersStartUp.ToString()));
            head.AppendChild(SerializeSetting("IfSetFilterViaClipboard", User.IfSetFilterViaClipboard.ToString()));
            head.AppendChild(SerializeSetting("FixCtrlAProblem", User.FixCtrlAProblem.ToString()));
            head.AppendChild(SerializeSetting("RegistrationLatency", User.RegistrationLatency.ToString()));
            head.AppendChild(SerializeSetting("PokerstarsSettingsFolder", User.PokerstarsSettingsFolder.ToString()));
            head.AppendChild(SerializeSetting("IfCloseToTray", User.IfCloseToTray.ToString()));
            head.AppendChild(SerializeSetting("IfCheckSNGTab", User.IfCheckSNGTab.ToString()));

            doc.Save(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Autoreg\user.xml");

            doc = new XmlDocument();
            head = doc.AppendChild(doc.CreateElement("SessionSettings"));
            head.AppendChild(SerializeSetting("IfKeepMaxTables", Sess.IfKeepMaxTables.ToString()));
            head.AppendChild(SerializeSetting("IfMinutesToSession", Sess.IfMinutesToSession.ToString()));
            head.AppendChild(SerializeSetting("IfTimeToSession", Sess.IfTimeToSession.ToString()));
            head.AppendChild(SerializeSetting("IfTotalMaxTables", Sess.IfTotalMaxTables.ToString()));
            head.AppendChild(SerializeSetting("KeepMax", Session.KeepMax.ToString()));
            head.AppendChild(SerializeSetting("TotalMax", Session.TotalMax.ToString()));
            head.AppendChild(SerializeSetting("Minutes", Sess.Minutes.ToString()));

            doc.Save(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Autoreg\session.xml");
        }

        public static bool Load()
        {
            SetDefaultSettings();
            doc = new XmlDocument();
            try
            {
                doc.Load(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Autoreg\user.xml");
                foreach (XmlNode setting in doc.ChildNodes[0])
                {
                    switch (setting.Attributes[0].Value)
                    {
                        case "ContinueRegAtTime": User.ContinueRegAtTime = int.Parse(setting.Attributes[1].Value); break;
                        case "UnregisterAllAtTime": User.UnregisterAllAtTime = int.Parse(setting.Attributes[1].Value); break;
                        case "IfUnregisterAtTime": User.IfUnregisterAtTime = bool.Parse(setting.Attributes[1].Value); break;
                        case "IfCloseDialogs": User.IfCloseDialogs = bool.Parse(setting.Attributes[1].Value); break;
                        case "IfLoadTournamentsInfo": User.IfLoadTournamentsInfo = bool.Parse(setting.Attributes[1].Value); break;
                        case "IfLoadFiltersStartUp": User.IfLoadFiltersStartUp = bool.Parse(setting.Attributes[1].Value); break;
                        case "IfSetFilterViaClipboard": User.IfSetFilterViaClipboard = bool.Parse(setting.Attributes[1].Value); break;
                        case "FixCtrlAProblem": User.FixCtrlAProblem = bool.Parse(setting.Attributes[1].Value); break;
                        case "IfCloseToTray": User.IfCloseToTray = bool.Parse(setting.Attributes[1].Value); break;
                        case "IfCheckSNGTab": User.IfCheckSNGTab = bool.Parse(setting.Attributes[1].Value); break;
                        case "RegistrationLatency": User.RegistrationLatency = int.Parse(setting.Attributes[1].Value); break;
                        case "PokerstarsSettingsFolder": User.PokerstarsSettingsFolder = setting.Attributes[1].Value; break; 
                    }
                }
            }
            catch (System.IO.FileNotFoundException e)
            {
                DialogResult dr = MessageBox.Show("User config is not found! Would You like to create it default?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr != DialogResult.Yes)
                {
                    Logger.Write("Cannot load user config: " + e.Message + ". Exiting...", Logger.L.ERROR, true, true);
                    Environment.Exit(0);
                }
            }
            catch (System.Xml.XmlException e)
            {
                Logger.Write("Cannot load user config: " + e.Message, Logger.L.ERROR, true, true);
            }
            catch
            {
                Logger.Write("Cannot load user config, unknown error!", Logger.L.ERROR, true, true);
            }

            doc = new XmlDocument();
            try
            {
                doc.Load(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Autoreg\session.xml");
                foreach (XmlNode setting in doc.ChildNodes[0])
                {
                    switch (setting.Attributes[0].Value)
                    {
                        case "IfKeepMaxTables": Sess.IfKeepMaxTables = bool.Parse(setting.Attributes[1].Value); break;
                        case "IfTotalMaxTables": Sess.IfTotalMaxTables = bool.Parse(setting.Attributes[1].Value); break;
                        case "IfMinutesToSession": Sess.IfMinutesToSession = bool.Parse(setting.Attributes[1].Value); break;
                        case "IfTimeToSession": Sess.IfTimeToSession = bool.Parse(setting.Attributes[1].Value); break;
                        case "KeepMax": Sess.KeepMax = int.Parse(setting.Attributes[1].Value); break;
                        case "TotalMax": Sess.TotalMax = int.Parse(setting.Attributes[1].Value); break;
                        case "Minutes": Sess.Minutes = int.Parse(setting.Attributes[1].Value); break;
                    }
                }
            }
            catch (System.IO.FileNotFoundException e)
            {
                Logger.Write("Cannot load session config: " + e.Message + ". Creating defaults...", Logger.L.ERROR, true, true);
            }
            catch (System.Xml.XmlException e)
            {
                Logger.Write("Cannot load session config: " + e.Message, Logger.L.ERROR, true, true);
            }
            catch
            {
                Logger.Write("Cannot load session config, unknown error!", Logger.L.ERROR, true, true);
            }
            
            return true;
        }
    }
}
