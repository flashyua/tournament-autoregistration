﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoReg
{
    public partial class OptionsBox : Form
    {
        public OptionsBox()
        {
            InitializeComponent();
            cb_closeNotifications.Checked = Settings.User.IfCloseDialogs;
            cb_loadFiltersStartup.Checked = Settings.User.IfLoadFiltersStartUp;
            cb_loadTournamentInfo.Checked = Settings.User.IfLoadTournamentsInfo;
            cb_filterViaClipboard.Checked = Settings.User.IfSetFilterViaClipboard;
            cb_fixCtrlA.Checked = Settings.User.FixCtrlAProblem;
            cb_minimizeToTray.Checked = Settings.User.IfCloseToTray;
            cb_checkSNGTab.Checked = Settings.User.IfCheckSNGTab;
            tb_regLatency.Text = Settings.User.RegistrationLatency.ToString();
            cb_unregisterAll.Checked = Settings.User.IfUnregisterAtTime;
            tb_unregAt.Text = Settings.User.UnregisterAllAtTime.ToString();
            tb_continueReg.Text = Settings.User.ContinueRegAtTime.ToString();
            tb_psSettingFolder.Text = Settings.User.PokerstarsSettingsFolder;
        }

        private void SaveOptions()
        {
            Settings.User.IfCloseDialogs = cb_closeNotifications.Checked;
            Settings.User.IfLoadFiltersStartUp = cb_loadFiltersStartup.Checked;
            Settings.User.IfLoadTournamentsInfo = cb_loadTournamentInfo.Checked;
            Settings.User.IfSetFilterViaClipboard = cb_filterViaClipboard.Checked;
            Settings.User.IfUnregisterAtTime = cb_unregisterAll.Checked;
            Settings.User.IfCloseToTray = cb_minimizeToTray.Checked;
            Settings.User.IfCheckSNGTab = cb_checkSNGTab.Checked;
            Settings.User.RegistrationLatency = int.Parse(tb_regLatency.Text);
            Settings.User.UnregisterAllAtTime = int.Parse(tb_unregAt.Text);
            Settings.User.ContinueRegAtTime = int.Parse(tb_continueReg.Text);
            Settings.User.PokerstarsSettingsFolder = tb_psSettingFolder.Text;
            Settings.Save();
        }

        private void cb_closeNotifications_CheckedChanged(object sender, EventArgs e)
        {
            Settings.User.IfCloseDialogs = cb_closeNotifications.Checked;
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            SaveOptions();
            this.Hide();
        }

        private void cb_loadFiltersStartup_CheckedChanged(object sender, EventArgs e)
        {
            Settings.User.IfLoadFiltersStartUp = cb_loadFiltersStartup.Checked;
        }

        private void cb_loadTournamentInfo_CheckedChanged(object sender, EventArgs e)
        {
            Settings.User.IfLoadTournamentsInfo = cb_loadTournamentInfo.Checked;
        }

        private void cb_unregisterAll_CheckedChanged(object sender, EventArgs e)
        {
            tb_unregAt.Enabled = cb_unregisterAll.Checked;
            tb_continueReg.Enabled = cb_unregisterAll.Checked;
            Settings.User.IfUnregisterAtTime = cb_unregisterAll.Checked;
        }

        private void tb_unregAt_TextChanged(object sender, EventArgs e)
        {
            Settings.User.UnregisterAllAtTime = int.Parse(tb_unregAt.Text);
        }

        private void cb_filterViaClipboard_CheckedChanged(object sender, EventArgs e)
        {
            Settings.User.IfSetFilterViaClipboard = cb_filterViaClipboard.Checked;
        }

        private void bt_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cb_minimizeToTray_CheckedChanged(object sender, EventArgs e)
        {
            Settings.User.IfCloseToTray = cb_minimizeToTray.Checked;
        }

        private void cb_fixCtrlA_CheckedChanged(object sender, EventArgs e)
        {
            Settings.User.FixCtrlAProblem = cb_fixCtrlA.Checked;
        }

        private void cb_checkSNGTab_CheckedChanged(object sender, EventArgs e)
        {
            Settings.User.IfCheckSNGTab = cb_checkSNGTab.Checked;
        }
    }
}
