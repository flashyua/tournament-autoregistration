﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Tournament autoregistration")]
[assembly: AssemblyDescription("Tournament autoregistration tool for Pokerstars")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Pavel Komarov")]
[assembly: AssemblyProduct("AutoReg")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("be74544b-994a-4d16-8559-58610de8f1be")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("0.25.*")]
[assembly: AssemblyFileVersion("0.25.0.0")]
[assembly: NeutralResourcesLanguageAttribute("en")]
