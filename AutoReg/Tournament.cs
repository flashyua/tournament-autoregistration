﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Xml;

namespace AutoReg
{
    enum TournamentState
    {
        UNKNOWN = 0,
        REGISTERED = 1,
        OPENED = 2,
        CLOSED = 3,
        PENDING = 4,
        ERROR = 10,
        ALREADYREGISTERED = 11,
    }

    class Tournament
    {
        private static long mTournamentCounter = 0;
        private int id = 0;
        private long mInnerId = 0;
        private DateTime registrationTime = new DateTime();
        private TournamentState state = TournamentState.UNKNOWN;
        public  delegate void OnStateChangedDelegate(Tournament tournament);
        private OnStateChangedDelegate mOnStateChanged;

        public int ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public DateTime RegistrationTime
        {
            get 
            {
                return registrationTime;
            }
            protected set
            {
                registrationTime = value;
            }
        }

        public TournamentState State
        {
            get
            {
                return state;
            }
            protected set
            {
                Logger.Write("Tournament " + ID + " state changed: " + value.ToString(), Logger.L.STATUS);
                state = value;
            }
        }

        public OnStateChangedDelegate OnStateChanged
        {
            set
            {
                mOnStateChanged = value;
            }
        }

        public Tournament()
        {
            mInnerId = mTournamentCounter++;
        }

        public void SetPendingState()
        {
            State = TournamentState.PENDING;
        }

        public bool Register()
        {
            //PokerstarsParser.OnTournamentStatusChanged += OnTournamentStateChanged;
            //TaskQueue.OnTournamentStatusChanged += OnTournamentStateChanged;
            if (Settings.User.RegistrationLatency != 0)
            {
                Thread.Sleep(Settings.User.RegistrationLatency);
            }
            IntPtr regBtn = GetRegButton();
            string capt = WinWrapper.GetText(regBtn);
            if (capt == "Register")
            {
                WinWrapper.SetActiveWindow(regBtn);
                WinWrapper.SendMessage(regBtn, (uint)0x100, (int)0x20, 0); // Отправляем ебаный ПРОБЕЛ ебаной кнопке
                WinWrapper.SendMessage(regBtn, (uint)0x101, (int)0x20, 0);
                Logger.Write("Waiting log answer... (iID : " + mInnerId + ")", Logger.L.DEBUG);
            }
            else if (capt == "Unregister")
            {
                Logger.Write("Tournament already registered! (iID : " + mInnerId + ")", Logger.L.DEBUG);
                TaskManager.FreeToRegister();
                State = TournamentState.ALREADYREGISTERED;
                mOnStateChanged(this);
            }
            else
            {
                Logger.Write("Tournament registration error! (iID : " + mInnerId + ")", Logger.L.WARN, false, true);
                State = TournamentState.ERROR;
                TaskManager.FreeToRegister();
                //PokerstarsParser.OnTournamentStatusChanged -= OnTournamentStateChanged;
                //TaskQueue.OnTournamentStatusChanged -= OnTournamentStateChanged;
                mOnStateChanged(this);
            }
            return true;
        }

        private IntPtr GetRegButton()
        {
            IntPtr regBtn = IntPtr.Zero;
            do
            {
                regBtn = WinWrapper.FindWindowEx(PokerStarsLobby.MainWindow, regBtn, null, null);
                string capt = WinWrapper.GetText(regBtn);
                if (((capt == "Register") || (capt == "Unregister")) && WinWrapper.IsWindowVisible(regBtn))
                {
                    Logger.Write("\"" + capt + "\" is allowed (iID : " + mInnerId + ")", Logger.L.DEBUG);
                    return regBtn;
                }
            } while (regBtn != IntPtr.Zero);
            return IntPtr.Zero;
        }

        public void OnTournamentStateChanged(TournamentState _state)
        {
            Logger.Write("Tournament " + ID + "(iID : " + mInnerId + ")" + " OnTournamentStateChanged()", Logger.L.DEBUG);
            if (State != _state) // If any change for this ID
            {
                RegistrationTime = DateTime.Now;
                State = _state;
                if (mOnStateChanged != null) mOnStateChanged(this);
                else Logger.Write("mOnStateChanged is null! (iID : " + mInnerId + ")", Logger.L.DEBUG);
            }
        }

        public void SetUnregistered()
        {
            if (State == TournamentState.REGISTERED)
            {
                State = TournamentState.UNKNOWN;
            }
        }

        public void Remove()
        {
            //PokerstarsParser.OnTournamentStatusChanged -= OnTournamentStateChanged;
        }

        public void FromXML(XmlNode _node)
        {
            foreach (XmlAttribute attr in _node.Attributes)
            {
                switch (attr.Name)
                {
                    case "ID": ID = int.Parse(attr.Value);  break;
                    case "State":
                        switch (attr.Value)
                        {
                            case "UNKNOWN": State = TournamentState.UNKNOWN; break;
                            case "REGISTERED": State = TournamentState.REGISTERED; break;
                            case "OPENED": State = TournamentState.OPENED; break;
                            case "CLOSED": State = TournamentState.CLOSED; break;
                            case "PENDING": State = TournamentState.PENDING; break;
                            case "ERROR": State = TournamentState.ERROR; break;
                            case "ALREADYREGISTERED": State = TournamentState.ALREADYREGISTERED; break;
                        }
                        break;
                }
            }
            
        }

        public XmlNode ToXML(XmlDocument _doc)
        {
            XmlNode node = _doc.CreateElement("Tournament");
            //doc.AppendChild(node);

            XmlAttribute attr = _doc.CreateAttribute("ID");
            attr.Value = ID.ToString();
            node.Attributes.Append(attr);

            attr = _doc.CreateAttribute("State");
            attr.Value = State.ToString();
            node.Attributes.Append(attr);
            
            return node;
        }
        
    }
}
