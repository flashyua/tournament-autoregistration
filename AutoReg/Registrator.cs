﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Linq;
using System.Diagnostics;
using System.Xml;

namespace AutoReg
{
    enum RegistratorRunningState
    {
        RUNNING,
        STOPPED,
        PAUSED,
        WAITING,
        READY
    }

    enum RegistratorLimitState
    {
        NOLIMIT,
        KEEPMAX,
        TOTALMAX,
        ALLLIMIT,
        SESSIONLIMIT
    }

    

    class Registrator
    {
        #region vars
        protected int mIndex = 0;
        protected bool mIsActive = false;
        protected string mName = "";
        protected string mFilter = "";
        protected int mMaxPerPass = 0;
        protected int mRegisterInThisCycle = 0; // what tournament is registering at this pass
        protected int mKeepMax = 0;
        protected int mKeepNow = 0;
        protected int mTotalMax = 0;
        protected int mTotalNow = 0;
        protected int mSessionLimit = 0;
        protected RegistratorLimitState mLimitState;
        protected RegistratorRunningState mRunningState;
        protected List<Tournament> mTournaments = new List<Tournament>();
        protected ListViewItem mListViewItem = new ListViewItem();

        public delegate void OnEndRegistrationDelegate(int _index);
        private OnEndRegistrationDelegate mOnEndRegistration;

        protected int mAttempts = 0;
        #endregion

        #region Properties

        public int Index
        {
            get
            {
                return mIndex;
            }
            set
            {
                Logger.Write("Setting index " + value, Logger.L.DEBUG);
                mIndex = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return mIsActive;
            }
            set
            {
                Logger.Write("Registrator " + Name + " active: " + value, Logger.L.DEBUG);
                mIsActive = mListViewItem.Checked = value;
            }
        }

        public string Name
        {
            get
            {
                return mName;
            }
            set
            {
                mName = mListViewItem.SubItems[0].Text = value;
            }
        }

        public string Filter
        {
            get
            {
                return mFilter;
            }
            set
            {
                mListViewItem.SubItems[7].Text = mFilter = value;
            }
        }

        public int MaxPerPass
        {
            get
            {
                return mMaxPerPass;
            }
            set
            {
                mMaxPerPass = value;
                mListViewItem.SubItems[3].Text = mMaxPerPass.ToString();
            }
        }

        public int KeepMax
        {
            get
            {
                return mKeepMax;
            }
            set
            {
                mKeepMax = value;
                mListViewItem.SubItems[2].Text = value.ToString();
                UpdateStatistics();
            }
        }

        public int TotalMax
        {
            get
            {
                return mTotalMax;
            }
            set
            {
                mTotalMax = value;
                mListViewItem.SubItems[5].Text = value.ToString();
                UpdateStatistics();
            }
        }

        public int SessionLimit
        {
            get
            {
                return mSessionLimit;
            }
            set
            {
                mSessionLimit = value;
                UpdateLimitState();
                mListViewItem.SubItems[6].Text = value.ToString();
            }
        }

        public OnEndRegistrationDelegate OnEndRegistration
        {
            set
            {
                mOnEndRegistration = value;
            }
        }

        public int KeepNow
        {
            get
            {
                return mKeepNow;
            }
            protected set
            {
                Program.MainWindow.BeginInvoke(new MethodInvoker(delegate()
                {
                    mListViewItem.SubItems[1].Text = value.ToString();
                    mListViewItem.SubItems[1].Text = value.ToString();
                }));
                mKeepNow = value;

                UpdateLimitState();
                Logger.Write("Registrator " + Name + " currently registered " + mKeepNow + " from " + mKeepMax + " tournaments", Logger.L.DEBUG);
            }
        }

        public int TotalNow
        {
            get
            {
                return mTotalNow;
            }
            protected set
            {
                Program.MainWindow.BeginInvoke(new MethodInvoker(delegate()
                {
                    mListViewItem.SubItems[4].Text = value.ToString();
                    mListViewItem.SubItems[4].Text = value.ToString();
                }));
                mTotalNow = value;
                UpdateLimitState();
                //if (mTotalNow >= mTotalMax)
                //{
                //    if (LimitState == RegistratorLimitState.KEEPMAX)
                //    {
                //        LimitState = RegistratorLimitState.ALLLIMIT;
                //    }
                //    else LimitState = RegistratorLimitState.TOTALMAX;
                //}
                //else if (LimitState == RegistratorLimitState.ALLLIMIT)
                //{
                //    LimitState = RegistratorLimitState.KEEPMAX;
                //}
                //else if (LimitState == RegistratorLimitState.TOTALMAX)
                //{
                //    LimitState = RegistratorLimitState.NOLIMIT;
                //}
                Logger.Write("Registrator " + Name + " total registered " + mTotalNow + " from " + mTotalMax + " tournaments", Logger.L.DEBUG);
            }
        }

        public RegistratorLimitState LimitState
        {
            get
            {
                return mLimitState;
            }
            protected set
            {
                mLimitState = value;
                UpdateColor();
                Logger.Write("Registrator " + Name + " limit state changed to: " + mLimitState.ToString(), Logger.L.DEBUG);
            }
        }

        public RegistratorRunningState RunningState
        {
            get
            {
                return mRunningState;
            }
            protected set
            {
                mRunningState = value;
                UpdateColor();
                Logger.Write("Registrator " + Name + " running state changed to: " + mRunningState.ToString(), Logger.L.DEBUG);
            }
        }

        public ListViewItem AssocLvItem
        {
            get
            {
                return mListViewItem;
            }
            set
            {
                mListViewItem = value;
            }
        }

        #endregion

        public Registrator()
        {
            mListViewItem.SubItems.AddRange(new string[] { "", "", "", "", "", "", "" });
            OnEndRegistration = Session.RunRegistrator;
            RunningState = RegistratorRunningState.PAUSED;
            Name = "New filter";
            Filter = "Empty";
            KeepMax = 0;
            KeepNow = 0;
            TotalNow = 0;
            TotalMax = 0;
            SessionLimit = 0;
            MaxPerPass = 0;
            IsActive = false;
        }

        public Registrator(string _name, string _filter, int _mpc, int _keepMax, int _totalMax)
        {
            Name = _name;
            Filter = _filter;
            MaxPerPass = _mpc;
            KeepMax = _keepMax;
            TotalMax = _totalMax;
        }

        public void SetReady()
        {
            RunningState = RegistratorRunningState.READY;
        }

        public bool Start()
        {
            
            for (var i = 0; i < MaxPerPass; i++) 
            {
                //RunningState = RegistratorRunningState.RUNNING;
                TaskManager.AddTask(new TaskRegisterOneTournament(this, i), TaskPriority.NORMAL);
            }
            return true;
        }

        public bool Pause()
        {
            RunningState = RegistratorRunningState.PAUSED;
            return true;
        }

        public bool Stop()
        {
            RunningState = RegistratorRunningState.STOPPED;
            return true;
        }

        private bool CheckState()
        {
            if (Session.RunningState != SessionRunningState.RUNNING)
            {
                Logger.Write("Registration is aborted because session is not running!", Logger.L.DEBUG);
                return false;
            }
            if (LimitState == RegistratorLimitState.KEEPMAX)
            {
                Logger.Write("Registration is aborted because filter " + Name + " is keep now " + KeepNow + " from " + KeepMax + " tournaments!", Logger.L.STATUS);
                return false;
            }
            if (LimitState == RegistratorLimitState.TOTALMAX)
            {
                Logger.Write("Registration is aborted because filter " + Name + " is total registered " + TotalNow + " from " + TotalMax + " tournaments!", Logger.L.STATUS);
                return false;
            }
            if (LimitState == RegistratorLimitState.SESSIONLIMIT)
            {
                Logger.Write("Registration is aborted because filter " + Name + " is keep now " + KeepNow + " and Session Limit is " + SessionLimit + "!", Logger.L.STATUS);
                return false;
            }
            if (LimitState != RegistratorLimitState.NOLIMIT)
            {
                Logger.Write("Registration is aborted because filter " + Name + " is reached unknown limit!", Logger.L.STATUS);
                return false;
            }
            if ((Session.LimitState == SessionLimitState.KEEPMAX)&&Program.MainWindow.cb_keepMax.Checked)
            {
                Logger.Write("Registration is aborted because session keeping now maximum of tournaments!", Logger.L.STATUS);
                return false;
            }
            if (((Session.LimitState == SessionLimitState.TOTALMAX) || 
                (Session.LimitState == SessionLimitState.ALLLIMIT)) && Program.MainWindow.cb_totalReg.Checked)
            {
                Logger.Write("Registration is aborted because session registered now maximum of tournaments!", Logger.L.STATUS);
                Session.Stop();
                return false;
            }
            //if ((Session.LimitState != SessionLimitState.NOLIMIT) && (Program.MainWindow.cb_keepMax.Checked || Program.MainWindow.cb_totalReg.Checked))
            //{
            //    Logger.Write("Registration is aborted because session limit is reached!", Logger.L.STATUS);
            //    return false;
            //}
            if (!IsActive)
            {
                Logger.Write("Registration is aborted because filter " + Name + " is not active", Logger.L.DEBUG);
                return false;
            }
            RunningState = RegistratorRunningState.RUNNING;
            //if (RunningState != RegistratorRunningState.READY)
            //{
            //    Logger.Write("Registration is aborted because filter " + Name + " is not ready!", Logger.L.STATUS);
            //    return false;
            //}
            return true;
        }

        public bool RegisterOneTournament(int offset) // Register one tournament with offset in listbox
        {
            //if ((LimitState == RegistratorLimitState.NOLIMIT) && (RunningState == RegistratorRunningState.RUNNING) && IsActive)
            if (CheckState())
            {
                Tournament tournament = new Tournament();
                if (!TaskManager.PushToRegister(tournament))
                {
                    Logger.Write("Registrator " + Name + ": TaskManager registration slot is not free! Adding delayed task", Logger.L.DEBUG);
                    TaskManager.AddDelayedTask(new TaskRegisterOneTournament(this, offset), 5);
                    return false;
                }
                Logger.Write("Registering tournament offset " + offset, Logger.L.DEBUG);
                if (!ApplyFilter(3)) // Testing filter 3 pass
                //if(false)
                {
                    TaskManager.FreeToRegister();
                    TaskManager.AddDelayedTask(new TaskRegisterOneTournament(this, offset), 20);
                    return false;
                }
                else
                {
                    // Попытка отложенной регистрации после ошибки фильтра
                }
                tournament.OnStateChanged = OnTournamentStateChanged;
                WinWrapper.ScrollListBox(PokerStarsLobby.TournamentListBox, offset, 10);
                tournament.Register();
                return true;
            }
            else
            {
                return false;
            }
        }

        public void OnTournamentStateChanged(Tournament tournament)
        {
            // REGISTERED
            if (tournament.State == TournamentState.REGISTERED)
            {
                Logger.Write("Tournament " + tournament.ID + " is registered!", Logger.L.DEBUG);
                RemoveDuplicate(tournament.ID);
                mTournaments.Add(tournament);
                UpdateStatistics();
                RunningState = RegistratorRunningState.WAITING;
                if (mRegisterInThisCycle < MaxPerPass)
                {
                    //RegisterOneTournament(mRegisterInThisCycle);
                }
                else
                {
                    mRegisterInThisCycle = 0;
                    //mOnEndRegistration(Index + 1);
                    //TaskManager.AddTask(new TaskSessionRunRegistrator(Index + 1), TaskPriority.NORMAL);
                }
            }
                // ALREADYREGISTERED
            else if (tournament.State == TournamentState.ALREADYREGISTERED)
            {
                
                RunningState = RegistratorRunningState.WAITING;
                //mOnEndRegistration(Index + 1);
                //TaskManager.AddTask(new TaskSessionRunRegistrator(Index + 1), TaskPriority.NORMAL);
                if (mAttempts != 2)
                {
                    TaskManager.AddDelayedTask(new TaskSessionRunRegistrator(Index), 10);
                    mAttempts++;
                }
                else
                {
                    Logger.Write("Registator is reached attempts limit on ALREADYREGISTERED state!", Logger.L.STATUS);
                    mAttempts = 0;
                }
            }
                // OPENED
            else if (tournament.State == TournamentState.OPENED)
            {
                if (RunningState != RegistratorRunningState.STOPPED)
                {
                    RunningState = RegistratorRunningState.READY;
                }
                UpdateStatistics();
                if (Session.RunningState == SessionRunningState.RUNNING) TaskManager.AddTask(new TaskRegisterOneTournament(this, 0), TaskPriority.NORMAL);
            }
                // CLOSED
            else if (tournament.State == TournamentState.CLOSED)
            {
                UpdateStatistics();
                if (RunningState != RegistratorRunningState.STOPPED) RunningState = RegistratorRunningState.READY;
                //TaskManager.AddTask(new TaskRegisterOneTournament(this, 0), TaskPriority.NORMAL);
            }
                // ERROR
            else
            {
                Logger.Write("Tournament registration error!", Logger.L.STATUS, false, true);
                if (RunningState != RegistratorRunningState.STOPPED) RunningState = RegistratorRunningState.READY;
                tournament = null;
                UpdateStatistics();
                TaskManager.AddDelayedTask(new TaskSessionRunRegistrator(Index), 10);
            }
        }

        private void RemoveDuplicate(int ID) // Remove duplicates ID if exists
        {
            for (var i = 0; i < mTournaments.Count; i++)
            {
                Tournament tournament = mTournaments[i];
                if (tournament.ID == ID)
                {
                    
                    mTournaments[i].Remove();
                    mTournaments.RemoveAt(i);
                    Logger.Write("Removing tournament duplicate with ID " + ID, Logger.L.DEBUG);
                }
            }
        }

        private bool TestFilter()
        {
            string c = WinWrapper.GetText(PokerStarsLobby.TournamentFilter);
            if (c == Filter)
            {
                Logger.Write("Testing filter: " + c + " = " + Filter + "... OK!", Logger.L.DEBUG);
                return true;
            }
            else
            {
                Logger.Write("Testing filter: " + c + " != " + Filter + "... Fail!", Logger.L.DEBUG);
                return false;
            }
            
        }

        public bool ApplyFilter(int maxAttempts)
        {
            if (!PokerStarsLobby.FindLobby()) return false;
            if (TestFilter()) return true;
            for (int att = 0; att < maxAttempts; att++)
            {
                WinWrapper.SetForegroundWindow(PokerStarsLobby.MainWindow);             //
                WinWrapper.SendMessage(PokerStarsLobby.TournamentFilter, 0x201, 1, 0);  // Focusing filter
                WinWrapper.SendMessage(PokerStarsLobby.TournamentFilter, 0x202, 0, 0);  // 

                if (Settings.User.FixCtrlAProblem) // Clearing filter
                {
                    for (int ic = 0; ic < 20; ic++)
                    {
                        WinWrapper.keybd_event(0x2E, 0, 0, 0); // via 20 dels
                        WinWrapper.keybd_event(0x2E, 0, 2, 0);
                    }
                }
                else
                {
                    WinWrapper.keybd_event(0x11, 0, 0, 0); // via ctrl+A
                    WinWrapper.keybd_event(0x41, 0, 0, 0);
                    WinWrapper.keybd_event(0x41, 0, 2, 0);
                    WinWrapper.keybd_event(0x11, 0, 2, 0);
                    WinWrapper.keybd_event(0x08, 0, 0, 0);
                    WinWrapper.keybd_event(0x08, 0, 2, 0);
                }

                if (Settings.User.IfSetFilterViaClipboard)
                {
                    Clipboard.SetText(Filter);
                    WinWrapper.keybd_event(0x11, 0, 0, 0); // Setting filter via clipboard
                    WinWrapper.keybd_event(0x56, 0, 0, 0);
                    WinWrapper.keybd_event(0x56, 0, 2, 0);
                    WinWrapper.keybd_event(0x11, 0, 2, 0);
                }
                else
                {
                    WinWrapper.SendKeys(Filter); // Setting filter via send keys
                }
                Thread.Sleep(200);
                if (TestFilter()) return true;
            }
            if (!TestFilter()) Logger.Write("Filter " + Name + " Error!", Logger.L.WARN, false, true);
            return false;
        }

        private int GetOpenedNow()
        {
            int openNow = 0;
            foreach (var tournament in mTournaments)
            {
                if ((tournament.State == TournamentState.REGISTERED) || (tournament.State == TournamentState.OPENED))
                {
                    openNow++;
                }
            }
            
            return openNow;
        }

        private int GetTotalNow()
        {
            int totalNow = 0;
            foreach (var tournament in mTournaments)
            {
                if ((tournament.State == TournamentState.REGISTERED) || (tournament.State == TournamentState.OPENED) ||
                    (tournament.State == TournamentState.CLOSED))
                {
                    totalNow++;
                }
            }
            
            return totalNow;
        }

        private void UpdateStatistics()
        {
            KeepNow = GetOpenedNow();
            TotalNow = GetTotalNow();
            Session.UpdateSession();
        }

        public void UpdateLimitState()
        {
            // Снимаем лимиты
            if (LimitState == RegistratorLimitState.TOTALMAX)
            {
                if (TotalNow < TotalMax)
                {
                    LimitState = RegistratorLimitState.NOLIMIT;
                    if (Session.RunningState == SessionRunningState.RUNNING) TaskManager.AddTask(new TaskSessionRunRegistrator(Index), TaskPriority.NORMAL);
                    UpdateColor();
                }
            }
            else if (LimitState == RegistratorLimitState.KEEPMAX)
            {
                if (KeepNow < KeepMax)
                {
                    LimitState = RegistratorLimitState.NOLIMIT;
                    if (Session.RunningState == SessionRunningState.RUNNING) TaskManager.AddTask(new TaskSessionRunRegistrator(Index), TaskPriority.NORMAL);
                    UpdateColor();
                }
            }
            else if (LimitState == RegistratorLimitState.SESSIONLIMIT)
            {
                if (Session.KeepNow < SessionLimit)
                {
                    LimitState = RegistratorLimitState.NOLIMIT;
                    if (Session.RunningState == SessionRunningState.RUNNING) TaskManager.AddTask(new TaskSessionRunRegistrator(Index), TaskPriority.NORMAL);
                    UpdateColor();
                }
            }
            // Ставим лимиты
            if (TotalNow >= TotalMax)
            {
                if (LimitState != RegistratorLimitState.TOTALMAX)
                {
                    LimitState = RegistratorLimitState.TOTALMAX;
                    UpdateColor();
                }
                
            }
            else if (KeepNow >= KeepMax)
            {
                if (LimitState != RegistratorLimitState.KEEPMAX)
                {
                    LimitState = RegistratorLimitState.KEEPMAX;
                    UpdateColor();
                }
                
            }
            else if (Session.KeepNow >= this.SessionLimit)
            {
                if (LimitState != RegistratorLimitState.SESSIONLIMIT)
                {
                    LimitState = RegistratorLimitState.SESSIONLIMIT;
                    UpdateColor();
                }
                
            }
            else
            {
                if (LimitState != RegistratorLimitState.NOLIMIT)
                {
                    LimitState = RegistratorLimitState.NOLIMIT;
                    UpdateColor();
                }
            }
        }

        private void UpdateColor()
        {
            Program.MainWindow.BeginInvoke(new MethodInvoker(delegate()
            {
                if ((RunningState == RegistratorRunningState.STOPPED) && (KeepNow == 0))
                {
                    mListViewItem.BackColor = Color.White; // WHITE
                }
                else if ((RunningState != RegistratorRunningState.STOPPED)
                    && (RunningState != RegistratorRunningState.PAUSED)
                    && (LimitState == RegistratorLimitState.NOLIMIT))
                {
                    mListViewItem.BackColor = Color.FromArgb(222, 255, 222);  // GREEN
                }
                else if (((LimitState == RegistratorLimitState.KEEPMAX)||(LimitState == RegistratorLimitState.SESSIONLIMIT))
                    && (RunningState != RegistratorRunningState.STOPPED)
                    && (RunningState != RegistratorRunningState.PAUSED))
                {
                    mListViewItem.BackColor = Color.FromArgb(255, 255, 222); // YELLOW
                }
                else if (((LimitState == RegistratorLimitState.TOTALMAX) || (LimitState == RegistratorLimitState.ALLLIMIT))
                    && (RunningState != RegistratorRunningState.STOPPED)
                    && (RunningState != RegistratorRunningState.PAUSED))
                {
                    mListViewItem.BackColor = Color.FromArgb(255, 222, 222); // RED
                }
                else if(((RunningState == RegistratorRunningState.STOPPED)
                    || (RunningState == RegistratorRunningState.PAUSED))
                    && (KeepNow > 0))
                {
                    mListViewItem.BackColor = Color.FromArgb(222, 240, 255); // BLUE
                }
            }));
        }

        public XmlNode ToXML(XmlDocument _doc)
        {
            XmlNode node = _doc.CreateElement("Registrator");

            XmlAttribute attr = node.Attributes.Append(_doc.CreateAttribute("Name"));
            attr.Value = Name;

            foreach (var tournament in mTournaments)
            {
                node.AppendChild(tournament.ToXML(_doc));
            }
            
            return node;
        }

        public void FromXML(XmlNode _node)
        {
            foreach (XmlNode tournXML in _node.ChildNodes)
            {
                Tournament tournament = new Tournament();
                tournament.FromXML(tournXML);
                mTournaments.Add(tournament);
            }
            UpdateStatistics();
        }

        public void Clear()
        {
            mTournaments.Clear();
            UpdateStatistics();
        }

        public void SetUregisteredAll()
        {
            foreach (var trnmnt in mTournaments)
            {
                trnmnt.SetUnregistered();
            }
            UpdateStatistics();
            UpdateColor();
        }

        public Tournament FindTournament(int _ID)
        {
            foreach (var trnmnt in mTournaments)
            {
                if (trnmnt.ID == _ID)
                {
                    Logger.Write("Registrator " + Name + " find tournament ID=" + trnmnt.ID, Logger.L.DEBUG);
                    return trnmnt;
                }
            }
            return null;
        }
    }
}
