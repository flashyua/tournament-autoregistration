﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoReg
{
    enum DelayedTaskType
    {
        DELAY,
        EVERYSECOND,
        EVERYMINUTE,
        EVERYHOUR,
        TIME
    }
    static class TaskManager
    {
        //private static Queue<ITask> mTaskQueue = new Queue<ITask>();
        private static TaskQueue<ITask> mTaskQueue = new TaskQueue<ITask>();
        private static List<Tournament> mToRegister = new List<Tournament>();
        private static List<DelayedTask> mDelayedTasks = new List<DelayedTask>();
        private static long mTaskCounter = 0;

        private struct DelayedTask
        {
            public ITask Task;
            public int Delay; // in ticks
            public DateTime Time;
            public DelayedTaskType Type;
        }

        public static bool IfFreeToRegister
        {
            get
            {
                if (mToRegister.Count == 0)
                {
                    return true;
                }
                else 
                    return false;
            }
        }

        public static void AddTask(ITask task, TaskPriority priority)
        {
            task.TaskID = mTaskCounter++;
            Logger.Write("Adding task (" + task.ToString() + ") TaskID = " + task.TaskID, Logger.L.DEBUG);
            mTaskQueue.Enqueue(task, priority);
        }

        public static void AddDelayedTask(ITask task, int delay)
        {
            task.TaskID = mTaskCounter++;
            Logger.Write("Adding delayed task (" + task.ToString() + ") TaskID = " + task.TaskID, Logger.L.DEBUG);
            DelayedTask dtask = new DelayedTask();
            dtask.Task = task;
            dtask.Delay = delay;
            dtask.Type = DelayedTaskType.DELAY;
            mDelayedTasks.Add(dtask);
        }

        public static void AddTimeTask(ITask task, DateTime time)
        {
            task.TaskID = mTaskCounter++;
            Logger.Write("Adding timed task (" + task.ToString() + ") TaskID = " + task.TaskID + " to time: " + time.ToString(), Logger.L.DEBUG);
            DelayedTask dtask = new DelayedTask();
            dtask.Task = task;
            dtask.Time = time;
            dtask.Type = DelayedTaskType.TIME;
            mDelayedTasks.Add(dtask);
        }

        public static void RunDelayedTasks()
        {
            for (var i = 0; i < mDelayedTasks.Count; i++)
            {
                DelayedTask dtask = mDelayedTasks[i];
                if (dtask.Type == DelayedTaskType.DELAY)
                {
                    if (dtask.Delay == 0)
                    {
                        Logger.Write("Executing delayed task (" + dtask.Task.ToString() + ")", Logger.L.DEBUG);
                        dtask.Task.Execute();
                        mDelayedTasks.RemoveAt(i);
                    }
                    else
                    {
                        dtask.Delay = dtask.Delay - 1;
                        mDelayedTasks[i] = dtask;
                    }
                }
                else if (dtask.Type == DelayedTaskType.TIME)
                {
                    if (DateTime.Now > dtask.Time)
                    {
                        Logger.Write("Executing delayed task (" + dtask.Task.ToString() + ")", Logger.L.DEBUG);
                        dtask.Task.Execute();
                        mDelayedTasks.RemoveAt(i);
                    }
                }
            }
        }

        public static void DoingTasks()
        {
            while (true)
            {
                while (mTaskQueue.Count != 0)
                {
                    ITask temp = mTaskQueue.Dequeue();
                    Logger.Write("Dequeing task to execute (" + temp.ToString() + ")", Logger.L.DEBUG);
                    if(temp != null) temp.Execute();
                }
                RunDelayedTasks();
                System.Threading.Thread.Sleep(200);
            }
        }

        public static bool PushToRegister(Tournament tournament)
        {
            if (mToRegister.Count == 0)
            {
                Logger.Write("Pushing tournament to register!", Logger.L.DEBUG);
                mToRegister.Add(tournament);
                return true;
            }
            else
            {
                return false;
            }
        }

        public static Tournament GetToRegister()
        {
            if (mToRegister.Count != 0)
            {
                return mToRegister[0];
            }
            else
            {
                Logger.Write("No tournament to register in task queue!", Logger.L.DEBUG);
                return null;
            }
        }

        public static void FreeToRegister()
        {
            Logger.Write("Freeing tournament to register!", Logger.L.DEBUG);
            mToRegister.Clear();
        }
    }
}
