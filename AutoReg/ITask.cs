﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoReg
{
    interface ITask
    {
        long TaskID
        {
            get;
            set;
        }
        bool Execute();
    }

    class TaskSessionRunRegistrator : ITask
    {
        public long TaskID
        {
            get;
            set;
        }
        private int mID;

        public int ID
        {
            get;
            set;
        }

        public TaskSessionRunRegistrator(int _ID)
        {
            ID = _ID;
        }

        public bool Execute()
        {
            Session.RunRegistrator(ID);
            return true;
        }

        public override string ToString()
        {
            return String.Format("Task: {0} (Task ID: {1}), Registrator ID: {2}", this.GetType().Name, this.TaskID, this.ID);
        }
    }

    class TaskTournamentStateChange : ITask
    {
        public long TaskID
        {
            get;
            set;
        }

        public delegate void TournamentStatusChanged(int iID, TournamentState state);
        public static event TournamentStatusChanged OnTournamentStatusChanged;

        private int mID;
        private TournamentState mState;

        public int ID
        {
            get;
            set;
        }

        public TournamentState State
        {
            get;
            set;
        }

        public TaskTournamentStateChange(int _ID, TournamentState _State)
        {
            ID = _ID;
            State = _State;
        }

        public bool Execute()
        {
            //Logger.Write("Executing 'TaskTournamentStateChange' (ID=" + ID + ", State=" + State.ToString() + ")", Logger.L.DEBUG);
            Tournament trnmnt = Session.FindTournament(ID);
            if (trnmnt != null)
            {
                trnmnt.OnTournamentStateChanged(State);
            }
            return true;
        }

        public override string ToString()
        {
            return String.Format("Task: {0}(Task ID: {1}), Tournament ID: {2}, State: {3}", this.GetType().Name, this.TaskID, this.ID, this.State);
        }
    }

    class TaskTournamentRegister : ITask
    {
        public long TaskID
        {
            get;
            set;
        }

        private int mID;

        public int ID
        {
            get;
            set;
        }

        public TaskTournamentRegister(int _ID)
        {
            ID = _ID;
        }

        public bool Execute()
        {
            //Logger.Write("Executing 'TaskTournamentRegister' (ID=" + ID + ")", Logger.L.DEBUG);
            Tournament trnmnt = TaskManager.GetToRegister();
            if (trnmnt != null)
            {
                trnmnt.ID = ID;
                trnmnt.OnTournamentStateChanged(TournamentState.REGISTERED);
                TaskManager.FreeToRegister();
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return String.Format("Task: {0}(Task ID: {1}), Tournament ID: {2}", this.GetType().Name, this.TaskID, this.ID);
        }
    }

    class TaskRegisterOneTournament : ITask
    {
        public long TaskID
        {
            get;
            set;
        }

        private Registrator mRegistrator;
        private int mOffset;

        public TaskRegisterOneTournament(Registrator registrator, int offset)
        {
            mRegistrator = registrator;
            mOffset = offset;
        }

        public bool Execute()
        {
            return mRegistrator.RegisterOneTournament(mOffset);
        }
    }

    class TaskShowMsg : ITask
    {
        public long TaskID
        {
            get;
            set;
        }

        public bool Execute()
        {
            Logger.Write("Test task!", Logger.L.WARN, true, true);
            return true;
        }
    }

    class TaskPauseSession : ITask
    {
        public long TaskID
        {
            get;
            set;
        }

        public bool Execute()
        {
            Session.Pause();
            //TaskManager.AddTimeTask(new TaskStartSession(), DateTime.Now.Minute
            return Session.UnregisterAll();
        }
    }

    class TaskStartSession : ITask
    {
        public long TaskID
        {
            get;
            set;
        }

        public bool Execute()
        {
            if (Session.RunningState == SessionRunningState.PAUSED)
            {
                //Program.MainWindow.bt_pause_Click();
            }
            return true;
        }
    }

    class TaskRestartParser : ITask
    {
        public long TaskID
        {
            get;
            set;
        }

        public bool Execute()
        {
            //PokerstarsParser.Init();
            PokerstarsParser.ResetCounter();
            //Logger.Write("Parser is restarted!", Logger.L.STATUS);
            TaskManager.AddTimeTask(new TaskRestartParser(), DateTime.Today.AddDays(1).AddMilliseconds(5000));
            //TaskManager.AddTimeTask(new TaskRestartParser(), DateTime.Now.AddMilliseconds(2000));
            return true;
        }
    }
}
