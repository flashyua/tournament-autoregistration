using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Xml;

namespace AutoReg
{
    class Tournament
    {
        public int ID = -1;
        public int ParentID = -1;
        public DateTime RegistrationTime =  new DateTime();
        public int Status = 0;

        private IntPtr TLobbyHndl = IntPtr.Zero;
        private int Offset = 0;

        public struct Statuses
        {
            public const int UNKNOWN = 0;
            public const int REGISTERED = 1;
            public const int OPENED = 2;
            public const int CLOSED = 3;
            public const int CURRENTLYREGISTERED = 4;
            public const int UNREGISTERED = 5;
            public const int ERROR = 10;
        }

        public Tournament()
        {
            this.Status = Statuses.UNKNOWN;
        }

        public void Register()
        {
            Logger.Write("Registering tournament...", Logger.L.DEBUG);
            IntPtr regBtn = GetRegButton();
            string capt = WinWrapper.GetText(regBtn);
            if (capt == "Register")
            {
                PokerstarsParser.onRegisterTournament += onRegister;
                ConfirmRegistration(regBtn);
            }
            else if (capt == "Unregister")
            {
                this.Status = Statuses.CURRENTLYREGISTERED;
                Logger.Write("Tournament already registered", Logger.L.DEBUG);
            }
        }

        private void onRegister(int tID)
        {
            ID = tID;
            if (ID == 0)
            {
                Logger.Write("Tournament ID returned is " + ID + ". Registration Error!", Logger.L.DEBUG);
                this.Status = Statuses.ERROR;
            }
            else
            {
                RegistrationTime = DateTime.Now;
                Status = Statuses.REGISTERED;
            }
            PokerstarsParser.onRegisterTournament -= onRegister;
            Logger.Write("Tournament " + tID.ToString() + " registered! (in log)", Logger.L.DEBUG);
            //ParentReg.RegisterCallback(this);
        }





        public void Unregister()
        {
            Logger.Write("Unregistering tournament...", Logger.L.DEBUG);
            IntPtr regBtn = GetRegButton();
            ID = GetTournamentID(Offset);
            ConfirmRegistration(regBtn);
            Logger.Write("Unregistering!", Logger.L.INFO);
            Status = Statuses.UNREGISTERED;
        }

        private IntPtr GetRegButton()
        {
            WinWrapper.ScrollListBox(PokerStarsLobby.MainWindow, Offset, 10);
            IntPtr regBtn = IntPtr.Zero;
            do{
                regBtn = WinWrapper.FindWindowEx(PokerStarsLobby.MainWindow, regBtn, null, null);
                string capt = WinWrapper.GetText(regBtn);
                if (((capt == "Register") || (capt == "Unregister")) && WinWrapper.IsWindowVisible(regBtn))
                {
                    Logger.Write("\"" + capt + "\" is allowed", Logger.L.DEBUG);
                    return regBtn;
                }
            } while (regBtn != IntPtr.Zero);
            return IntPtr.Zero;
        }

        private int GetTournamentID(int offset) // Открываем лобби и вытаскиеваем ИД турнира
        {
            WinWrapper.SendMessage(PokerStarsLobby.TournamentListBox, (uint)0x100, 0x0D, 0);
            WinWrapper.SendMessage(PokerStarsLobby.TournamentListBox, (uint)0x101, 0x0D, 0);
            Logger.Write("Opening Lobby", Logger.L.DEBUG);

            ID = FindTournamentLobby();

            WinWrapper.SendMessage(TLobbyHndl, 0x0010, 0, 0);
            Logger.Write("Closing Lobby " + ID.ToString(), Logger.L.DEBUG);
            
            return ID;
        }

        private int FindTournamentLobby()
        {
            //IntPtr TLobbyHndl = IntPtr.Zero;
            Regex regex = new Regex("Tournament ([0-9]*) Lobby");
            IntPtr desktopWnd = WinWrapper.GetDesktopWindow();
            for (int i = 0; i < 50; i++)
            {
                TLobbyHndl = WinWrapper.FindWindowEx(desktopWnd, TLobbyHndl, "#32770", null);
                string capt = WinWrapper.GetText(TLobbyHndl);
                if (regex.IsMatch(capt))
                {
                    Match match = regex.Match(capt);
                    return int.Parse(match.Groups[1].Value);
                }
            }
            return -1;
        }

        private bool ConfirmRegistration(IntPtr hBtnReg) // ПОдтверждение регистрации в одном турнире
        {
            
            WinWrapper.SetActiveWindow(hBtnReg);
            WinWrapper.SendMessage(hBtnReg, (uint)0x100, (int)0x20, 0); // Отправляем ебаный ПРОБЕЛ ебаной кнопке
            WinWrapper.SendMessage(hBtnReg, (uint)0x101, (int)0x20, 0);
            //Thread.Sleep(int.Parse(Configurator.GetSetting("TimingWaitToConfirm", "0")));
            //RegistratorOld.WaitForConfirm = true;
            //Logger.Write("Waiting for closing messagebox...", Logger.L.DEBUG);
            //if (closeNotification())
            //{
            //    Logger.Write("Notification #1 OK!", Logger.L.DEBUG);
            //    Thread.Sleep((Configurator.GetSetting("TimingWaitToConfirm") != null ? int.Parse(Configurator.GetSetting("TimingWaitToConfirm")) : 500));
            //    bool t = closeNotification();
            //    Logger.Write(t.ToString(), Logger.L.DEBUG);
            //    if (!t) // Проверка, если ещё одно окно (регистратио из клозед) не закрыли - ОК!
            //    {
            //        Logger.Write("Notification #2 OK!", Logger.L.DEBUG);
            //        return true;
            //    }
            //    else
            //    {
            //        Logger.Write("Registration to tournament " + ID.ToString() + " is closed!", Logger.L.INFO);
            //        return false;
            //    }
            //}
            //else
            //{
            //    Logger.Write("Cannot confirm registration to tournament " + ID.ToString(), Logger.L.INFO);
            //    return false;
            //}
            return true;
        }

        private bool closeNotification()
        {
            
            Logger.Write("Not found notifications! ", Logger.L.DEBUG);
            return false;
        }

        public bool FromXML(XmlNode node)
        {
            
            ID = int.Parse(node.Attributes[0].Value);
            ParentID = int.Parse(node.Attributes[1].Value);
            RegistrationTime = DateTime.Parse(node.Attributes[2].Value);
            Status = int.Parse(node.Attributes[3].Value);
            Offset = int.Parse(node.Attributes[4].Value);

            return true;
        }

        public XmlNode ToXML(XmlDocument doc)
        {
            XmlNode node = doc.CreateElement("Tournament");
            //doc.AppendChild(node);

            XmlAttribute attr = doc.CreateAttribute("ID");
            attr.Value = ID.ToString();
            node.Attributes.Append(attr);
            attr = doc.CreateAttribute("ParentID");
            attr.Value = ParentID.ToString();
            node.Attributes.Append(attr);
            attr = doc.CreateAttribute("RegistrationTime");
            attr.Value = RegistrationTime.ToString();
            node.Attributes.Append(attr);
            attr = doc.CreateAttribute("Status");
            attr.Value = this.Status.ToString();
            node.Attributes.Append(attr);
            attr = doc.CreateAttribute("Offset");
            attr.Value = Offset.ToString();
            node.Attributes.Append(attr);

            return node;
        }


    }
}
