﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace AutoReg
{
    static class ErrorMessager
    {

        public static void LobbyNotFound()
        {
            string msg = "Main Pokerstars Lobby not found!";
            MessageBox.Show(msg);
            //Logger.Write(msg);
        }

        public static void CustomMessage(string msg)
        {
            MessageBox.Show(msg);
            //Logger.Write(msg);
        }
    }
}
