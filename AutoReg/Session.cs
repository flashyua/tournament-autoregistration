﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Xml;

namespace AutoReg
{
    enum SessionRunningState
    {
        RUNNING,
        PAUSED,
        STOPPED,
    }

    enum SessionLimitState
    {
        NOLIMIT,
        KEEPMAX,
        TOTALMAX,
        ALLLIMIT
    }

    static class Session 
    {
        private static SessionRunningState mRunningState = SessionRunningState.STOPPED;
        private static SessionLimitState mLimitState = SessionLimitState.NOLIMIT;

        public static List<Registrator> mRegistratorPool = new List<Registrator>();

        private static Thread mThread;
        private static int mKeepMax = 0;
        private static int mKeepNow = 0;
        private static int mTotalMax = 0;
        private static int mTotalNow = 0;
        private static bool IfLoaded = false;

        #region Properties

        public static SessionRunningState RunningState
        {
            get
            {
                return mRunningState;
            }
            private set
            {
                mRunningState = value;
                Logger.Write("Session running state changed to: " + mRunningState.ToString(), Logger.L.DEBUG);
            }
        }

        public static SessionLimitState LimitState
        {
            get
            {
                return mLimitState;
            }
            private set
            {
                mLimitState = value;
                Program.MainWindow.BeginInvoke(new MethodInvoker(delegate()
                {
                    if (mLimitState == SessionLimitState.KEEPMAX) Program.MainWindow.lb_sesKeepNow.BackColor = System.Drawing.Color.FromArgb(255, 255, 77);
                    else if (mLimitState == SessionLimitState.TOTALMAX) Program.MainWindow.lb_sesTotalNow.BackColor = System.Drawing.Color.FromArgb(255, 77, 77);
                    else if (mLimitState == SessionLimitState.ALLLIMIT)
                    {
                        Program.MainWindow.lb_sesKeepNow.BackColor = System.Drawing.Color.FromArgb(255, 255, 77);
                        Program.MainWindow.lb_sesTotalNow.BackColor = System.Drawing.Color.FromArgb(255, 77, 77);
                    }
                    else
                    {
                        Program.MainWindow.lb_sesKeepNow.BackColor = System.Drawing.Color.White;
                        Program.MainWindow.lb_sesTotalNow.BackColor = System.Drawing.Color.White;
                    }
                }));
                Logger.Write("Session limit state changed to: " + mLimitState.ToString(), Logger.L.DEBUG);
            }
        }

        public static int KeepNow
        {
            get
            {
                return mKeepNow;
            }
            private set
            {
                mKeepNow = value;
                if (mKeepNow >= KeepMax)
                {
                    if (LimitState == SessionLimitState.TOTALMAX) LimitState = SessionLimitState.ALLLIMIT;
                    else LimitState = SessionLimitState.KEEPMAX;
                }
                else if (LimitState == SessionLimitState.ALLLIMIT) LimitState = SessionLimitState.TOTALMAX;
                else if (LimitState == SessionLimitState.KEEPMAX)
                {
                    LimitState = SessionLimitState.NOLIMIT;
                    Session.Start();
                }
                Program.MainWindow.BeginInvoke(new MethodInvoker(delegate()
                {
                    Program.MainWindow.lb_sesKeepNow.Text = mKeepNow.ToString();
                    Program.MainWindow.StatisticsBox.lb_keepNowStat.Text = mKeepNow.ToString();
                }));
            }
        }

        public static int KeepMax
        {
            get
            {
                return mKeepMax;
            }
            set
            {
                mKeepMax = value;
                UpdateSession();
            }
        }

        public static int TotalMax
        {
            get
            {
                return mTotalMax;
            }
            set
            {
                mTotalMax = value;
                UpdateSession();
            }
        }

        public static int TotalNow
        {
            get
            {
                return mTotalNow;
            }
            private set
            {
                mTotalNow = value;
                if (mTotalNow >= TotalMax)
                {
                    if (LimitState == SessionLimitState.KEEPMAX) LimitState = SessionLimitState.ALLLIMIT;
                    else LimitState = SessionLimitState.TOTALMAX;
                }
                else if (LimitState == SessionLimitState.ALLLIMIT) LimitState = SessionLimitState.KEEPMAX;
                else if (LimitState == SessionLimitState.TOTALMAX) LimitState = SessionLimitState.NOLIMIT;
                Program.MainWindow.BeginInvoke(new MethodInvoker(delegate()
                {
                    Program.MainWindow.lb_sesTotalNow.Text = mTotalNow.ToString();
                    Program.MainWindow.StatisticsBox.lb_totalNowStat.Text = mTotalNow.ToString();
                }));
            }
        }

        #endregion

        public static void Init()
        {
            mThread = new Thread(delegate()
            {
                TaskManager.DoingTasks();
            });
            mThread.Name = "Session thread";
            mThread.SetApartmentState(ApartmentState.STA);
            mThread.Start();
        }

        public static void RemoveAt(int index)
        {
            mRegistratorPool[index].AssocLvItem.Remove();
            mRegistratorPool.RemoveAt(index);
        }

        public static void Start()
        {
            if (Settings.User.IfUnregisterAtTime)
            {

            }

            RunningState = AutoReg.SessionRunningState.RUNNING;
            Logger.Write("Session is started!", Logger.L.STATUS);
            foreach (var registrator in mRegistratorPool)
            {
                if (registrator.IsActive)
                {
                    registrator.SetReady();
                    registrator.Start();
                }
            }
            Program.MainWindow.bt_start.Enabled = false;
            Program.MainWindow.bt_pause.Text = "Pause";
            Program.MainWindow.bt_pause.Enabled = true;
            Program.MainWindow.bt_stop.Enabled = true;
        }

        public static Registrator AddRegistrator()
        {
            Registrator reg = new Registrator();
            Program.MainWindow.lw_mainFilter.Items.Add(reg.AssocLvItem);
            
            reg.Index = mRegistratorPool.Count;
            Session.mRegistratorPool.Add(reg);
            return reg;
        }

        public static void RunRegistrator(int _index)
        {
            if (LimitState != SessionLimitState.NOLIMIT)
            {
                Logger.Write("Session limit is reached: " + LimitState.ToString(), Logger.L.DEBUG);
                return;
            }
            System.Threading.Thread.Sleep(500);
            if (_index >= mRegistratorPool.Count)
            {
                Logger.Write("RunRegistrator: index is not valid!", Logger.L.DEBUG);
                return;
            }
            mRegistratorPool[_index].Start();
            //if(!mRegistratorPool[_index].Start())
            //{
            //    RunRegistrator(_index + 1);
            //}
        }

        public static bool Pause()
        {
            if (RunningState == SessionRunningState.RUNNING)
            {
                RunningState = AutoReg.SessionRunningState.PAUSED;
                Logger.Write("Session is paused!", Logger.L.STATUS);
                Program.MainWindow.bt_pause.Text = "Continue";
                return true;
            }
            else if (RunningState == SessionRunningState.PAUSED)
            {
                RunningState = SessionRunningState.RUNNING;
                Session.Start();
                Logger.Write("Session is continued!", Logger.L.STATUS);
                Program.MainWindow.bt_pause.Text = "Pause";
                return false;
            }
            else
            {
                Logger.Write("Session is not paused!", Logger.L.STATUS);
                return false;
            }
        }

        public static void Stop()
        {
            RunningState = AutoReg.SessionRunningState.STOPPED;
            foreach (var registrator in mRegistratorPool)
            {
                registrator.Stop();
            }
            //if(mThread != null) mThread.Abort();
            Logger.Write("Session is stopped!", Logger.L.STATUS);
            Program.MainWindow.BeginInvoke(new MethodInvoker(delegate(){
                Program.MainWindow.bt_start.Enabled = true;
                Program.MainWindow.bt_pause.Enabled = false;
                Program.MainWindow.bt_stop.Enabled = false;
            }));
        }

        public static void StopThread()
        {
            if (mThread != null) mThread.Abort();
        }

        public static void SessionChanged()
        {
            UpdateSession();
        }

        public static void UpdateSession()
        {
            int openNow = 0;
            int totalNow = 0;
            foreach (var registrator in mRegistratorPool)
            {
                openNow += registrator.KeepNow;
                totalNow += registrator.TotalNow;
            }
            KeepNow = openNow;
            TotalNow = totalNow;
            foreach (var registrator in mRegistratorPool)
            {
                registrator.UpdateLimitState();
            }
           
            Write();
            Logger.Write("Current state of session:" + 
                " KN=" + KeepNow + "(" + KeepMax + ")" +
                " TN=" + TotalNow + "(" + TotalMax + ")", Logger.L.DEBUG);
        }

        public static void Write()
        {
            if (IfLoaded)
            {
                XmlDocument doc = new XmlDocument();
                XmlNode head = doc.AppendChild(doc.CreateElement("Session"));
                foreach (var registrator in mRegistratorPool)
                {
                    head.AppendChild(registrator.ToXML(doc));
                }
                doc.Save("session.bak");
            }
        }

        public static void Load()
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load("session.bak");
                foreach (XmlNode node in doc.ChildNodes[0])
                {
                    string name = "";
                    foreach (XmlAttribute attr in node.Attributes)
                    {
                        switch (attr.Name)
                        {
                            case "Name": name = attr.Value; break;
                        }
                    }
                    foreach (var registrator in mRegistratorPool)
                    {
                        if (registrator.Name == name)
                        {
                            registrator.FromXML(node);
                        }
                    }
                }
            }
            catch (XmlException e)
            {
                Logger.Write("Error parsing saved tournaments. Nothing loaded! " + e.Message, Logger.L.ERROR);
            }
            catch (System.IO.IOException e)
            {
                Logger.Write("Error loading saved tournaments. Nothing loaded! " + e.Message, Logger.L.ERROR);
            }
            catch
            {
                Logger.Write("Unknown error loading saved tournaments!", Logger.L.ERROR);
            }
            IfLoaded = true;
        }

        public static void Clear() // Clearing session
        {
            Logger.Write("Clearing session", Logger.L.DEBUG);
            Program.MainWindow.tb_status.Text = "";
            foreach (var reg in mRegistratorPool)
            {
                reg.Clear();
            }
            UpdateSession();
        }

        public static bool UnregisterAll() // Отменяет регистрацию во всех турнирах через Ктрл+Р
        {
            Logger.Write("Unregistering from all possible tournaments...", Logger.L.INFO);
            WinWrapper.ShowWindow(PokerStarsLobby.MainWindow, 9);
            WinWrapper.SetForegroundWindow(PokerStarsLobby.MainWindow);
            WinWrapper.keybd_event(0x11, 0, 0, 0);
            WinWrapper.keybd_event(0x52, 0, 0, 0);
            WinWrapper.keybd_event(0x52, 0, 2, 0);
            WinWrapper.keybd_event(0x11, 0, 2, 0);

            IntPtr regInHndl = IntPtr.Zero;
            for (int i = 0; i < 10; i++)
            {
                regInHndl = WinWrapper.FindWindow("#32770", "Registered In Tournaments");
                Thread.Sleep(25);
            }
            bool success = false;
            IntPtr selAllChk = IntPtr.Zero;
            do
            {
                Thread.Sleep(600);
                selAllChk = WinWrapper.FindWindowEx(regInHndl, selAllChk, "PokerStarsButtonClass", null);
                WinWrapper.RECT rct;
                WinWrapper.GetWindowRect(selAllChk, out rct);
                Logger.Write("Window size is " + (rct.Right - rct.Left) + "x" + (rct.Bottom - rct.Top), Logger.L.DEBUG);
                if ((rct.Right - rct.Left == 15) && (rct.Bottom - rct.Top == 15))
                {
                    Logger.Write("\"Registering in ...\" window and checkbox finded!", Logger.L.DEBUG);
                    WinWrapper.SetForegroundWindow(selAllChk);
                    WinWrapper.SendMessage(selAllChk, 0x100, 0x20, 0);
                    WinWrapper.SendMessage(selAllChk, 0x101, 0x20, 0);
                    IntPtr unRegBtn = IntPtr.Zero;
                    do
                    {
                        unRegBtn = WinWrapper.FindWindowEx(regInHndl, unRegBtn, null, null);
                        string capt = WinWrapper.GetText(unRegBtn);
                        if (capt == "Unregister")
                        {
                            //WinWrapper.SetForegroundWindow(selAllChk);
                            WinWrapper.SendMessage(unRegBtn, 0x100, 0x20, 0);
                            WinWrapper.SendMessage(unRegBtn, 0x101, 0x20, 0);
                            IntPtr okWnd = IntPtr.Zero;
                            for (int i = 0; i < 10; i++)
                            {
                                okWnd = WinWrapper.FindWindow("#32770", "Pokerstars");
                                Thread.Sleep(25);
                            }
                            if (okWnd != IntPtr.Zero)
                            {
                                WinWrapper.SendMessage(okWnd, 0x0010, 0, 0);
                                success = true;
                            }
                            break;
                        }
                    } while (unRegBtn != IntPtr.Zero);
                }
            } while (selAllChk != IntPtr.Zero);
            if (!success) Logger.Write("You have no registered tournaments now!", Logger.L.INFO);
            WinWrapper.SetForegroundWindow(regInHndl);
            WinWrapper.SendMessage(regInHndl, 0x0010, 0, 0);
            foreach (Registrator reg in mRegistratorPool)
            {
                reg.SetUregisteredAll();
            }
            Logger.Write("All possible tournaments is unregistered!", Logger.L.STATUS);

            return true;
        }

        public static Tournament FindTournament(int _ID)
        {
            foreach (var reg in mRegistratorPool)
            {
                Tournament trnmnt = reg.FindTournament(_ID);
                if (trnmnt != null)
                {
                    //Logger.Write("Session find tournament ID=" + trnmnt.ID, Logger.L.DEBUG);
                    return trnmnt;
                }
            }
            Logger.Write("Session didn't find tournament ID=" + _ID, Logger.L.DEBUG);
            return null;
        }
    }
}
