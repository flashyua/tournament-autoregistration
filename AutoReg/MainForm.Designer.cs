﻿namespace AutoReg
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.bt_stop = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showStatisticsWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.enableLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sessionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSessionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unregisterAllTournamentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FiltersItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filtersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFiltersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutTournamentAutoregistrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertWarningInLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkAliveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeLastReadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetCounterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lw_mainFilter = new System.Windows.Forms.ListView();
            this.shortName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CurrentlyOpened = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.maxRegistering = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MaxPerCycle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TotalRegistered = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TargetRegister = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SessionLimit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.filterDetails = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bt_testFilter = new System.Windows.Forms.Button();
            this.nb_sessionLimit = new System.Windows.Forms.NumericUpDown();
            this.nb_totalMax = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.nb_maxCycle = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bt_deleteRow = new System.Windows.Forms.Button();
            this.nb_keepMax = new System.Windows.Forms.NumericUpDown();
            this.bt_EditRow = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.bt_addRow = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_filterDetails = new System.Windows.Forms.TextBox();
            this.tb_shortName = new System.Windows.Forms.TextBox();
            this.bt_start = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cb_upTime = new System.Windows.Forms.CheckBox();
            this.cb_minutes = new System.Windows.Forms.CheckBox();
            this.cb_totalReg = new System.Windows.Forms.CheckBox();
            this.cb_keepMax = new System.Windows.Forms.CheckBox();
            this.dp_sesTime = new System.Windows.Forms.DateTimePicker();
            this.nb_sesMinutes = new System.Windows.Forms.NumericUpDown();
            this.nb_sesMaxTotal = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nb_sesKeepMax = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lb_sesTime = new System.Windows.Forms.Label();
            this.lb_sesMinutes = new System.Windows.Forms.Label();
            this.lb_sesTotalNow = new System.Windows.Forms.Label();
            this.lb_sesKeepNow = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_status = new System.Windows.Forms.TextBox();
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.bt_pause = new System.Windows.Forms.Button();
            this.ss_main = new System.Windows.Forms.StatusStrip();
            this.ss_lb_parser = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nb_sessionLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_totalMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_maxCycle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_keepMax)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nb_sesMinutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_sesMaxTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_sesKeepMax)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.ss_main.SuspendLayout();
            this.SuspendLayout();
            // 
            // bt_stop
            // 
            this.bt_stop.Enabled = false;
            this.bt_stop.Location = new System.Drawing.Point(705, 311);
            this.bt_stop.Name = "bt_stop";
            this.bt_stop.Size = new System.Drawing.Size(100, 30);
            this.bt_stop.TabIndex = 0;
            this.bt_stop.Text = "Stop!";
            this.bt_stop.UseVisualStyleBackColor = true;
            this.bt_stop.Click += new System.EventHandler(this.stop_btn_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenuItem,
            this.sessionMenuItem,
            this.FiltersItem,
            this.aboutToolStripMenuItem,
            this.debugToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(817, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showStatisticsWindowToolStripMenuItem,
            this.toolStripMenuItem4,
            this.toolStripMenuItem3,
            this.enableLogToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.FileMenuItem.Name = "FileMenuItem";
            this.FileMenuItem.Size = new System.Drawing.Size(37, 20);
            this.FileMenuItem.Text = "File";
            // 
            // showStatisticsWindowToolStripMenuItem
            // 
            this.showStatisticsWindowToolStripMenuItem.Name = "showStatisticsWindowToolStripMenuItem";
            this.showStatisticsWindowToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.showStatisticsWindowToolStripMenuItem.Text = "Show Statistics window";
            this.showStatisticsWindowToolStripMenuItem.Click += new System.EventHandler(this.showStatisticsWindowToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(197, 22);
            this.toolStripMenuItem4.Text = "Load Config";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(197, 22);
            this.toolStripMenuItem3.Text = "Save Config";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // enableLogToolStripMenuItem
            // 
            this.enableLogToolStripMenuItem.Checked = true;
            this.enableLogToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enableLogToolStripMenuItem.Name = "enableLogToolStripMenuItem";
            this.enableLogToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.enableLogToolStripMenuItem.Text = "Enable log";
            this.enableLogToolStripMenuItem.Click += new System.EventHandler(this.enableLogToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // sessionMenuItem
            // 
            this.sessionMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetSessionToolStripMenuItem,
            this.unregisterAllTournamentsToolStripMenuItem});
            this.sessionMenuItem.Name = "sessionMenuItem";
            this.sessionMenuItem.Size = new System.Drawing.Size(58, 20);
            this.sessionMenuItem.Text = "Session";
            // 
            // resetSessionToolStripMenuItem
            // 
            this.resetSessionToolStripMenuItem.Name = "resetSessionToolStripMenuItem";
            this.resetSessionToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.resetSessionToolStripMenuItem.Text = "Reset Session";
            this.resetSessionToolStripMenuItem.Click += new System.EventHandler(this.resetSessionToolStripMenuItem_Click);
            // 
            // unregisterAllTournamentsToolStripMenuItem
            // 
            this.unregisterAllTournamentsToolStripMenuItem.Name = "unregisterAllTournamentsToolStripMenuItem";
            this.unregisterAllTournamentsToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.unregisterAllTournamentsToolStripMenuItem.Text = "Unregister all tournaments";
            this.unregisterAllTournamentsToolStripMenuItem.Click += new System.EventHandler(this.unregisterAllTournamentsToolStripMenuItem_Click);
            // 
            // FiltersItem
            // 
            this.FiltersItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filtersToolStripMenuItem,
            this.saveFiltersToolStripMenuItem});
            this.FiltersItem.Name = "FiltersItem";
            this.FiltersItem.Size = new System.Drawing.Size(50, 20);
            this.FiltersItem.Text = "Filters";
            this.FiltersItem.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // filtersToolStripMenuItem
            // 
            this.filtersToolStripMenuItem.Name = "filtersToolStripMenuItem";
            this.filtersToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.filtersToolStripMenuItem.Text = "Load filters";
            this.filtersToolStripMenuItem.Click += new System.EventHandler(this.loadFilters);
            // 
            // saveFiltersToolStripMenuItem
            // 
            this.saveFiltersToolStripMenuItem.Name = "saveFiltersToolStripMenuItem";
            this.saveFiltersToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.saveFiltersToolStripMenuItem.Text = "Save filters";
            this.saveFiltersToolStripMenuItem.Click += new System.EventHandler(this.saveFilters);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.aboutTournamentAutoregistrationToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.aboutToolStripMenuItem.Text = "Options";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // aboutTournamentAutoregistrationToolStripMenuItem
            // 
            this.aboutTournamentAutoregistrationToolStripMenuItem.Name = "aboutTournamentAutoregistrationToolStripMenuItem";
            this.aboutTournamentAutoregistrationToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.aboutTournamentAutoregistrationToolStripMenuItem.Text = "About Tournament Autoregistration";
            this.aboutTournamentAutoregistrationToolStripMenuItem.Click += new System.EventHandler(this.aboutTournamentAutoregistrationToolStripMenuItem_Click);
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertWarningInLogToolStripMenuItem,
            this.parserToolStripMenuItem});
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.debugToolStripMenuItem.Text = "Debug";
            // 
            // insertWarningInLogToolStripMenuItem
            // 
            this.insertWarningInLogToolStripMenuItem.Name = "insertWarningInLogToolStripMenuItem";
            this.insertWarningInLogToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.insertWarningInLogToolStripMenuItem.Text = "Insert warning in log";
            this.insertWarningInLogToolStripMenuItem.Click += new System.EventHandler(this.insertWarningInLogToolStripMenuItem_Click);
            // 
            // parserToolStripMenuItem
            // 
            this.parserToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.restartToolStripMenuItem,
            this.checkAliveToolStripMenuItem,
            this.writeLastReadToolStripMenuItem,
            this.resetCounterToolStripMenuItem});
            this.parserToolStripMenuItem.Name = "parserToolStripMenuItem";
            this.parserToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.parserToolStripMenuItem.Text = "Parser";
            // 
            // restartToolStripMenuItem
            // 
            this.restartToolStripMenuItem.Name = "restartToolStripMenuItem";
            this.restartToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.restartToolStripMenuItem.Text = "Restart";
            this.restartToolStripMenuItem.Click += new System.EventHandler(this.restartToolStripMenuItem_Click);
            // 
            // checkAliveToolStripMenuItem
            // 
            this.checkAliveToolStripMenuItem.Name = "checkAliveToolStripMenuItem";
            this.checkAliveToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.checkAliveToolStripMenuItem.Text = "Check state";
            this.checkAliveToolStripMenuItem.Click += new System.EventHandler(this.checkAliveToolStripMenuItem_Click);
            // 
            // writeLastReadToolStripMenuItem
            // 
            this.writeLastReadToolStripMenuItem.Name = "writeLastReadToolStripMenuItem";
            this.writeLastReadToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.writeLastReadToolStripMenuItem.Text = "Write last record to log";
            this.writeLastReadToolStripMenuItem.Click += new System.EventHandler(this.writeLastReadToolStripMenuItem_Click);
            // 
            // resetCounterToolStripMenuItem
            // 
            this.resetCounterToolStripMenuItem.Name = "resetCounterToolStripMenuItem";
            this.resetCounterToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.resetCounterToolStripMenuItem.Text = "Reset counter";
            this.resetCounterToolStripMenuItem.Click += new System.EventHandler(this.resetCounterToolStripMenuItem_Click);
            // 
            // lw_mainFilter
            // 
            this.lw_mainFilter.CheckBoxes = true;
            this.lw_mainFilter.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.shortName,
            this.CurrentlyOpened,
            this.maxRegistering,
            this.MaxPerCycle,
            this.TotalRegistered,
            this.TargetRegister,
            this.SessionLimit,
            this.filterDetails});
            this.lw_mainFilter.FullRowSelect = true;
            this.lw_mainFilter.GridLines = true;
            this.lw_mainFilter.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lw_mainFilter.HideSelection = false;
            this.lw_mainFilter.Location = new System.Drawing.Point(11, 27);
            this.lw_mainFilter.MultiSelect = false;
            this.lw_mainFilter.Name = "lw_mainFilter";
            this.lw_mainFilter.Size = new System.Drawing.Size(459, 175);
            this.lw_mainFilter.TabIndex = 6;
            this.lw_mainFilter.UseCompatibleStateImageBehavior = false;
            this.lw_mainFilter.View = System.Windows.Forms.View.Details;
            this.lw_mainFilter.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lw_mainFilter_ItemChecked);
            this.lw_mainFilter.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // shortName
            // 
            this.shortName.Text = "Name";
            this.shortName.Width = 83;
            // 
            // CurrentlyOpened
            // 
            this.CurrentlyOpened.Text = "Cur";
            this.CurrentlyOpened.Width = 35;
            // 
            // maxRegistering
            // 
            this.maxRegistering.Text = "Max";
            this.maxRegistering.Width = 35;
            // 
            // MaxPerCycle
            // 
            this.MaxPerCycle.Text = "MPC";
            this.MaxPerCycle.Width = 35;
            // 
            // TotalRegistered
            // 
            this.TotalRegistered.Text = "TOT";
            this.TotalRegistered.Width = 35;
            // 
            // TargetRegister
            // 
            this.TargetRegister.Text = "Targ";
            this.TargetRegister.Width = 35;
            // 
            // SessionLimit
            // 
            this.SessionLimit.Text = "SL";
            this.SessionLimit.Width = 35;
            // 
            // filterDetails
            // 
            this.filterDetails.Text = "Filter details";
            this.filterDetails.Width = 160;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bt_testFilter);
            this.groupBox1.Controls.Add(this.nb_sessionLimit);
            this.groupBox1.Controls.Add(this.nb_totalMax);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.nb_maxCycle);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.bt_deleteRow);
            this.groupBox1.Controls.Add(this.nb_keepMax);
            this.groupBox1.Controls.Add(this.bt_EditRow);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.bt_addRow);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tb_filterDetails);
            this.groupBox1.Controls.Add(this.tb_shortName);
            this.groupBox1.Location = new System.Drawing.Point(11, 208);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(459, 141);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edit Filters";
            // 
            // bt_testFilter
            // 
            this.bt_testFilter.Location = new System.Drawing.Point(243, 62);
            this.bt_testFilter.Name = "bt_testFilter";
            this.bt_testFilter.Size = new System.Drawing.Size(100, 30);
            this.bt_testFilter.TabIndex = 5;
            this.bt_testFilter.Text = "Test filter";
            this.bt_testFilter.UseVisualStyleBackColor = true;
            this.bt_testFilter.Click += new System.EventHandler(this.bt_testFilter_Click);
            // 
            // nb_sessionLimit
            // 
            this.nb_sessionLimit.Location = new System.Drawing.Point(247, 110);
            this.nb_sessionLimit.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nb_sessionLimit.Name = "nb_sessionLimit";
            this.nb_sessionLimit.Size = new System.Drawing.Size(70, 20);
            this.nb_sessionLimit.TabIndex = 2;
            // 
            // nb_totalMax
            // 
            this.nb_totalMax.Location = new System.Drawing.Point(167, 110);
            this.nb_totalMax.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nb_totalMax.Name = "nb_totalMax";
            this.nb_totalMax.Size = new System.Drawing.Size(70, 20);
            this.nb_totalMax.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(244, 94);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Session limit";
            // 
            // nb_maxCycle
            // 
            this.nb_maxCycle.Location = new System.Drawing.Point(88, 110);
            this.nb_maxCycle.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nb_maxCycle.Name = "nb_maxCycle";
            this.nb_maxCycle.Size = new System.Drawing.Size(70, 20);
            this.nb_maxCycle.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(164, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Total register";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Max per cycle";
            // 
            // bt_deleteRow
            // 
            this.bt_deleteRow.Location = new System.Drawing.Point(349, 26);
            this.bt_deleteRow.Name = "bt_deleteRow";
            this.bt_deleteRow.Size = new System.Drawing.Size(100, 30);
            this.bt_deleteRow.TabIndex = 3;
            this.bt_deleteRow.Text = "Delete filter";
            this.bt_deleteRow.UseVisualStyleBackColor = true;
            this.bt_deleteRow.Click += new System.EventHandler(this.bt_deleteRow_Click);
            // 
            // nb_keepMax
            // 
            this.nb_keepMax.Location = new System.Drawing.Point(9, 110);
            this.nb_keepMax.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nb_keepMax.Name = "nb_keepMax";
            this.nb_keepMax.Size = new System.Drawing.Size(70, 20);
            this.nb_keepMax.TabIndex = 2;
            // 
            // bt_EditRow
            // 
            this.bt_EditRow.Location = new System.Drawing.Point(349, 62);
            this.bt_EditRow.Name = "bt_EditRow";
            this.bt_EditRow.Size = new System.Drawing.Size(100, 30);
            this.bt_EditRow.TabIndex = 3;
            this.bt_EditRow.Text = "Edit filter";
            this.bt_EditRow.UseVisualStyleBackColor = true;
            this.bt_EditRow.Click += new System.EventHandler(this.bt_EditRow_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Keep max";
            // 
            // bt_addRow
            // 
            this.bt_addRow.Location = new System.Drawing.Point(243, 26);
            this.bt_addRow.Name = "bt_addRow";
            this.bt_addRow.Size = new System.Drawing.Size(100, 30);
            this.bt_addRow.TabIndex = 3;
            this.bt_addRow.Text = "Add filter";
            this.bt_addRow.UseVisualStyleBackColor = true;
            this.bt_addRow.Click += new System.EventHandler(this.bt_addRow_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Filter details";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Short name";
            // 
            // tb_filterDetails
            // 
            this.tb_filterDetails.Location = new System.Drawing.Point(6, 71);
            this.tb_filterDetails.Name = "tb_filterDetails";
            this.tb_filterDetails.Size = new System.Drawing.Size(231, 20);
            this.tb_filterDetails.TabIndex = 0;
            // 
            // tb_shortName
            // 
            this.tb_shortName.Location = new System.Drawing.Point(6, 32);
            this.tb_shortName.Name = "tb_shortName";
            this.tb_shortName.Size = new System.Drawing.Size(231, 20);
            this.tb_shortName.TabIndex = 0;
            // 
            // bt_start
            // 
            this.bt_start.Location = new System.Drawing.Point(485, 311);
            this.bt_start.Name = "bt_start";
            this.bt_start.Size = new System.Drawing.Size(100, 30);
            this.bt_start.TabIndex = 0;
            this.bt_start.Text = "Start!";
            this.bt_start.UseVisualStyleBackColor = true;
            this.bt_start.Click += new System.EventHandler(this.start_btn_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(316, 119);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "Test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.stop_btn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cb_upTime);
            this.groupBox2.Controls.Add(this.cb_minutes);
            this.groupBox2.Controls.Add(this.cb_totalReg);
            this.groupBox2.Controls.Add(this.cb_keepMax);
            this.groupBox2.Controls.Add(this.dp_sesTime);
            this.groupBox2.Controls.Add(this.nb_sesMinutes);
            this.groupBox2.Controls.Add(this.nb_sesMaxTotal);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.nb_sesKeepMax);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(480, 28);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(327, 62);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Session parameters";
            // 
            // cb_upTime
            // 
            this.cb_upTime.AutoSize = true;
            this.cb_upTime.Checked = true;
            this.cb_upTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_upTime.Enabled = false;
            this.cb_upTime.Location = new System.Drawing.Point(297, 35);
            this.cb_upTime.Name = "cb_upTime";
            this.cb_upTime.Size = new System.Drawing.Size(15, 14);
            this.cb_upTime.TabIndex = 4;
            this.cb_upTime.UseVisualStyleBackColor = true;
            this.cb_upTime.CheckedChanged += new System.EventHandler(this.cb_upTime_CheckedChanged);
            // 
            // cb_minutes
            // 
            this.cb_minutes.AutoSize = true;
            this.cb_minutes.Checked = true;
            this.cb_minutes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_minutes.Enabled = false;
            this.cb_minutes.Location = new System.Drawing.Point(204, 36);
            this.cb_minutes.Name = "cb_minutes";
            this.cb_minutes.Size = new System.Drawing.Size(15, 14);
            this.cb_minutes.TabIndex = 4;
            this.cb_minutes.UseVisualStyleBackColor = true;
            this.cb_minutes.CheckedChanged += new System.EventHandler(this.cb_minutes_CheckedChanged);
            // 
            // cb_totalReg
            // 
            this.cb_totalReg.AutoSize = true;
            this.cb_totalReg.Checked = true;
            this.cb_totalReg.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_totalReg.Location = new System.Drawing.Point(131, 36);
            this.cb_totalReg.Name = "cb_totalReg";
            this.cb_totalReg.Size = new System.Drawing.Size(15, 14);
            this.cb_totalReg.TabIndex = 4;
            this.cb_totalReg.UseVisualStyleBackColor = true;
            this.cb_totalReg.CheckedChanged += new System.EventHandler(this.cb_totalReg_CheckedChanged);
            // 
            // cb_keepMax
            // 
            this.cb_keepMax.AutoSize = true;
            this.cb_keepMax.Checked = true;
            this.cb_keepMax.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_keepMax.Location = new System.Drawing.Point(59, 36);
            this.cb_keepMax.Name = "cb_keepMax";
            this.cb_keepMax.Size = new System.Drawing.Size(15, 14);
            this.cb_keepMax.TabIndex = 4;
            this.cb_keepMax.UseVisualStyleBackColor = true;
            this.cb_keepMax.CheckedChanged += new System.EventHandler(this.cb_keepMax_CheckedChanged);
            // 
            // dp_sesTime
            // 
            this.dp_sesTime.CustomFormat = "HH : mm";
            this.dp_sesTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dp_sesTime.Location = new System.Drawing.Point(234, 33);
            this.dp_sesTime.Name = "dp_sesTime";
            this.dp_sesTime.ShowUpDown = true;
            this.dp_sesTime.Size = new System.Drawing.Size(60, 20);
            this.dp_sesTime.TabIndex = 3;
            // 
            // nb_sesMinutes
            // 
            this.nb_sesMinutes.Location = new System.Drawing.Point(161, 33);
            this.nb_sesMinutes.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nb_sesMinutes.Name = "nb_sesMinutes";
            this.nb_sesMinutes.Size = new System.Drawing.Size(40, 20);
            this.nb_sesMinutes.TabIndex = 2;
            this.nb_sesMinutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nb_sesMinutes.ValueChanged += new System.EventHandler(this.nb_sesMinutes_ValueChanged);
            // 
            // nb_sesMaxTotal
            // 
            this.nb_sesMaxTotal.Location = new System.Drawing.Point(86, 33);
            this.nb_sesMaxTotal.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.nb_sesMaxTotal.Name = "nb_sesMaxTotal";
            this.nb_sesMaxTotal.Size = new System.Drawing.Size(40, 20);
            this.nb_sesMaxTotal.TabIndex = 2;
            this.nb_sesMaxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nb_sesMaxTotal.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nb_sesMaxTotal.ValueChanged += new System.EventHandler(this.nb_sesMaxTotal_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(231, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "To time";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(158, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Minutes";
            // 
            // nb_sesKeepMax
            // 
            this.nb_sesKeepMax.Location = new System.Drawing.Point(15, 33);
            this.nb_sesKeepMax.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nb_sesKeepMax.Name = "nb_sesKeepMax";
            this.nb_sesKeepMax.Size = new System.Drawing.Size(40, 20);
            this.nb_sesKeepMax.TabIndex = 2;
            this.nb_sesKeepMax.TabStop = false;
            this.nb_sesKeepMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nb_sesKeepMax.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nb_sesKeepMax.ValueChanged += new System.EventHandler(this.nb_sesMaxCount_ValueChanged_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(83, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Total register";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Keep max";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lb_sesTime);
            this.groupBox3.Controls.Add(this.lb_sesMinutes);
            this.groupBox3.Controls.Add(this.lb_sesTotalNow);
            this.groupBox3.Controls.Add(this.lb_sesKeepNow);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Location = new System.Drawing.Point(480, 96);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(327, 62);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Statistics";
            // 
            // lb_sesTime
            // 
            this.lb_sesTime.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_sesTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_sesTime.Location = new System.Drawing.Point(257, 33);
            this.lb_sesTime.Margin = new System.Windows.Forms.Padding(3);
            this.lb_sesTime.Name = "lb_sesTime";
            this.lb_sesTime.Padding = new System.Windows.Forms.Padding(3);
            this.lb_sesTime.Size = new System.Drawing.Size(40, 20);
            this.lb_sesTime.TabIndex = 2;
            this.lb_sesTime.Text = "20";
            this.lb_sesTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_sesMinutes
            // 
            this.lb_sesMinutes.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_sesMinutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_sesMinutes.Location = new System.Drawing.Point(172, 33);
            this.lb_sesMinutes.Margin = new System.Windows.Forms.Padding(3);
            this.lb_sesMinutes.Name = "lb_sesMinutes";
            this.lb_sesMinutes.Padding = new System.Windows.Forms.Padding(3);
            this.lb_sesMinutes.Size = new System.Drawing.Size(40, 20);
            this.lb_sesMinutes.TabIndex = 2;
            this.lb_sesMinutes.Text = "20";
            this.lb_sesMinutes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_sesTotalNow
            // 
            this.lb_sesTotalNow.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_sesTotalNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_sesTotalNow.Location = new System.Drawing.Point(92, 33);
            this.lb_sesTotalNow.Margin = new System.Windows.Forms.Padding(3);
            this.lb_sesTotalNow.Name = "lb_sesTotalNow";
            this.lb_sesTotalNow.Padding = new System.Windows.Forms.Padding(3);
            this.lb_sesTotalNow.Size = new System.Drawing.Size(40, 20);
            this.lb_sesTotalNow.TabIndex = 2;
            this.lb_sesTotalNow.Text = "20";
            this.lb_sesTotalNow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_sesKeepNow
            // 
            this.lb_sesKeepNow.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lb_sesKeepNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lb_sesKeepNow.Location = new System.Drawing.Point(20, 33);
            this.lb_sesKeepNow.Margin = new System.Windows.Forms.Padding(3);
            this.lb_sesKeepNow.Name = "lb_sesKeepNow";
            this.lb_sesKeepNow.Padding = new System.Windows.Forms.Padding(3);
            this.lb_sesKeepNow.Size = new System.Drawing.Size(40, 20);
            this.lb_sesKeepNow.TabIndex = 2;
            this.lb_sesKeepNow.Text = "20";
            this.lb_sesKeepNow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lb_sesKeepNow.Click += new System.EventHandler(this.lb_sesKeepNow_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(248, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Remaining";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(158, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Run minutes";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(71, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Currently total";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Currently";
            // 
            // tb_status
            // 
            this.tb_status.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tb_status.Location = new System.Drawing.Point(483, 164);
            this.tb_status.Multiline = true;
            this.tb_status.Name = "tb_status";
            this.tb_status.ReadOnly = true;
            this.tb_status.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_status.Size = new System.Drawing.Size(323, 135);
            this.tb_status.TabIndex = 11;
            // 
            // trayIcon
            // 
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Text = "Tournament autoregistrator";
            this.trayIcon.Visible = true;
            this.trayIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.trayIcon_MouseClick);
            this.trayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.trayIcon_MouseDoubleClick);
            // 
            // bt_pause
            // 
            this.bt_pause.Enabled = false;
            this.bt_pause.Location = new System.Drawing.Point(595, 311);
            this.bt_pause.Name = "bt_pause";
            this.bt_pause.Size = new System.Drawing.Size(100, 30);
            this.bt_pause.TabIndex = 0;
            this.bt_pause.Text = "Pause";
            this.bt_pause.UseVisualStyleBackColor = true;
            this.bt_pause.Click += new System.EventHandler(this.bt_pause_Click);
            // 
            // ss_main
            // 
            this.ss_main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ss_lb_parser});
            this.ss_main.Location = new System.Drawing.Point(0, 357);
            this.ss_main.Name = "ss_main";
            this.ss_main.Size = new System.Drawing.Size(817, 22);
            this.ss_main.SizingGrip = false;
            this.ss_main.TabIndex = 12;
            this.ss_main.Text = "statusStrip1";
            // 
            // ss_lb_parser
            // 
            this.ss_lb_parser.Name = "ss_lb_parser";
            this.ss_lb_parser.Size = new System.Drawing.Size(118, 17);
            this.ss_lb_parser.Text = "toolStripStatusLabel1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 379);
            this.Controls.Add(this.ss_main);
            this.Controls.Add(this.tb_status);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.bt_stop);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bt_pause);
            this.Controls.Add(this.bt_start);
            this.Controls.Add(this.lw_mainFilter);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nb_sessionLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_totalMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_maxCycle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_keepMax)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nb_sesMinutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_sesMaxTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nb_sesKeepMax)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ss_main.ResumeLayout(false);
            this.ss_main.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FiltersItem;
        private System.Windows.Forms.ToolStripMenuItem enableLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutTournamentAutoregistrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ColumnHeader shortName;
        private System.Windows.Forms.ColumnHeader CurrentlyOpened;
        private System.Windows.Forms.ColumnHeader maxRegistering;
        private System.Windows.Forms.ColumnHeader MaxPerCycle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tb_shortName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bt_addRow;
        private System.Windows.Forms.NumericUpDown nb_maxCycle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nb_keepMax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader filterDetails;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_filterDetails;
        private System.Windows.Forms.ColumnHeader TargetRegister;
        private System.Windows.Forms.ColumnHeader TotalRegistered;
        private System.Windows.Forms.NumericUpDown nb_totalMax;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bt_EditRow;
        private System.Windows.Forms.Button bt_deleteRow;
        private System.Windows.Forms.Button bt_testFilter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown nb_sesMinutes;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dp_sesTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lb_sesTime;
        private System.Windows.Forms.Label lb_sesMinutes;
        private System.Windows.Forms.ToolStripMenuItem filtersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveFiltersToolStripMenuItem;
        public System.Windows.Forms.TextBox tb_status;
        private System.Windows.Forms.ToolStripMenuItem sessionMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetSessionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unregisterAllTournamentsToolStripMenuItem;
        public System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.CheckBox cb_upTime;
        private System.Windows.Forms.CheckBox cb_minutes;
        public System.Windows.Forms.Label lb_sesTotalNow;
        public System.Windows.Forms.Label lb_sesKeepNow;
        private System.Windows.Forms.ColumnHeader SessionLimit;
        public System.Windows.Forms.NumericUpDown nb_sesMaxTotal;
        public System.Windows.Forms.NumericUpDown nb_sesKeepMax;
        public System.Windows.Forms.ListView lw_mainFilter;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertWarningInLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeLastReadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkAliveToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown nb_sessionLimit;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.ToolStripMenuItem showStatisticsWindowToolStripMenuItem;
        public System.Windows.Forms.Button bt_stop;
        public System.Windows.Forms.Button bt_start;
        public System.Windows.Forms.Button bt_pause;
        private System.Windows.Forms.ToolStripMenuItem resetCounterToolStripMenuItem;
        public System.Windows.Forms.StatusStrip ss_main;
        public System.Windows.Forms.ToolStripStatusLabel ss_lb_parser;
        public System.Windows.Forms.CheckBox cb_totalReg;
        public System.Windows.Forms.CheckBox cb_keepMax;
        
    }
}

