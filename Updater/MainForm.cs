﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Xml;
using System.Security.Cryptography;

namespace Updater
{
    public partial class MainForm : Form
    {
        
        public MainForm()
        {
            InitializeComponent();
            CheckUpdates();
        }

        private void CheckUpdates()
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://lugansktrams.org.ua/autoreg/update.php");
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            XmlDocument doc = new XmlDocument();
            string XMLResponse;

            using (StreamReader stream = new StreamReader(
                response.GetResponseStream()))
            {
                XMLResponse = stream.ReadToEnd();
            }
            doc.LoadXml(XMLResponse);
            CheckLocalFiles(doc);
        }

        private string GetMD5Hash(string filename)
        {
            FileStream file = new FileStream(filename, FileMode.Open);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(file);
            file.Close();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString();
        }

        private void CheckLocalFiles(XmlDocument doc)
        {
            
            XmlNode head = doc.ChildNodes[1];
            XmlNode version = head.ChildNodes[0];
            XmlNode filelist = head.ChildNodes[1];
            foreach (XmlNode filenode in filelist.ChildNodes)
            {
                //MessageBox.Show(filenode.Attributes[0].Value);
                FileInfo file = new FileInfo(Application.StartupPath + filenode.Attributes[0]);
                if (file.Exists)
                {

                }
            }
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}